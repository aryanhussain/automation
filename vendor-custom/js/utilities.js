$(function () {
	if (!$.ds) {
		$.ds = {};
	};
	$.ds.utilities = {};

	// Detect touch devices
	$.ds.utilities.isTouchDevice = 'ontouchstart' in window || (window.DocumentTouch && document instanceof DocumentTouch);
	if ($.ds.utilities.isTouchDevice) {
		document.body.classList.add('js_touch-enabled');
	} else {
		document.body.classList.add('js_touch-disabled');
	}
});