/**
 * DIALOG
 *
 * POSSIBLE WAYS TO INSTANTIATE:
 * $('.yourSelector').dsDialog({
 * 	triggerSelector: '.yourTriggerSelector'
 * });
 * // Access plugin object
 * var myDialog = $('.yourSelector').data('plugin_dsDialog');
 * // Instantiate plugin and store plugin object for future use
 * var myDialog = $('.yourSelector').dsDialog({
 * 	triggerSelector: '.yourTriggerSelector'
 * }).data('plugin_dsDialog');
 * // Vanilla JS instantiation and storing as variable
 * var myDialog = new $.ds.Dialog(document.querySelector('.yourSelector'), {
 * 	triggerSelector: '.yourTriggerSelector'
 * });
 */

;(function ( $, window, document, undefined ) {
	// Set the plugin namespace
	var pluginPrefix = 'ds';
	if (!$[pluginPrefix]) {
		$[pluginPrefix] = {};
	};

	// Create the defaults once
	var pluginName = 'Dialog',
		pluginServiceName = 'Dialogs',
		eventNamespace = '.ds.Dialog',
		defaults = {
			type: 'modal', // [string] 'modal' || 'menu' || 'drawer'
			triggerSelector: null, // [selector] to trigger / open the dialog
			animationTime: 280, // [number] milliseconds to zoom in/out
			openFrom: 'origin', // [string] 'origin' || 'center' || 'trigger' || 'manual' (CSS)
			placement: 'center center', // [string] sets placement of dialog on place of origin (must set both x and y values);
			transformOrigin: false, // [string] overrides the default CSS property transform-origin, useful for more control over the animation
			showOverlay: true, // [boolean] whether to show overlay background
			closeOnBgClick: true, // [boolean] whether to close when clicking background overlay
			closeOnEscape: true, // [boolean] whether to close on 'ESC' key
			focusOnFirst: true, // [boolean] whether to focus on first tabbable element on open or, if false, on first tab press
			tabbableElements: 'a, button, input, textarea, select, [tabindex]', // [string] of comma-separated elements in the dialog element that should be "tabbable"
			restrictTabFocus: true, // [boolean] restricts tab key from focusing outside of dialog
			closeOnBlur: true, // [boolean] if {restrictTabFocus: false}, will close dialog when tabbing away from it
			useUpDownKeys: false, // [boolean] whether to use up and down keys to move keyboard focus
			cycleArrowKeys: true, // [boolean] if {useUpDownKeys: true}, cycles from las tabbable child element to the last

			callbacks: {
				beforeOpen: false, // [function] runs before dialog is open (function (dialogObject) {})
				afterOpen: false, // [function] runs after dialog is open (function (dialogObject) {})
				beforeClose: false, // [function] runs before dialog is closed (function (dialogObject) {})
				afterClose: false // [function] runs after dialog is closed (function (dialogObject) {})
			}
		};
	// Set up private methods / cache
	var _private = {};
	_private.nextId = 0;

	// Create Dialog service, which indexes an array of [all] Dialogs according to their _id and also tracks an array of all [active] Dialogs
	/**
	 * Public service API
	 * @type {object}
	 * @param {array} all Array of all plugin objects (sorted by plugin._id)
	 * @param {array} active Array of currently active plugin objects
	 * @param {integer} overlays Number of active dialogs which have {showOverlay: true}. When this reaches 0 it will close.
	 */
	$[pluginPrefix][pluginServiceName] = {active: [], all: [], overlays: 0};
	// Close dialogs that match the {settings} object OR, if !settings, all active dialogs
	$[pluginPrefix][pluginServiceName].close = function (settings) {
		for (var i in $[pluginPrefix][pluginServiceName].active) {
			if (!settings) {
				$[pluginPrefix][pluginServiceName].active[i].close();
			} else {
				for (var property in settings) {
					if ($[pluginPrefix][pluginServiceName].active[i].settings[property] !== settings[property]) {
						return false;
					}
				};
				$[pluginPrefix][pluginServiceName].active[i].close();
			}
		};
	};
	// Center the most recently active dialog
	$[pluginPrefix][pluginServiceName].centerActive = function () {
		_private.centerPluginElement($[pluginPrefix][pluginServiceName].active[0]);
	};

	// Set default internal settings
	defaults.centerOnWindowResize = true;
	defaults.stayOpenOnScroll = true;
	defaults.allowMultiple = true;
	defaults.classes = {
		container: 'ds_Dialog',
		isActive: 'ds_Dialog--active',
		closeButton: 'ds_Dialog__closeButton',
		menu: 'ds_Dialog--menu',
		drawer: 'ds_Dialog--drawer',
		overlay: 'ds_Dialog__overlay',
		animateReady: 'ds_animate--ready',
		animateIn: 'ds_animate--in',
		animateZoom: 'ds_animate-zoom',
		animateFade: 'ds_animate-fade',
		scrollContainer: 'mdl-layout__content'
	};

	// Create plugin object
	$[pluginPrefix][pluginName] = function (element, options) {
		var plugin = this;

		// Extend options
		plugin.settings = $.extend({}, defaults, options);

		// Create "DOM" to store related elements
		plugin.dom = {};
		plugin.dom.element = element;

		// Set options for various types
		if (plugin.settings.type === 'menu') {
			var typeDefaults = {
				openFrom: 'manual',
				showOverlay: false,
				centerOnWindowResize: false,
				placement: 'left bottom',
				transformOrigin: '25% top 0',
				restrictTabFocus: false,
				useUpDownKeys: true,
				allowMultiple: false
			};
			// Extend settings again to type defaults
			plugin.settings = $.extend({}, plugin.settings, typeDefaults, options);
			// plugin.dom.element.classList.add(plugin.settings.classes.menu, plugin.settings.classes.animateZoom);
			plugin.dom.element.classList.add(plugin.settings.classes.animateZoom);
		} else if (plugin.settings.type === 'drawer') {
			var typeDefaults = {
				openFrom: 'manual',
				placement: 'left',
				centerOnWindowResize: false
			};
			// Extend settings again to type defaults
			plugin.settings = $.extend({}, plugin.settings, typeDefaults, options);
			// plugin.dom.element.classList.add(plugin.settings.classes.drawer);
		} else {
			plugin.dom.element.classList.add(plugin.settings.classes.animateZoom);
		}

		// Create background overlay if it doesn't exist already
		plugin.dom.overlay = document.querySelector('.' + plugin.settings.classes.overlay) || document.body.appendChild(document.createElement('div'));
		plugin.dom.overlay.classList.add(plugin.settings.classes.overlay, plugin.settings.classes.animateFade);

		// Store useful data
		plugin._defaults = defaults;
		plugin._name = 'dsplugin';
		plugin._id = _private.nextId;
		_private.nextId += 1;

		// Add to Dialog service
		$[pluginPrefix][pluginServiceName].all[plugin._id] = plugin;

		// Set up and store trigger elements
		if (plugin.settings.triggerSelector) {
			plugin.dom.$triggers = $(plugin.settings.triggerSelector);
			_private.setupTriggers(plugin);
		}

		// Return the plugin object
		return plugin;
	}

	_private.setupTriggers = function (plugin) {
		function openPluginElement(event) {
			if (plugin.isOpen) {
				plugin.close();
				return true;
			}
			event.stopPropagation();
			plugin.dom.currentTarget = event.currentTarget;
			plugin.open(event);
		}
		plugin.dom.$triggers.on('click' + eventNamespace, openPluginElement);
		// Ensure {type: 'menu'} opens on ENTER and SPACE
		if (plugin.settings.type === 'menu') {
			plugin.dom.$triggers.on('keydown' + eventNamespace, function (event) {
				if (event.keyCode === 32 || event.keyCode === 13) {
					openPluginElement(event);
				}
			});
		}
	};
	_private.positionAtCoordinates = function (plugin, event) {
		plugin.styles = plugin.styles || {};
		plugin.styles.origin = plugin.styles.origin || {};

		// Verify that coordinates exist and are inside the currentTarget element
		if (!event.pageX || !event.pageY) {
			// If falsy, positionAtElement instead
			_private.positionAtElement(plugin, event.currentTarget || plugin.dom.currentTarget, 'center center');
			return false;
		}

		// Calculate styles to position container at origin
		plugin.styles.origin.position = 'fixed';
		plugin.styles.origin.left = (event.pageX - (plugin.dom.element.offsetWidth / 2)) + 'px';
		plugin.styles.origin.top = (event.pageY - (plugin.dom.element.offsetHeight / 2) - parseFloat(window.getComputedStyle(plugin.dom.element, null).getPropertyValue('margin-top'), 10)) + 'px';

		// Set styles on container element
		$(plugin.dom.element).css(plugin.styles.origin);
	},
	_private.positionAtElement = function (plugin, targetElement, placement) {
		var targetElementBox = targetElement.getBoundingClientRect(),
			transformOriginY;
		placement = (placement || plugin.settings.placement).split(' ');
		plugin.styles = plugin.styles || {};
		plugin.styles.origin = plugin.styles.origin || {};

		// Ensure order of placement array is [x, y] and it has two values
		if (placement.length !== 2) {
			placement = ['center', 'center'];
		}
		if (placement[1] === 'left' || placement[1] === 'right') {
			placement.push(placement.shift());
		}

		// Calculate styles to position at the origin
		plugin.styles.origin.position = 'fixed';
		// Set left and top positions
		plugin.styles.origin.left = targetElementBox[placement[0]] || targetElementBox.left + (targetElementBox.width / 2) - (plugin.dom.element.offsetWidth / 2);
		plugin.styles.origin.top = targetElementBox[placement[1]] || targetElementBox.top + (targetElementBox.height / 2) - (plugin.dom.element.offsetHeight / 2);
		// Make adjustments to calculations as needed
		if (placement[0] === 'right') {
			plugin.styles.origin.left -= plugin.dom.element.offsetWidth;
		}
		if (placement[1] === 'over') {
			plugin.styles.origin.top = targetElementBox.top;
		} else if (placement[1] === 'top') {
			plugin.styles.origin.top -= plugin.dom.element.offsetHeight;
		}
		// Set the CSS transform-origin property
		if (plugin.settings.transformOrigin) {
			plugin.styles.origin.transformOrigin = plugin.settings.transformOrigin;
		} else if (placement.indexOf('center') < 0) {
			if (placement[1] === 'bottom' || placement[1] === 'over') {
				transformOriginY = 'top';
			} else if (placement[1] === 'top') {
				transformOriginY = 'bottom';
			}
			plugin.styles.origin.transformOrigin = placement[0] + ' ' + (transformOriginY || placement[1]) + ' 0';
		}

		// Set styles on container element
		$(plugin.dom.element).css(plugin.styles.origin);
	};
	_private.centerPluginElement = function (plugin) {
		plugin.styles = plugin.styles || {};
		plugin.styles.centered = plugin.styles.centered || {};

		// Calculate styles to position at center
		if (plugin.settings.openFrom === 'origin' || plugin.settings.openFrom === 'trigger') {
			plugin.styles.centered.position = 'fixed';
			plugin.styles.centered.left = (window.innerWidth - plugin.dom.element.offsetWidth) / 2 + 'px';
		}
		plugin.styles.centered.top = (window.innerHeight - plugin.dom.element.offsetHeight) / 2 + 'px';

		// Set styles on container element
		$(plugin.dom.element).css(plugin.styles.centered);
	};
	_private.attachEvents = function (plugin) {
		// Close button
		var closeButtonElements = plugin.dom.element.querySelectorAll('.' + plugin.settings.classes.closeButton);
		$(closeButtonElements).on('click' + eventNamespace + plugin._id, function (event) {
			event.stopPropagation();
			plugin.close();
		});

		// Click on overlay background
		if (plugin.settings.closeOnBgClick) {
			$(document).on('click' + eventNamespace + plugin._id, function () {
				plugin.close();
			});
			$(plugin.dom.element).on('click' + eventNamespace + plugin._id, function (event) {
				// Close if {allowMultiple: false}
				if ($[pluginPrefix][pluginServiceName].active.length > 1) {
					$[pluginPrefix][pluginServiceName].close({allowMultiple: false});
				}
				event.stopPropagation();
			});
		}

		// Keyboard handling
		$(document).on('keydown' + eventNamespace + plugin._id, function (event) {
			if (plugin.settings.useUpDownKeys && (event.keyCode === 38 || event.keyCode === 40)) {
				// UP and DOWN key presses
				var $focusedElement = $(plugin.dom.element).find(':focus'),
					currentIndex = plugin.dom.$tabbableElements.index($focusedElement),
					newIndex = (event.keyCode === 40) ? currentIndex + 1 : currentIndex - 1;

				if (plugin.settings.cycleArrowKeys) {
					if (newIndex >= plugin.dom.$tabbableElements.length) {
						newIndex = 0;
					} else if (newIndex < 0) {
						newIndex = plugin.dom.$tabbableElements.length - 1;
					}

					plugin.dom.$tabbableElements.eq(newIndex).focus();
				} else {
					plugin.dom.$tabbableElements.eq(Math.max(newIndex, 0)).focus();
				}
			} else if (plugin.settings.closeOnEscape && event.keyCode === 27) {
				// ESCAPE key press closes only currently active container
				if ($[pluginPrefix][pluginServiceName].active[0]._id === plugin._id) {
					$[pluginPrefix][pluginServiceName].active[0].close();
				}
				if (plugin.dom.currentTarget) {
					plugin.dom.currentTarget.focus();
				}
			}
		});
		// TAB key handling on first tabbable element
		function handleTabKey(event, eq) {
			if (plugin.settings.restrictTabFocus) {
				event.preventDefault();
				plugin.dom.$tabbableElements.eq(eq).focus();
			} else if (plugin.settings.closeOnBlur) {
				plugin.close();
			}
		}
		plugin.dom.$tabbableElements.first().on('keydown' + eventNamespace + plugin._id, function (event) {
			if (event.keyCode === 9 && event.shiftKey) {
				handleTabKey(event, -1);
			}
		});
		// TAB key handling on last tabbable element
		plugin.dom.$tabbableElements.last().on('keydown' + eventNamespace + plugin._id, function (event) {
			if (event.keyCode === 9 && !event.shiftKey) {
				handleTabKey(event, 0);
			}
		});

		// Window resize with debounce
		if (plugin.settings.centerOnWindowResize) {
			var resizeTimer;
			$(window).on('resize' + eventNamespace + plugin._id, function () {
				clearTimeout(resizeTimer);
				resizeTimer = setTimeout(function () {
					_private.centerPluginElement(plugin);
				}, 100);
			});

			// Fire window.resize on any interaction that could change the dimensions of the container. This will re-center the container.
			$(plugin.dom.element).on('click' + eventNamespace + ' ' + 'keyup' + eventNamespace, function () {
				setTimeout(function () {
					window.dispatchEvent(new Event('resize'));
				}, 100);
			});
		}

		// Scroll container with scrollbar
		if (plugin.settings.stayOpenOnScroll) {
			var resizeTimer;
			$(plugin.settings.classes.scrollContainer).on('scroll' + eventNamespace + plugin._id, function (event) {
				plugin.dom.element.style.top = parseFloat(plugin.styles.origin.top, 10) - this.scrollTop + 'px'
			});
		}
	};
	_private.detachEvents = function (plugin) {
		var closeButtonElements = plugin.dom.element.querySelectorAll('.' + plugin.settings.classes.closeButton);
		$(closeButtonElements).off(eventNamespace + plugin._id);
		$(plugin.dom.overlay).off(eventNamespace + plugin._id);
		$(plugin.dom.element).off(eventNamespace + plugin._id);
		plugin.dom.$tabbableElements.off(eventNamespace + plugin._id);
		$(plugin.settings.classes.scrollContainer).off(eventNamespace + plugin._id);
		$(document).off(eventNamespace + plugin._id);
		$(window).off(eventNamespace + plugin._id);
	};

	// Open method
	$[pluginPrefix][pluginName].prototype.open = function (event) {
		var plugin = this;
		// Fire beforeOpen() callback
		if (plugin.settings.callbacks.beforeOpen) {
			plugin.settings.callbacks.beforeOpen(plugin);
		}
		// Store original event for later access
		plugin.event = event || plugin.event;
		// Close other containers that allow one at a time
		if ($[pluginPrefix][pluginServiceName].active.length > 0) {
			$[pluginPrefix][pluginServiceName].close({allowMultiple: false});
		}
		// Inactivate any secondary dialogs if {showOverlay: true}
		if (plugin.settings.showOverlay && $[pluginPrefix][pluginServiceName].overlays > 0) {
			$[pluginPrefix][pluginServiceName].active[0].dom.element.classList.remove($[pluginPrefix][pluginServiceName].active[0].settings.classes.isActive);
		}
		// Add plugin to active Dialogs service
		plugin.isOpen = true;
		$[pluginPrefix][pluginServiceName].active.unshift(plugin);
		// Prepare plugin for animation
		plugin.dom.element.style.transitionDuration = '0s';
		plugin.dom.element.classList.add(plugin.settings.classes.animateReady, plugin.settings.classes.isActive);
		if (plugin.settings.showOverlay) {
			$[pluginPrefix][pluginServiceName].overlays += 1;
			plugin.dom.overlay.classList.add(plugin.settings.classes.animateReady, plugin.settings.classes.overlay + '--' + plugin.settings.type);
		}
		// Store tabbable elements to access later
		plugin.dom.$tabbableElements = $(plugin.dom.element).find(plugin.settings.tabbableElements).not('[tabindex="-1"], [disabled]');
		// Position container
		if (plugin.event && plugin.settings.openFrom === 'origin') {
			// Position container at the click origin
			_private.positionAtCoordinates(plugin, plugin.event);
		} else if (plugin.event && plugin.settings.openFrom === 'trigger') {
			// Position container at the trigger element
			_private.positionAtElement(plugin, plugin.dom.currentTarget);
		} else if (!plugin.settings.openFrom === 'manual') {
			// Position container in the center & reset origin styles (so it closes to the center)
			_private.centerPluginElement(plugin);
			plugin.styles.origin = {};
		}
		// Add events while open
		_private.attachEvents(plugin);
		setTimeout(function () {
			// Reset transition-duration to its previous value
			plugin.dom.element.style.transitionDuration = plugin.settings.animationTime + 'ms';
			// Show overlay background
			if (plugin.settings.showOverlay) {
				plugin.dom.overlay.classList.add(plugin.settings.classes.animateIn);
				plugin.settings.overlayIsActive = true;
			}
			// Move container from origin to center
			plugin.dom.element.classList.add(plugin.settings.classes.animateIn);
			if (plugin.settings.openFrom === 'origin') {
				_private.centerPluginElement(plugin);
			}
			// Add active class on trigger element
			if (plugin.dom.currentTarget) {
				plugin.dom.currentTarget.classList.add(plugin.settings.classes.isActive);
			}
			// Focus on first focusable element
			if (plugin.settings.focusOnFirst) {
				plugin.dom.$tabbableElements.eq(0).focus();
			}
			// Fire afterOpen() callback
			if (plugin.settings.callbacks.afterOpen) {
				plugin.settings.callbacks.afterOpen(plugin);
			}
		}, 20);
	};
	// Close method
	$[pluginPrefix][pluginName].prototype.close = function () {
		var plugin = this;
		// Fire beforeClose() callback
		if (plugin.settings.callbacks.beforeClose) {
			plugin.settings.callbacks.beforeClose(plugin);
		}
		// Set isOpen state
		plugin.isOpen = false;
		// Remove events from active plugin
		_private.detachEvents(plugin);
		if (plugin.dom.currentTarget) {
			// Focus on trigger element & remove its reference
			plugin.dom.currentTarget.classList.remove(plugin.settings.classes.isActive);
		}
		// Remove animateIn class
		plugin.dom.element.classList.remove(plugin.settings.classes.animateIn);
		// Determine whether to hide overlay
		if (plugin.settings.showOverlay) {
			$[pluginPrefix][pluginServiceName].overlays -= 1;
			if (!$[pluginPrefix][pluginServiceName].overlays) {
				plugin.dom.overlay.classList.remove(plugin.settings.classes.animateIn);
			} else {
				$[pluginPrefix][pluginServiceName].active[1].dom.element.classList.add($[pluginPrefix][pluginServiceName].active[1].settings.classes.isActive);
				plugin.dom.overlay.classList.remove(plugin.settings.classes.overlay + '--' + plugin.settings.type);
				plugin.dom.overlay.classList.add(plugin.settings.classes.overlay + '--' + $[pluginPrefix][pluginServiceName].active[1].settings.type);
			}
		}
		// Position container where it opened
		if (plugin.event && plugin.settings.openFrom === 'origin') {
			// Position container at the click origin
			_private.positionAtCoordinates(plugin, plugin.event);
		} else if (plugin.event && plugin.settings.openFrom === 'trigger') {
			// Position container at the trigger element
			_private.positionAtElement(plugin, plugin.dom.currentTarget);
		}
		setTimeout(function () {
			// Once animation completes, hide container completely
			plugin.dom.element.classList.remove(plugin.settings.classes.animateReady);
			// Reset styles
			plugin.dom.element.classList.remove(plugin.settings.classes.isActive);
			plugin.dom.element.setAttribute('style', '');
			if (!$[pluginPrefix][pluginServiceName].overlays) {
				plugin.dom.overlay.classList.remove(plugin.settings.classes.animateReady, plugin.settings.classes.overlay + '--' + plugin.settings.type);
				plugin.dom.overlay.setAttribute('style', '');
			}
			if (plugin.styles && plugin.styles.origin) {
				plugin.styles.origin = {};
			}
			// Remove from active Dialogs service
			for (var i in $[pluginPrefix][pluginServiceName].active) {
				if ($[pluginPrefix][pluginServiceName].active[i]._id === plugin._id) {
					$[pluginPrefix][pluginServiceName].active.splice(i, 1);
					break;
				}
			};
			// Fire afterClose() callback
			if (plugin.settings.callbacks.afterClose) {
				plugin.settings.callbacks.afterClose(plugin);
			}
		}, plugin.settings.animationTime + 40); // provide a buffer for animation
	};
	// Destroy method
	$[pluginPrefix][pluginName].prototype.destroy = function () {
		var plugin = this,
			closeButtonElements = plugin.dom.element.querySelectorAll('.' + plugin.settings.classes.closeButton);
		plugin.close();
		plugin.dom.$triggers.off();
		$(closeButtonElements).off(eventNamespace);
		$(plugin.dom.overlay).off(eventNamespace);
		$(plugin.dom.element).off(eventNamespace);
		plugin.dom.$tabbableElements.off(eventNamespace);
		$(plugin.settings.classes.scrollContainer).off(eventNamespace);
		$(document).off(eventNamespace);
		$(window).off(eventNamespace);
	};

	// A really lightweight plugin wrapper around the constructor, preventing against multiple instantiations
	$.fn[pluginPrefix + pluginName] = function (options, params) {
		return this.each(function () {
			if (options === 'open') {
				$.data(this, 'plugin_' + pluginPrefix + pluginName).open(params);
			} else if (options === 'close') {
				$.data(this, 'plugin_' + pluginPrefix + pluginName).close();
			} else if (!$.data(this, 'plugin_' + pluginPrefix + pluginName)) {
				// Prevent from multiple instantiations
				$.data(this, 'plugin_' + pluginPrefix + pluginName, new $[pluginPrefix][pluginName](this, options)._id);
			}
		});
	}

})( jQuery, window, document );
