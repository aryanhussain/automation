$(function () {
	// Instantiate hide/show items
	$('.ds_HideShow').dsHideshow();

	// Instantiate dialogs
	$('.ds_Dialog').each(function () {
		var dialogSettings = {triggerSelector: '[data-dialog=#' + this.id + ']'};
		if (!$.ds.utilities.isTouchDevice) {
			dialogSettings.triggerSelector += ', [data-fab-dialog=#' + this.id + ']';
		}
		if (this.classList.contains('ds_Dialog--menu')) {
			dialogSettings = {
				triggerSelector: '[data-menu="#' + this.id + '"]',
				type: 'menu',
				stayOpenOnScroll: false,
				animationTime: 180
			}
		}
		$(this).dsDialog(dialogSettings);
	});
	// Mimick dialog actions (should be [ng-click])
	$('.ds_Dialog').find('[data-action]').on('click', function () {
		alert(this.getAttribute('data-action') + ' (for pretend).');
		$.ds.activeDialogs[$.ds.activeDialogs.last.type][$.ds.activeDialogs.last._id].close();
	});


	/**
	 * Filter toolbar
	 */
	$('#ds_Nav-admin-filter-toolbar').slideUp(280);
	$('#ds_Nav-admin-filter-toggle').on('change', function () {
		var self = this;
		function triggerResize() {
			$(window).trigger('resize');
		}
		if (self.checked) {
			$('#ds_Nav-admin-filter-toolbar').stop().slideDown(280, triggerResize);
			$('.mdl-icon-toggle[for="ds_Nav-admin-filter-toggle"]').find('.mdl-icon-toggle__label').removeClass('icon-filter').addClass('icon-filter-remove');
		} else {
			$('#ds_Nav-admin-filter-toolbar').stop().slideUp(280, triggerResize);
			$('.mdl-icon-toggle[for="ds_Nav-admin-filter-toggle"]').find('.mdl-icon-toggle__label').removeClass('icon-filter-remove').addClass('icon-filter');
		}
	});


	/**
	 * Instantiate sidebar / drawers and add click events on icon toggle buttons
	 */
	// Instantiate drawers
	$('#ds_Drawer-left').dsDialog({
		type: 'drawer',
		triggerSelector: '[data-toggle="#ds_Drawer-left"]',
		callbacks: {
			beforeOpen: function (dialog) {
				document.body.classList.add('ds_layout--left-drawer-active');
			},
			afterClose: function (dialog) {
				document.body.classList.remove('ds_layout--left-drawer-active');
			}
		}
	});
	var rightDrawerElement = document.querySelector('#ds_Drawer-right')
	var rightDrawer = new $.ds.Dialog(rightDrawerElement, {
		type: 'drawer',
		showOverlay: false,
		closeOnBgClick: false,
		closeOnEscape: false,
		focusOnFirst: false,
		restrictTabFocus: false,
		closeOnBlur: false,
		callbacks: {
			beforeOpen: function (dialog) {
				document.body.classList.add('ds_layout--right-drawer-active');
				rightDrawerElement.style.top = document.querySelector('.ds_Nav-admin__content').offsetTop + 'px';
				var resizeTimer;
				$(window).on('resize.dsRightDrawer', function () {
					clearTimeout(resizeTimer);
					resizeTimer = setTimeout(function () {
						rightDrawerElement.style.top = document.querySelector('.ds_Nav-admin__content').offsetTop + 'px';
					}, 200);
				});
			},
			beforeClose: function (dialog) {
				document.body.classList.remove('ds_layout--right-drawer-active');
				$(window).off('resize.dsRightDrawer');
			},
			afterOpen: function (dialog) {
				setTimeout(function () {
					rightDrawerElement.style.top = document.querySelector('.ds_Nav-admin__content').offsetTop + 'px';
				}, 200);
			}
		}
	});
	$('[data-checkbox-group]').on('change', function () {
		var self = this,
			group = self.dataset.checkboxGroup,
			checkboxes = document.querySelectorAll('[data-checkbox-group="' + group + '"]');
		if (self.checked) {
			rightDrawer.open();
			Array.prototype.filter.call(checkboxes, function (checkbox) {
				if (checkbox !== self) {
					checkbox.checked = false;
					checkbox.parentNode.classList.remove('is-checked');
				}
			});
			rightDrawerElement.classList.remove('ds_Nav-admin--preview-phone', 'ds_Nav-admin--preview-tablet', 'ds_Nav-admin--preview-desktop');
			rightDrawerElement.classList.add('ds_Nav-admin--' + self.value);
		} else {
			rightDrawer.close();
		}
		// rightDrawerElement.classList.add('')
	});
	$('[data-action-close="#ds_Drawer-right"]').on('click', function (e) {
		e.preventDefault();
		document.querySelector('[data-checkbox-group="ds_Nav-admin-filters__preview-toggle"]:checked').click();
	});


	/**
	 * Hide/show toggles for filters
	 */
	function toggleFilter($checkbox) {
		var filterPostfixes = ['user-types', 'countries', 'languages', 'status', 'rank', 'type', 'date', 'subscriber', 'new-enrollee'];
		var selectorPrefix = '#ds_Nav-admin-filters__';
		var index = $checkbox.attr('id').slice(-1) - 1;
		if ($checkbox.is(':checked')) {
			$(selectorPrefix + filterPostfixes[index]).closest('.ds_Nav-admin-filters__cell').fadeIn(280);
		} else {
			$(selectorPrefix + filterPostfixes[index]).closest('.ds_Nav-admin-filters__cell').fadeOut(280);
		}
	}
	$('#NavAdminFilterMenu__more__input-1, #NavAdminFilterMenu__more__input-2, #NavAdminFilterMenu__more__input-3').find('input[type="checkbox"]').prop('checked', true);
	$('#NavAdminFilterMenu__more').find('input[type="checkbox"]')
		.on('change', function () {
			toggleFilter($(this));
		})
		.each(function () {
			// Hide / show filters on DOM load
			var checkboxesToMark = ['NavAdminFilterMenu__more__input-1', 'NavAdminFilterMenu__more__input-2', 'NavAdminFilterMenu__more__input-3'];
			if (checkboxesToMark.indexOf(this.id) > -1) {
				$(this).prop('checked', true);
			}
			toggleFilter($(this));
		});


	/**
	 * Date pickers
	 */
	var datePickerOptions = {shortTime: true, format: 'dddd, MMMM D, YYYY - h:mma', minDate: new Date(), okText: "Save"};
	$('#ds_Nav-admin-filters__date').on('change', function () {
		if ($(this).is(':checked')) {
			$('#ds_Nav-admin-filters__date-input').trigger('click');
		}
	})
	$('#ds_Nav-admin-filters__date-input').datepicker(datePickerOptions).on('change', function (e, result) {
		$('#ds_Nav-admin-filters__date').siblings('.mdl-icon-toggle__text-label').text(moment(result._d).format('M/D/YYYY'));
		$('#ds_Nav-admin-filters__date').trigger('click');
	});
	$('#dialog_edit-link-input__end-date, #dialog_edit-link-input__start-date').datepicker(datePickerOptions).on('beforeChange', function () {
		$(this).parent('.mdl-textfield--floating-label').addClass('is-dirty').focus();
	});
	/**
	 * Hide/show toggle for "Always Show" datepicker
	 */
	$('#ds_Dialog-edit-link__link-dates').fadeOut(0);
	$('#dialog_edit-link-input__always-show').on('change', function () {
		if ($(this).is(':checked')) {
			$('#ds_Dialog-edit-link__link-dates').fadeOut(280);
		} else {
			$('#ds_Dialog-edit-link__link-dates').fadeIn(280);
		}
	});

	/**
	 * Sortable dragging
	 */
	var navAdminTableElement = document.querySelector('.ds_Nav-admin-table');
	//var sortableFolders = Sortable.create(document.querySelector('.ds_Nav-admin-table'), {
	//	group: 'links',
	//	draggable: '.ds_Nav-admin-table__row--sortable',
	//	handle: $.ds.utilities.isTouchDevice ? '.icon-drag-vertical' : false,
	//	onUpdate: function (event) {
	//		if (event.item.classList.contains('ds_Nav-admin-table__row--folder')) {
	//			// Move folder's content after folder
	//			$(event.item).after($('.ds_Nav-admin-table__folder-links[data-sortable-content-index="' + event.item.dataset.sortableIndex + '"]'));
	//		}
	//		// If a link is moved to the top level, make sure hide/show content is sibling of its trigger
	//		else if ($(event.item).next('.ds_Nav-admin-table__folder-links').length) {
	//			$(event.item).next('.ds_Nav-admin-table__folder-links').after($(event.item));
	//		}
	//	},
	//	onMove: function (event) {
	//		// Prevent from moving folder inside of another folder
	//		if (event.dragged.classList.contains('ds_Nav-admin-table__row--folder') && event.to.classList.contains('ds_Nav-admin-table__folder-links')) {
	//			return false;
	//		}
	//	}
	//});

	$('.ds_Nav-admin-table__folder-links').each(function (i, element) {
		var sortable = Sortable.create(this, {
			group: 'links',
			draggable: '.ds_Nav-admin-table__row--sortable',
			handle: $.ds.utilities.isTouchDevice ? '.icon-drag-vertical' : false
		});
	});

	/**
	 * FAB group focus handling
	 */
	if ($.ds.utilities.isTouchDevice) {
		$('.ds_mdl-fab-group').on('click', '.mdl-button', function (event) {
			var fabGroupElement = this.parentNode;
			event.stopPropagation();
			if (fabGroupElement.classList.contains('ds_mdl-fab-group--active')) {
				var dialog = $.ds.Dialogs.all[$(this.dataset.fabDialog).data('plugin_dsDialog')];
				dialog.open(event);
				fabGroupElement.classList.remove('ds_mdl-fab-group--active');
			} else {
				fabGroupElement.classList.add('ds_mdl-fab-group--active');
			}
		});
	} else {
		$('.ds_mdl-fab-group').on('focus', '.mdl-button', function () {
			var fabGroupElement = this.parentNode;
			fabGroupElement.classList.add('ds_mdl-fab-group--active');
		}).on('blur', '.mdl-button', function () {
			var fabGroupElement = this.parentNode;
			fabGroupElement.classList.remove('ds_mdl-fab-group--active');
		});
	}


	window.onload = function () {
		$('.material-icons').removeClass('material-icons').addClass('icon icon-menu').html('');
	}
});