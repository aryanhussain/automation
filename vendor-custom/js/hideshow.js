; (function ($, window, document, undefined) {
    // Create defaults once
    var defaults = {
        animation: 'slide', // [string] 'slide' || 'fade'
        animationTime: 200 // [number] in millseconds
    };

    function dsHideShow(element, options) {
        // Extend options
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = 'dsHideshow';

        // Build the hideshow DOM
        this.dom = {
            element: element,
            // content: $(element).next()[0]
            content: element.nextElementSibling
        };
        this.dom.trigger = document.createElement('button');
        this.dom.trigger.classList.add('mdl-button', 'mdl-button--icon', 'ds_HideShow__trigger');
        this.dom.trigger.setAttribute('type', 'button');
        this.dom.trigger.innerHTML = '<i class="icon icon-chevron-down icon-24px"/>';
        this.dom.element.appendChild(this.dom.trigger);

        this.init();
    }

    function destroy(self) {
        $(self).off('.dsHideshow');
        $.removeData(self, 'dsHideshow');
    }

    function addEvents(self) {
        // Click event
        var animationInFn = (self.options.animation === 'fade') ? 'fadeIn' : 'slideDown';
        var animationOutFn = (self.options.animation === 'fade') ? 'fadeOut' : 'slideUp';
        $(self.dom.element).on('click.dsHideshow', function () {
            $(self.dom.content).stop();
            if (self.dom.content.classList.contains('js-active')) {
                $(self.dom.content)[animationOutFn](self.options.animationTime).add($(self.dom.element)).removeClass('js-active');
                if (window.location.href.indexOf("DocumentsAndMedia") > -1) {
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('i').removeClass('icon-folder-outline c-orange').addClass('icon-folder c-blue');
                    //$(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('span.labeltext').css('color', 'black');
                }
                else {
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('span.labeltext').css('color', 'black');
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('svg path').attr('fill', '#000000');
                    $(self.dom.element).children('.ds_Nav-admin-table__cell').find('.icon-pencil').removeClass('c-black');
                }
              
                

            } else {
                $(self.dom.content)[animationInFn](self.options.animationTime).add($(self.dom.element)).addClass('js-active');
                if (window.location.href.indexOf("DocumentsAndMedia") > -1) {
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('i').addClass('icon-folder-outline c-orange').removeClass('icon-folder c-blue');
                    //$(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('span.labeltext').css('color', '#2196F3');
                }
                else {
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('span.labeltext').css('color', 'black');
                    $(self.dom.element).children('.ds_Nav-admin-table__cell:first-child').find('svg path').attr('fill', '#FF980F');
                    $(self.dom.element).children('.ds_Nav-admin-table__cell').find('.icon-check').addClass('c-black');
                    $(self.dom.element).children('.ds_Nav-admin-table__cell').find('.icon-pencil').addClass('c-black');
                }
                
            }
        });
    }

    dsHideShow.prototype.init = function () {
        var self = this;
        // You have access to DOM element and options with this.element and this.options

        // Close all hideshow elements without .js-active
        $(self.dom.content).each(function () {
            if (!this.classList.contains('js-active')) {
                $(this).slideUp();
            }
        });

        addEvents(self);
    }

    $.fn.dsHideshow = function (options) {
        return this.each(function () {
            var self = this;
            if (options === 'destroy') {
                destroy(self);
            } else if (!$.data(self, 'dsHideshow')) {
                // Prevent against multiple instantiations
                $.data(self, 'dsHideshow', new dsHideShow(self, options));
            }
        });
    }
})(jQuery, window, document);