﻿angular.module('cookieService', [

]).
    factory('cookieService', [
    '$http', '$q', 'pendingRequests', 'ApiUrl',  '$state', '$stateParams', '$http', 'ApiUrl', '$injector', '$window',
    function ($http, $q, pendingRequests, ApiUrl, $state, $stateParams, $injector, $rootScope, $scope, $window) {
        return {
            createCookie: function (name, value, days) {
                var domain = "";
                var domainParts, date, expires, host;

                if (days) {
                    //date = new Date();
                    //date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = "; expires=" + days;
                }
                else {
                    expires = "";
                }

                host = location.host;
                if (host.split('.').length === 1) {
                    // no "." in a domain - it's localhost or something similar
                    document.cookie = name + "=" + value + expires + "; path=/";
                }
                else {
                    // Remember the cookie on all subdomains.
                    //
                    // Start with trying to set cookie to the top domain.
                    // (example: if user is on foo.com, try to set
                    //  cookie to domain ".com")
                    //
                    // If the cookie will not be set, it means ".com"
                    // is a top level domain and we need to
                    // set the cookie to ".foo.com"
                    domainParts = host.split('.');
                    if (location.hostname.search("directscale") > -1) {
                        domainParts.shift();
                        domainParts.shift();
                    } else {
                        domainParts.shift();
                    }
                    domain = '.' + domainParts.join('.');
                    console.log("domain40", domain);
                    document.cookie = name + "=" + value + expires + "; path=/; domain=" + domain;

                    // check if cookie was successfuly set to the given domain
                    // (otherwise it was a Top-Level Domain)
                    if (this.getCookie(name) == null || this.getCookie(name) != value) {
                        // append "." to current domain
                        domain = '.' + host;
                        console.log("domain48", domain);
                        document.cookie = name + "=" + value + expires + "; path=/; domain=" + domain;
                    }

                }
            },

            getCookie: function (name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1, c.length);
                    }

                    if (c.indexOf(nameEQ) === 0) { return c.substring(nameEQ.length, c.length); }
                }
                return null;
            },

            eraseCookie: function (name) {
                this.createCookie(name, '', -1);
            }

        };
    }
]);

//factory('cookieService', ['$cookieStore', '$location', function ($cookieStore, $location) {
//    return {
//        getRootDomain: function () {
//            if (new RegExp('[^.]+\\.[^.]{1,4}(\\.[^.]{1,3})?$').exec($location.host())) {
//                return new RegExp('[^.]+\\.[^.]{1,4}(\\.[^.]{1,3})?$').exec($location.host())[0];
//            }
//        },
//        getCookie: function (name) {
//            return $cookieStore.get(name);
//        },
//        eraseCookie: function (name) {
//            $cookieStore.remove(name, { domain: this.getRootDomain() });
//            // The V2.75 token
//        },
//        createCookie: function (name, token, days) {
//            $cookieStore.put(name, token, { domain: this.getRootDomain() });
//        }

//    };
//}]);


