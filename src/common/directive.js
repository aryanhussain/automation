﻿
angular.module('OUApp.directives', [])
 .value('THROTTLE_MILLISECONDS', null)
.directive('focusout', ['$parse', function ($parse) {
    return {
        compile: function ($element, attr) {
            var fn = $parse(attr.focusout);
            return function handler(scope, element) {
                element.on('focusout', function (event) {
                    scope.$apply(function () {
                        fn(scope, { $event: event });
                    });
                });
            };
        }
    };
}])
.directive('validateemail', ['$rootScope', '$parse', '$RestService', function ($rootScope, $parse, $RestService) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            ngModel: '='

        },
        compile: function ($element, attr) {
            return function handler(scope, element) {
                element.on('focusout', function (event) {
                    scope.$apply(function () {
                        var email = "email=" + scope.ngModel;
                        var filter = new RegExp('^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
                        if (scope.ngModel && filter.test(scope.ngModel)) {
                            $RestService.ValidateEmail(email).success(function (result) {
                                if (result) {

                                }
                                else {
                                    scope.ngModel = "";
                                }
                            }).error(function (error) {
                                $rootScope.Notification('Error', 'Some error occur please try again', 'danger');
                            });
                        }

                    });
                });
            };
        }
    };
}])
.directive('setzero', ['$parse', function ($parse) {
    return {
        compile: function ($element, attr) {
            //var fn = $parse(attr.focusout);
            return function handler(scope, element) {
                element.on('focusout', function (event) {
                    if (!element.val()) {
                        element.val(0);
                    }
                });
            };
        }
    };
}])

//Html

// .directive('html', ['nicescrollService', function (nicescrollService) {
//         return {
//             restrict: 'E',
//             link: function (scope, element) {

//                 if (!element.hasClass('ismobile')) {
//                     if (!$('.login-content')[0]) {
//                         nicescrollService.niceScroll(element, 'rgba(0,0,0,0.3)', '5px');
//                     }
//                 }
//             }
//         };
//     }])


    .directive('tabNav', ['nicescrollService', function (nicescrollService) {
        return {
            restrict: 'C',
            link: function (scope, element) {

                if (!$('html').hasClass('ismobile')) {
                    nicescrollService.niceScroll(element, 'rgba(0,0,0,0.3)', '2px');
                }
            }
        };
    }])


    //For custom class

    .directive('cOverflow', ['nicescrollService', function (nicescrollService) {
        return {
            restrict: 'C',
            link: function (scope, element) {

                if (!$('html').hasClass('ismobile')) {
                    nicescrollService.niceScroll(element, 'rgba(0,0,0,0.5)', '5px');
                }
            }
        };
    }])


    // =========================================================================
    // WAVES
    // =========================================================================

    // For .btn classes
    .directive('btn', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                try {
                    if (element.hasClass('btn-icon') || element.hasClass('btn-float')) {
                        Waves.attach(element, ['waves-circle']);
                    }

                    else if (element.hasClass('btn-light')) {
                        Waves.attach(element, ['waves-light']);
                    }

                    else {
                        Waves.attach(element);
                    }

                    Waves.init();
                } catch (ex) { console.log(ex); }
            }
        };
    })  // =========================================================================
    // LAYOUT
    // =========================================================================




    // =========================================================================
    // MAINMENU COLLAPSE
    // =========================================================================

    .directive('toggleSidebar', function () {

        return {
            restrict: 'A',
            scope: {
                modelLeft: '=',
                modelRight: '='
            },

            link: function (scope, element, attr) {

                element.on('click', function () {
                    if (element.data('target') === 'mainmenu') {
                        scope.$apply(function () {
                            scope.modelLeft = scope.modelLeft ? false : true;

                            var size = $(window).width();
                            setTimeout(function () {
                                $("#sidebar").css("width", (size - 56) + "px");
                                $("#sidebar.toggled").css("-webkit-transform", "translate3d(0,0,0)");
                                $("#sidebar.toggled").css("transform", "translate3d(0,0,0)");
                            }, 1);
                        });


                    }

                    if (element.data('target') === 'chat') {
                        if (scope.modelRight === false) {
                            scope.$apply(function () {
                                scope.modelRight = true;
                            });
                        }
                        else {
                            scope.$apply(function () {
                                scope.modelRight = false;
                            });
                        }

                    }
                });
            }
        };

    })




    // =========================================================================
    // SUBMENU TOGGLE
    // =========================================================================

    .directive('toggleSubmenu', function () {

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.click(function () {
                    //$('ul.main-menu li').removeClass('toggled');
                    $('ul.main-menu li').not(element.parent()).removeClass('toggled');
                    element.parent().toggleClass('toggled');
                    element.parent().find('ul').stop(true, false).slideToggle(200);
                    $('.sub-menu').each(function () {
                        var submenu = this;

                        if ($(submenu).hasClass('toggled')) {

                        }
                        else {

                            $(submenu).find('ul').css('display', 'none');
                        }

                    });
                });
            }
        };
    })

     // =========================================================================
    // WIDGETS TOGGLE
    // =========================================================================

    .directive('toggleWidgetsmenu', function () {

        return {
            restrict: 'A',
            scope: {
                ngModel: '='
            },
            link: function (scope, element, attr) {
                element.click(function () {
                    setTimeout(function myfunction() {
                        scope.$apply(function () {
                            if (scope.ngModel === true) {
                                $('ul.main-menu li').not(element.parents('.card')).removeClass('toggled');
                                element.parents('.card').toggleClass('toggled');
                                element.parents('.card').find('ul').stop(true, false).slideToggle(200);
                            }
                            else {
                                $('ul.main-menu li').not(element.parents('.card')).removeClass('toggled');
                                element.parents('.card').toggleClass('toggled');
                                element.parents('.card').find('ul').stop(true, false).slideToggle(200);
                            }
                        });
                    }, 50);
                   
                });
            }
    };
})


// =========================================================================
// STOP PROPAGATION
// =========================================================================

    .directive('stopPropagate', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                element.on('click', function (event) {
                    event.stopPropagation();
                });
            }
        };
    })

    .directive('aPrevent', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                element.on('click', function (event) {
                    event.preventDefault();
                });
            }
        };
    })


    // =========================================================================
    // PRINT
    // =========================================================================

    .directive('print', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.click(function () {
                    window.print();
                });
            }
        };
    }).directive('mediaElement', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                element.mediaelementplayer();
            }
        };

    })



    .directive('fgLine', function () {
        return {
            restrict: 'C',
            link: function (scope, element) {
                if ($('.fg-line')[0]) {
                    $('body').on('focus', '.form-control', function () {
                        $(this).closest('.fg-line').addClass('fg-toggled');
                    });

                    $('body').on('blur', '.form-control', function () {
                        var p = $(this).closest('.form-group');
                        var i = p.find('.form-control').val();

                        if (p.hasClass('fg-float')) {
                            if (i.length === 0) {
                                $(this).closest('.fg-line').removeClass('fg-toggled');
                            }
                        }
                        else {
                            $(this).closest('.fg-line').removeClass('fg-toggled');
                        }
                    });
                }

            }
        };

    })





    // =========================================================================
    // BOOTSTRAP SELECT
    // =========================================================================

    .directive('selectPicker', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //if (element[0]) {
                element.selectpicker();
                //}
            }
        };
    })



    // =========================================================================
    // WEATHER WIDGET
    // =========================================================================



    .directive('fullCalendar', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var event = [];
                if (attrs.eventdata) {
                    event = JSON.parse(attrs.eventdata);
                }
                element.fullCalendar({
                    contentHeight: 'auto',
                    theme: true,
                    header: {
                        right: '',
                        center: 'prev, title, next',
                        left: ''
                    },
                    defaultDate: new Date(),
                    editable: true,
                    events: event
                });
            }
        };
    })


    // =========================================================================
    // MAIN CALENDAR
    // =========================================================================

    .directive('calendar', function ($compile, $location) {
        return {
            restrict: 'A',
            scope: {
                select: '&',
                actionLinks: '='
            },
            link: function (scope, element, attrs) {

            }
        };
    })


    //Change Calendar Views
    .directive('calendarView', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    $('#calendar').fullCalendar('changeView', attrs.calendarView);
                });
            }
        };
    })


.directive('popoverHtmlUnsafePopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
        template: '<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title" bind-html-unsafe="title" ng-show="title"></h3><div class="popover-content" bind-html-unsafe="content"></div></div></div>'
    };
})
.directive('popoverHtmlUnsafe', ['$tooltip', function ($tooltip) {
    return $tooltip('popoverHtmlUnsafe', 'popover', 'click');
}])


.directive('dropzone', function (ApiUrl) {

    return function (scope, element, attrs) {
        var method = element.attr('function');
        Dropzone.autoDiscover = false;
        Dropzone.options.myAwesomeDropzone = false;

        $(element).dropzone(
            {
                maxFiles: 1,
                maxfilesexceeded: function (file) {
                    this.removeAllFiles();
                    this.addFile(file);
                },
                url: ApiUrl,
                autoProcessQueue: false,
                complete: function (file)
                { },
                clickable: true,
                init: function () {
                    myDropzone = this;
                    myDropzone.on("addedfile", function (file) {
                        scope.SessionData.ImageUploaded = true;
                        var img = new Image();
                        img.src = window.URL.createObjectURL(file);
                        img.onload = function () {

                            var width = img.naturalWidth,
                                height = img.naturalHeight;

                            window.URL.revokeObjectURL(img.src);

                            if (width == 800 && height == 400) {
                            }
                            else {
                                scope.SessionData.Errorclass = 'c-red';
                            }
                        };

                        scope.$apply();

                    });

                }


            });

    };
})
.directive('whenScrollEnds', function () {
    return {
        restrict: "A",
        scope: {
            reportdata: '='
        },
        link: function (scope, element, attrs) {
            var visibleHeight = element.height();
            var threshold = 100;
            scope.$watch('reportdata', function (newValue, oldValue) {

                if (newValue !== oldValue) {
                    //var myScroll = element.getNiceScroll().resize();
                    var myScroll = element.niceScroll();
                    setTimeout(function () {




                        myScroll.scrollend(function () {

                            var scrollableHeight = element.prop('scrollHeight');
                            var hiddenContentHeight = scrollableHeight - visibleHeight;

                            if (hiddenContentHeight - element.scrollTop() <= threshold) {

                                scope.$apply(attrs.whenScrollEnds);
                            }
                            if (myScroll.scrollvaluemax == myScroll.scroll.y) {
                                scope.$apply(attrs.whenScrollEnds);
                            }
                        });
                    }, 500);
                }
            }, true);

        }

    };
})
.directive("customdropdown", function ($rootScope) {
    return {
        restrict: "E",
        templateUrl: 'Common/leftpanel/customdropdown.tpl.html',
        scope: {
            placeholder: "@",
            list: "=",
            selected: "=",
            property: "@",
            icon: "@"
        },
        link: function (scope, element, attr) {
            scope.listVisible = false;
            scope.isPlaceholder = true;
            scope.Isicon = scope.icon ? true : false;
            scope.select = function (item) {
                scope.isPlaceholder = false;
                scope.selected = item;
            };

            scope.isSelected = function (item) {
                if (scope.selected) {
                    return item[scope.property] === scope.selected[scope.property];
                }
            };

            scope.show = function () {
                scope.listVisible = true;
            };

            $rootScope.$on("documentClicked", function (inner, target) {
                var x = $(target[0]).is(".dropdown-display.clicked") ? false : true;
                var y = $(target[0]).parents(".dropdown-display.clicked").length > 0 ? false : true;
                if (x && y) {
                    scope.$apply(function () {
                        scope.listVisible = false;
                        $(".dropdown-list").removeClass("reverse");
                    });
                }
            });
            scope.$watch("selected", function (value) {
                if (scope.selected) {
                    scope.isPlaceholder = scope.selected[scope.property] === undefined;
                    scope.display = scope.selected[scope.property];
                }
            });
            element.click(function () {
                //console.log('elemet', element);
                // get the scollTop (distance scrolled from top)
                var scrollTop = $(window).scrollTop();
                // get the top offset of the dropdown (distance from top of the page)
                var topOffset = $(".dropdown-display").offset().top;
                // calculate the dropdown offset relative to window position
                var relativeOffset = topOffset - scrollTop;
                // get the window height
                var windowHeight = $(window).height();

                // if the relative offset is greater than half the window height,
                // reverse the dropdown.
                //console.log(scrollTop, topOffset, relativeOffset, windowHeight / 2);
                if (relativeOffset > windowHeight / 2) {
                    $(".dropdown-list").addClass("reverse");
                }
                else {
                    $(".dropdown-list").removeClass("reverse");
                }
            });
        }
    };
})
.directive("selectcustom", function ($rootScope, $timeout) {
    return {
        restrict: "C",
        require: "ngModel",
        scope: {
            ngModel: '='
        },
        link: function (scope, element, attr, ngModel) {
            attr.$observe('dynamicdata', function (value) {
                element.next('.dropdownjs').children('ul').children('li').remove();

                setTimeout(function () {
                    element.next('.dropdownjs').find('.fakeinput').val(element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').text());

                }, 1000);
            });
            $(element).dropdown();
            $timeout(function () {
                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (modelValue) {
                    if (modelValue !== null) {
                        element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                        element.next('.dropdownjs').find('.fakeinput').val(element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').text());
                        element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                        setTimeout(function () {
                            element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                            element.next('.dropdownjs').find('.fakeinput').val(element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').text());
                            element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                        }, 1000);
                    }
                });
                if (scope.ngModel !== null) {
                    element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                    element.next('.dropdownjs').find('.fakeinput').val($('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').text());
                    element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                }
            }, true);

        }
    };

})
    .directive("selectcustomspecial", function ($rootScope, $timeout) {
        return {
            restrict: "C",
            require: "ngModel",
            scope: {
                ngModel: '='

            },
            link: function (scope, element, attr, ngModel) {
                var DisplayText = "";
                attr.$observe('dynamicdata', function (value) {
                    element.next('.dropdownjs').children('ul').children('li').remove();
                    setTimeout(function () {
                        element.next('.dropdownjs').find('.fakeinput').val("");
                        $("#DisplayItem").remove();
                        DisplayText = _.filter(JSON.parse(element.attr("dynamicdata")), function (item) { return parseInt(item.Id, 10) === parseInt(element.val(), 10); })[0].DisplayItem;
                        $(element.next('.dropdownjs').find('.fakeinput')).before("<div id='DisplayItem'>" + DisplayText + "</div>");

                    }, 10);
                    $(element).dropdown();
                });
                $timeout(function () {
                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, function (modelValue) {
                        if (modelValue !== null) {
                            $("#DisplayItem").remove();
                            element.next('.dropdownjs').find('.fakeinput').val("");
                            DisplayText = _.filter(JSON.parse(element.attr("dynamicdata")), function (item) { return parseInt(item.Id, 10) === parseInt(element.val(), 10); })[0].DisplayItem;
                            $(element.next('.dropdownjs').find('.fakeinput')).before("<div id='DisplayItem'>" + DisplayText + "</div>");

                            element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                            element.next('.dropdownjs').find('.fakeinput').val("");
                            element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                            setTimeout(function () {
                                element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                                element.next('.dropdownjs').find('.fakeinput').val("");
                                element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                            }, 1000);
                        }
                    });
                    if (scope.ngModel !== null) {
                        $("#DisplayItem").remove();
                        DisplayText = _.filter(JSON.parse(element.attr("dynamicdata")), function (item) { return parseInt(item.Id, 10) === parseInt(element.val(), 10); })[0].DisplayItem;
                        $(element.next('.dropdownjs').find('.fakeinput')).before("<div id='DisplayItem'>" + DisplayText + "</div>");

                        element.next('.dropdownjs').children('ul').children('li').first().removeClass('selected').removeAttr('selected');
                        element.next('.dropdownjs').find('.fakeinput').val("");
                        element.next('.dropdownjs').children('ul').children('li[value="' + element.val() + '"]').addClass('selected').attr('selected', 'selected');
                    }
                }, true);

            }
        };

    })
   .directive("showEdit", function ($rootScope) {
       return {
           restrict: "A",
           scope: {
               canedit: '@',
               getedit: '='

           },
           link: function (scope, element, attr, ngModel) {


               element.on('mouseenter', function (event) {
                   scope.$apply(function () {
                       scope.getedit = {};
                       scope.getedit[scope.canedit] = true;

                   });
               });
               element.on('mouseleave', function (event) {
                   scope.$apply(function () {
                       scope.getedit = {};
                       scope.getedit[scope.canedit] = false;
                   });
               });

           }
       };
   })
.directive("selectcustommulti", function ($rootScope, $timeout) {

    return {
        restrict: "C",
        require: "ngModel",
        scope: {
            ngModel: '=',
            parammodel: '='
        },
        link: function (scope, element, attr, ngModel) {


            attr.$observe('dynamicdata', function (value) {
                // body

                if (value !== "" && value !== null && typeof value !== "undefined") {
                    var inputText = "";
                    $timeout(function () {
                        element.next('.dropdownjs').children('ul').children('li').remove();
                        $(element).dropdown();
                        if (scope.parammodel !== null) {
                            setTimeout(function () {
                                _.each(scope.parammodel, function (item) {
                                    element.next('.dropdownjs').children('ul').children('li[value="' + item.trim() + '"]').addClass("selected");
                                    element.next('.dropdownjs').children('ul').children('li[value="' + item.trim() + '"]').find("input").prop("checked", true);
                                    if (item.trim() === "All") {
                                        inputText = "All";
                                    }
                                    if (inputText.trim() !== "All") {
                                        if (inputText.trim() !== "") {
                                            inputText += ", " + element.next('.dropdownjs').children('ul').children('li[value="' + item.trim() + '"]').text().trim();

                                        }
                                        else {
                                            inputText = element.next('.dropdownjs').children('ul').children('li[value="' + item.trim() + '"]').text().trim();

                                        }
                                    }
                                });
                                element.next('.dropdownjs').find(".fakeinput").val(inputText);
                            }, 10);
                        }
                    }, 5000);
                }
            });
            scope.$watch(function () {
                return ngModel.$modelValue;
            }, function (newValue) {

                var newModelValue = element.next('.dropdownjs').find(".fakehiddeninput").val();
                var elmfirst;
                $timeout(function () {
                    if (newModelValue && newValue) {

                        scope.parammodel = [];
                        var checkAll = true;
                        if (newValue[0] === '0') {
                            var checkvalue = element.next('.dropdownjs').children('ul').children('li[value="0"]').find('input[type="checkbox"]').is(":checked");
                            if (checkvalue) {
                                element.next('.dropdownjs').children('ul').children('li[value="0"]').find('input[type="checkbox"]').prop("checked", true);

                                $timeout(function () {
                                    $(element.next('.dropdownjs').children('ul').children('li')).each(function () {
                                        $(this).find('input[type="checkbox"]').prop("checked", true);
                                        scope.parammodel.push($(this).attr("value").toString());

                                    });
                                }, true);
                                elmfirst = true;
                            }
                            else {
                                element.next('.dropdownjs').children('ul').children('li').find('input[type="checkbox"]').prop("checked", false);
                                setTimeout(function () {
                                    element.next('.dropdownjs').children('ul').children('li').first().next().find('input[type="checkbox"]').prop("checked", true);
                                }, 10);
                                newModelValue = element.next('.dropdownjs').children('ul').children('li').first().next().val();
                                scope.parammodel.push(newModelValue);

                                elmfirst = false;
                            }
                        }

                        else {

                            var status = $(element.next('.dropdownjs').children('ul').children('li[value="' + newValue + '"]').find('input[type="checkbox"]').is(":checked"));

                            $timeout(function () {
                                if (status[0]) {
                                    $(element.next('.dropdownjs').children('ul').children('li[value="' + newValue + '"]').find('input[type="checkbox"]').prop(":checked", true));

                                    setTimeout(function () {
                                        var totalchild = element.next('.dropdownjs').children('ul').children('li').find('input[type="checkbox"]').length - 1;

                                        var status = $(element.next('.dropdownjs').children('ul').children('li[value="' + newValue + '"]').find('input[type="checkbox"]').is(":checked"));

                                        if (elmfirst === false) {
                                            if ((parseInt(totalchild, 10) === parseInt(scope.parammodel.length, 10)) && status[0] === true) {

                                                element.next('.dropdownjs').children('ul').children('li').first().find('input[type="checkbox"]').prop("checked", true);
                                            }
                                        }

                                    }, 50);
                                }


                                else {

                                    $(element.next('.dropdownjs').children('ul').children('li[value="' + newValue + '"]').find('input[type="checkbox"]').prop(":checked", false));
                                    $timeout(function () {
                                        element.next('.dropdownjs').children('ul').children('li[value="0"]').find('input[type="checkbox"]').prop("checked", false);



                                    }, true);
                                    elmfirst = false;
                                }

                            }, 1500);
                        }


                    }
                }, true);

            });
        }
    };

})


 .directive('uparrow', function () {
     return {
         restrict: 'A',
         replace: true,
         template: ' <svg version="1.1" id="imgView" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="14px" viewBox="0 0 466.667 466.667" style="enable-background:new 0 0 466.667 466.667;" xml:space="preserve" class="detail convertSvgInline replaced-svg" data-id="24215" data-kw="upload38">' +
                                                  '<g>' +
                                            ' <path d="M423.571,176.43L256.904,9.764c-13.017-13.018-34.122-13.018-47.14,0L43.098,176.43c-13.018,13.018-13.018,34.123,0,47.141   c13.018,13.018,34.123,13.018,47.141,0l109.762-109.764v319.527c0,18.408,14.924,33.333,33.333,33.333   c18.409,0,33.333-14.925,33.333-33.333V113.807l109.764,109.764c6.509,6.508,15.039,9.762,23.57,9.762   c8.53,0,17.062-3.254,23.569-9.764C436.588,210.552,436.588,189.448,423.571,176.43z" fill="#777" />' +
                                               '</g>' +
                                                '</svg>'
     };
 })
     .directive('downarrow', function () {
         return {
             restrict: 'A',
             replace: true,
             template: '<svg version="1.1" id="imgView" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="14px" viewBox="0 0 448 448" style="enable-background:new 0 0 448 448;" xml:space="preserve" class="detail convertSvgInline replaced-svg" data-id="23670" data-kw="download41">' +
                                               '<g>' +
                                                '<path d="M41.373,278.627l160,160c12.496,12.497,32.758,12.497,45.255,0l160-160c12.495-12.497,12.495-32.758,0-45.255   c-12.497-12.497-32.759-12.497-45.256,0L256,338.745V32c0-17.673-14.327-32-32-32c-17.673,0-32,14.327-32,32v306.745   L86.627,233.372C80.379,227.124,72.189,224,64,224s-16.379,3.124-22.627,9.372C28.876,245.869,28.876,266.13,41.373,278.627z" fill="#777" />' +
                                           '</g>' +
                                           '</svg>'
         };
     })
.directive("closeheader", function () {
    return function (scope, element, attrs) {
        element.bind(window, "resize", function () {
            scope.CloseHeader();
        });
    };
})



.directive("showmanualpopover", function () {
    return {
        restrict: "A",
        scope: {
            param: "=",
            returnparam: '='
        },
        link: function (scope, element, attr) {
            element.mouseenter(function () {
                scope.returnparam = {};
                scope.returnparam[scope.param] = true;
                scope.$apply();
            });
            element.mouseleave(function () {
                scope.returnparam = {};
                scope.$apply();
            });
        }
    };
})
.directive('myFocus', function () {
    return {
        restrict: 'A',
        link: function postLink(scope, element, attrs) {
            if (attrs.myFocus === "") {
                attrs.myFocus = "focusElement";
            }
            scope.$watch(attrs.myFocus, function (value) {
                if (value === attrs.id) {
                    element[0].focus();
                }
            });
            element.on("blur", function () {
                scope[attrs.myFocus] = "";
                scope.$apply();
            });
        }
    };
}).directive('t', function () {
    return {
        restrict: 'E',
        transclude: true,
        template: "<translate ng-transclude></translate>"
    };
})







  .directive('notificationdesktopview', function () {
      return {
          link: function ($scope) {
          },
          templateUrl: 'Notifications/NotificationsDesktopView.tpl.html',
          scope: true,
          controller: 'NotificationsCtrl',
          restrict: 'E'
      };
  })



.directive('ngModelOnblur', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1, // needed for angular 1.2.x
        link: function (scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') {
                return;
            }

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function () {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
})
.directive('adminheaderfilters', function () {
    return {
        restrict: 'E',
        templateUrl: 'Common/adminheaderfilters.tpl.html'

    };

})
    .directive('companyinformation', function () {
        return {
            restrict: 'E',
            templateUrl: 'CreateCompany/CompanyInformation.tpl.html'
        };
    })
      .directive('companybasicinfo', function () {
          return {
              restrict: 'E',
              templateUrl: 'CreateCompany/CompanyBasics.tpl.html'
          };
      })

.directive('companydetails', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/CompanyDetails.tpl.html'
    };
})
.directive('companytreesetup', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/TreeSetup.tpl.html'
    };
})
.directive('companylanguage', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/CompanyLanguage.tpl.html'
    };
})
.directive('companymailingdetails', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/MailingDetails.tpl.html'
    };
})
.directive('companyteamlink', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/TeamlinkSetup.tpl.html'
    };
})
.directive('companyziplingo', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/ZiplingoSetup.tpl.html'
    };
})
.directive('companysecurity', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/SecuritySetup.tpl.html'
    };
})
.directive('calendarsetting', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/CalendarSettings.tpl.html'
    };
})
.directive('communitysetting', function () {
    return {
        restrict: 'E',
        templateUrl: 'CreateCompany/CommunitySettings.tpl.html'
    };
})
    .directive('companysettingcontrols', function () {
        return {
            restrict: 'E',
            templateUrl: 'CreateCompany/CompanySettingControls.tpl.html'
        };
    })
       .directive('widgetsettings', function () {
           return {
               restrict: 'E',
               templateUrl: 'CreateCompany/WidgetsSettings.tpl.html'
           };
       })
       .directive('reportcentersettings', function () {
           return {
               restrict: 'E',
               templateUrl: 'CreateCompany/ReportCenterSettings.tpl.html'
           };
       })
    .directive('shoppingcartconfiguration', function () {
           return {
               restrict: 'E',
               templateUrl: 'CreateCompany/ShoppingCartsConfig.tpl.html'
           };
       })
.directive('infoicon', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            displaytxt: "@"
        },
        link: function (scope, elm, attr, ngModelCtrl) {
            var htmlele = '<md-icon class="icon icon-16px icon-alert">' +
                    '<md-tooltip md-direction="top">' +
                        '' + scope.displaytxt + '' +
                    '</md-tooltip>' +
                '</md-icon>';
            elm.mouseenter(function () {
                elm.append($compile(htmlele)(scope));
            });
            elm.mouseleave(function () {
                $('md-icon').remove();
            });
          
        }
    };
})
.directive("checkvalidation", function ($rootScope, $timeout, $compile) {
    return {
        restrict: "A",
        scope: {
            validatetag: "@"
        },
        link: function (scope, element, attr) {
            attr.$observe('validatetag', function (value) {
                if (value) {
                    var values = value.split(',');
                    for (var i = 0; i < values.length; i++) {
                        element.attr(values[i], true);
                        var fn = $compile(element);
                    }
                }
            });
            return function (scope) {
                fn(scope);
            };
        }
    };

})
;