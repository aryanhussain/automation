/* Services */

angular.module('OUApp.services', ['ngResource'])

.service("countryModel", ['$window', function ($window) {

    var statesList_us = [{ "name": "Alabama", "code": "AL" },
    { "name": "Alaska", "code": "AK" },
    { "name": "Arizona", "code": "AZ" },
    { "name": "Arkansas", "code": "AR" },
    { "name": "California", "code": "CA" },
    { "name": "Colorado", "code": "CO" },
    { "name": "Connecticut", "code": "CT" },
    { "name": "Dist. Columbia", "code": "DC" },
    { "name": "Delaware", "code": "DE" },
    { "name": "Florida", "code": "FL" },
    { "name": "Georgia", "code": "GA" },
    { "name": "Hawaii", "code": "HI" },
    { "name": "Idaho", "code": "ID" },
    { "name": "Illinois", "code": "IL" },
    { "name": "Indiana", "code": "IN" },
    { "name": "Iowa", "code": "IA" },
    { "name": "Kansas", "code": "KS" },
    { "name": "Kentucky", "code": "KY" },
    { "name": "Louisiana", "code": "LA" },
    { "name": "Maine", "code": "ME" },
    { "name": "Maryland", "code": "MD" },
    { "name": "Massachusetts", "code": "MA" },
    { "name": "Michigan", "code": "MI" },
    { "name": "Minnesota", "code": "MN" },
    { "name": "Mississippi", "code": "MS" },
    { "name": "Missouri", "code": "MO" },
    { "name": "Montana", "code": "MT" },
    { "name": "Nebraska", "code": "NE" },
    { "name": "Nevada", "code": "NV" },
    { "name": "New Hampshire", "code": "NH" },
    { "name": "New Jersey", "code": "NJ" },
    { "name": "New Mexico", "code": "NM" },
    { "name": "New York", "code": "NY" },
    { "name": "North Carolina", "code": "NC" },
    { "name": "North Dakota", "code": "ND" },
    { "name": "Ohio", "code": "OH" },
    { "name": "Oklahoma", "code": "OK" },
    { "name": "Oregon", "code": "OR" },
    { "name": "Pennsylvania", "code": "PA" },
    { "name": "Rhode Island", "code": "RI" },
    { "name": "South Carolina", "code": "SC" },
    { "name": "South Dakota", "code": "SD" },
    { "name": "Tennessee", "code": "TN" },
    { "name": "Texas", "code": "TX" },
    { "name": "Utah", "code": "UT" },
    { "name": "Vermont", "code": "VT" },
    { "name": "Virginia", "code": "VA" },
    { "name": "Washington", "code": "WA" },
    { "name": "West Virginia", "code": "WV" },
    { "name": "Wisconsin", "code": "WI" },
    { "name": "Wyoming", "code": "WY" }
    ];


    var unitedstates = {
        "name": "United States", "code": "US", "id": "", "market": "usa", "languages": [{ "locale": "en_US", "name": "English", "id": "0" }, { "locale": "es_US", "name": "Espanol", "id": "1" }], "states": statesList_us   // US
    };

    this.list = [
        unitedstates
    ];
    this.init = function () {
        var selectedCountry = this.list.filter(function (element, index, array) {
            // check that country is at least minimally setup
            if (element.languages !== undefined) {
                array[index].active = true;
                element.active = true;
                return true;
            } else {
                return false;
            }
        });
        if (!$window.localStorage.getItem('country') && selectedCountry.length === 0) {
            // default to US
            selectedCountry = [korea];
            // unitedstates.active = true;
        } else if (selectedCountry.length === 0) {
            selectedCountry = [unitedstates];
            /*  country = $window.localStorage.getItem('country');
              thisCountry = $.grep(this.list, function(n,i){
                  return n.name === country;
              });
              thisCountry[0].active=true;
              selectedCountry=[thisCountry[0]];*/
        } else {
            selectedCountry = [unitedstates]; // selectedCountry;
        }
        this.selectedCountry = selectedCountry.shift();
    };

    this.countryLookup = function (country) {
        var selCountry;
        country = country.replace("-", " ");
        selCountry = $.grep(this.list, function (n, i) {
            return n.name.toLowerCase() === country.toLowerCase();
        });
        return selCountry[0];
    };

    this.setSelectedCountry = function (country) {
        this.selectedCountry.active = false;
        if (this.list.indexOf(country) > -1) {
            country.active = true;
            this.selectedCountry = country;
        }
    };
}])
.service('pendingRequests', function () {
    var pending = [];
    this.get = function () {
        return pending;
    };
    this.add = function (request) {
        pending.push(request);
    };
    this.remove = function (request) {
        pending = _.filter(pending, function (p) {
            return p.url.slice(p.url.lastIndexOf('/')) === request;
        });
        angular.forEach(pending, function (p) {
            p.canceller.resolve();
        });

    };
    this.cancelAll = function () {
        angular.forEach(pending, function (p) {
            console.log('cancelAll');
            p.canceller.resolve();
        });
    };
});