﻿angular.module('OUApp.validation-directives', ['pascalprecht.translate'])

.directive('validate', function ($translate) {
    //TODO: Create checkbox logic for enroller/sponsor ids...
    //TODO: Create animation when continue button is clicked..

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var formcompletecheck = true;
            var StopExecution = true;
            /*--Window Blur logic---*/

            /* Note: In Chrome - if focused on an input and then you blur out of the browser ie: Click on a different application - two blurs get called
            the input blur and window.blur - the following logic addresses this use case. A check for windowBlur happens in showErr() method below... */

            var windowBlur = false;
            $(window).blur(function () { windowBlur = true; });
            $(window).focus(function () { windowBlur = false; });



            element.focus(function () {
                //on focus remove all visible error states/msgs
                element.closest('.control-group').removeClass('has-error');
                element.closest('.control-group').find(".has-error").remove();
                element.find('state-error').remove();
                element.removeClass('state-error');
                element.removeClass('state-success');
                element.find('state-success').remove();

            });

            /*---Cancel Validation Logic--*/
            var cancelBox = element.closest(".controls").find('.cancel-validation');
            cancelBox.click(function () {

                element.toggleClass('novalidate');

                if ($(this).is(":checked")) {
                    element.closest('.control-group').removeClass('has-error');
                    element.closest('.control-group').find(".has-error").remove();
                    element.closest('.control-group').hide();
                    scope.$apply(attrs.updateId);

                }
                else {

                    element.closest('.control-group').show();

                }

            });


            element.blur(function () {
                StopExecution = false;
                checkForm(element, 0);
            });

            element.keypress("keydown keypress", function (e) {

                if (!!element.attr('inputspace')) {

                    var altkeypress;
                    if (e.altKey) {
                        altkeypress = true;
                    }
                    else {
                        if (altkeypress) {
                            altkeypress = false;
                            return false;
                        }
                    }

                    if (e.which === 32 || e.which === 33 || e.which === 34 || e.which === 35 || e.which === 36 || e.which === 37 || e.which === 38 || e.which === 39 || e.which === 40 || e.which === 41 || e.which === 42 || e.which === 43 || e.which === 44 || e.which === 46 || e.which === 47 || e.which === 45 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || e.which === 91 || e.which === 93 || e.which === 92 || e.which === 94 || e.which === 95 || e.which === 96 || e.which === 123 || e.which === 124 || e.which === 125 || e.which === 126) {
                        return false;
                    }
                }
                if (!!element.attr('inputapthash')) {
                    if (e.which === 32 || e.which === 33 || e.which === 34 || e.which === 36 || e.which === 37 || e.which === 38 || e.which === 39 || e.which === 40 || e.which === 41 || e.which === 42 || e.which === 43 || e.which === 44 || e.which === 46 || e.which === 47 || e.which === 45 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || e.which === 91 || e.which === 93 || e.which === 92 || e.which === 94 || e.which === 95 || e.which === 96 || e.which === 123 || e.which === 124 || e.which === 125 || e.which === 126) {
                        return false;
                    }
                }
                if (!!element.attr('inputmobiledash')) {
                    if (e.which === 32 || e.which === 33 || e.which === 34 || e.which === 35 || e.which === 36 || e.which === 37 || e.which === 38 || e.which === 39 || e.which === 40 || e.which === 41 || e.which === 42 || e.which === 43 || e.which === 44 || e.which === 46 || e.which === 47 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || e.which === 91 || e.which === 93 || e.which === 92 || e.which === 94 || e.which === 95 || e.which === 96 || e.which === 123 || e.which === 124 || e.which === 125 || e.which === 126) {
                        return false;
                    }
                }
                if (!!element.attr('inputmobilespacendash')) {
                    if (e.which === 33 || e.which === 34 || e.which === 35 || e.which === 36 || e.which === 37 || e.which === 38 || e.which === 39 || e.which === 40 || e.which === 41 || e.which === 42 || e.which === 43 || e.which === 44 || e.which === 46 || e.which === 47 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || e.which === 91 || e.which === 93 || e.which === 92 || e.which === 94 || e.which === 95 || e.which === 96 || e.which === 123 || e.which === 124 || e.which === 125 || e.which === 126) {
                        return false;
                    }
                }
                if (!!element.attr('inputspaceallow')) {
                    if (e.which === 33 || e.which === 34 || e.which === 35 || e.which === 36 || e.which === 37 || e.which === 38 || e.which === 39 || e.which === 40 || e.which === 41 || e.which === 42 || e.which === 43 || e.which === 44 || e.which === 46 || e.which === 47 || e.which === 45 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || e.which === 91 || e.which === 93 || e.which === 92 || e.which === 94 || e.which === 95 || e.which === 96 || e.which === 123 || e.which === 124 || e.which === 125 || e.which === 126) {
                        return false;

                    }
                }
                if (!!element.attr('inputonlynumber')) {
                    if ((e.which >= 32 && e.which <= 47) || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || (e.which <= 122 && e.which >= 65)) {
                        return false;
                    }
                }
                if (!!element.attr('numberwithcomma')) {

                    if ((e.which >= 32 && e.which <= 43) || e.which === 45 || e.which === 46 || e.which === 47 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || (e.which <= 122 && e.which >= 65)) {
                        return false;
                    }
                }
                if (!!element.attr('decimal')) {

                    if ((e.which >= 32 && e.which <= 45) || e.which === 45 || e.which === 46 || e.which === 47 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || (e.which <= 122 && e.which >= 65)) {
                        return false;
                    }
                }
                if (!!element.attr('decimalwithcommal')) {

                    if ((e.which >= 32 && e.which <= 45 && e.which <= 43) || e.which === 45 || e.which === 47 || e.which === 58 || e.which === 59 || e.which === 60 || e.which === 61 || e.which === 62 || e.which === 63 || e.which === 64 || (e.which <= 122 && e.which >= 65)) {
                        return false;
                    }
                }
                if (!!element.attr('nospaceallow')) {

                    if (e.which === 32) {
                        return false;
                    }
                }
            });

            var checkForm = function (element, type) {
                var msg, attr;
                var value = element.val();


                /*--attrs is not used because when the continue button is clicked attrs held the value of the button and not the element we passsed in - instead
                we check the old fashioned way with jQuery to see what validation needs to be done.--*/


                //enroller/dist validation --

                //to avoid calling hydra twice on browser blur we check that here

                if (!!element.attr('hydra-profile') && !windowBlur) {
                    var idCheck = $hydraService.dist2profileBean();

                    idCheck.set_complete_cb(function (bean) {
                        removeBusyWaiting(element);
                        if (bean.data.id === null) {
                            if (type === 0) {
                                showError('error_valid_id', element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    });

                    if (attrs.enrollerId) {
                        idCheck.updateInput(scope.account.enroller_id);
                    } else {
                        idCheck.updateInput(scope.account.sponsor_id);
                    }

                    addBusyWaiting(element);
                }

                if (!!element.attr('hydra-username') && !windowBlur) {
                    //console.log("call validation hydra user name");
                    var userCheck = $hydraService.VerifyUserName(scope.account.username);
                    userCheck.set_complete_cb(function (bean) {
                        //console.log("call validation hydra user name");
                        removeBusyWaiting(element);
                        var result = JSON.parse(bean.data.result);
                        //console.log("result", result);
                        if (result.IsValid === false) {
                            if (type === 0) {
                                showError('error_username_exists', element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        } else if (bean.error) {
                            if (type === 0) {
                                showError('error_processing', element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    });
                    //    userCheck.updateInput(scope.account.username);
                    addBusyWaiting(element);
                }

                if (!!element.attr('validate-username')) {
                    if (!value.match(/^[a-zA-Z0-9]*$/)) {
                        //if (!/^[a-zA-Z0-9]{6,}$/.test(element)) {
                        if (type === 0) {
                            showError('error_invalid_username', element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                }
                //not empty validation
                if (!!element.attr('noempty')) {
                    if ($.trim(value) === '') {
                        if (type === 0) {
                            showError('error_required_field', element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }
                //not empty validation
                if (!!element.attr('fakeinputnoempty')) {
                    if (element.attr("fakevalue") === '') {
                        if (type === 0) {
                            showError('error_required_field', element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }

                //valid postal validation
                if (!!element.attr("valid-postal")) {

                    /*Initially we want to work with the existing logic. This means grabbing
                    the current status of shipping/billing error models */

                    var validPostal;
                    /* depending on the value of attrs.validPostal determines which model(billing or shipping) to use */
                    if (attrs.validPostal === "billing") { validPostal = scope.postal2divisionsBean_billing.error; }
                    else if (attrs.validPostal === "shipping") { validPostal = scope.postal2divisionsBean_shipping.error; }

                    //When we click on continue btn sometimes validPostal would be set to null
                    if (validPostal) {
                        var pElement = element.closest('.control-group');

                        if (validPostal && !pElement.hasClass('has-error')) {
                            if (type === 0) {
                                showError('INVALID_POSTAL', element);
                            }
                            else {
                                formcompletecheck = false;
                            }

                        }
                        /*this section only runs if for any reason the value of validPostal( the current error status for billing/shipping) is null*/
                    } else {
                        var checkPostal = $hydraService.postal2divisionsBean();

                        checkPostal.set_complete_cb(function (bean) {
                            if (bean.error === "VERTEX_ADDRESS_NOT_FOUND") {
                                if (type === 0) {
                                    showError('VERTEX_ADDRESS_NOT_FOUND', element);
                                }
                                else {
                                    formcompletecheck = false;
                                }
                            }
                            else if (bean.error === "INVALID_POSTAL") {
                                if (type === 0) {
                                    showError('INVALID_POSTAL', element);
                                }
                                else {
                                    formcompletecheck = false;
                                }

                            }
                        });
                        checkPostal.updateInput(value);
                    }

                }

                //*****-***** style zip validation
                if (!!element.attr('valid_postal_usstyle')) {
                    if (!value.match(/^\d{5}([\-]?\d{4})?$/)) {
                        if (type === 0) {
                            showError('INVALID_POSTAL', element);

                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }

                //alphanumeric validation
                if (!!element.attr('alphanum')) {
                    if (!value.match(/^[a-zA-Z0-9_-]*$/)) {
                        if (type === 0) {
                            showError('error_alpha_num', element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }
                if (!!element.attr('ValidName')) {
                    if (value.match(/[*^|\":<>[\]{}\\()';?/~!,]/)) {
                        if (type === 0) {
                            showError('error_special_char', element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }

                //address validation
                if (!!element.attr('pipeChar')) {
                    if (value.match(/\|/)) {
                        if (type === 0) {
                            showError('error_pipe_char', element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }
                //address validation
                if (!!element.attr('validOneInTwo')) {
                    var field1 = $(element.attr('prevfield')).val();
                    if (!value && !field1) {
                        if (type === 0) {
                            showError('Please fill one of two emails', element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }

                //date validation
                if (!!element.attr('date')) {
                    value = $.trim(value);

                    var dateArray = value.split("/");

                    //Date and month correction
                    if (dateArray[0].length == 1) {
                        dateArray[0] = "0" + dateArray[0];
                    }
                    value = dateArray.join("/");
                    if (dateArray[1] !== undefined && dateArray[1].length == 1) {
                        dateArray[1] = "0" + dateArray[1];
                    }
                    if (dateArray[0] > 12) {
                        if (type === 0) {
                            showError("error_valid_date", element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else if (dateArray[1] > 31) {
                        if (type === 0) {
                            showError("error_valid_date", element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else if (isNaN(Date.parse(value))) {
                        if (type === 0) {
                            showError("error_valid_date", element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    element.val(value);
                }
                //integer only validation
                if (!!element.attr('numeric')) {
                    if (!value.match(/^\d*$/)) {
                        if (type === 0) {

                            showError("error_alpha_not_permitted", element);

                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }
                if (!!element.attr('phoneno')) {
                    if (!value.match(/^\d*$/)) {

                        if (type === 0) {
                            showError("only Numbers", element);
                            //showError("error_only_numbers", element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                }
                //min length validation
                if (!!element.attr('minlength')) {
                    attr = element.attr('minlength');
                    if (value) {
                        if (value.length < attr) {
                            if (type === 0) {

                                showError("error_minimum_chars", element, attr);

                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                        else {
                            showSuccess(element);
                        }
                    }
                }
                if (!!element.attr('pwminlength')) {
                    attr = element.attr('pwminlength');
                    if (value) {
                        if (value.length < attr) {
                            if (type === 0) {

                                showError("error_pwminimum_chars", element, attr);
                            }

                            else {
                                formcompletecheck = false;
                            }
                        }
                        else {
                            showSuccess(element);
                        }
                    }
                }
                //max length validation
                if (!!element.attr('validate-maxlength')) {
                    attr = element.attr('validate-maxlength');
                    if (value.length > attr) {
                        if (type === 0) {
                            showError("error_maxlength_chars", element, attr);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }
                //phone validation
                if (!!element.attr('phone')) {
                    attr = element.attr('phone');
                    var numbers = value.replace(/[^0-9]/g, "");
                    if (numbers.length < attr) {
                        if (type === 0) {
                            showError("error_minimum_digits", element, attr);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }
                //url validation
                if (!!element.attr('url')) {
                    if (value) {
                        validateUrl(value);
                    }
                }
                function validateUrl(url) {                    
                    var pattern = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                    if (pattern.test(url)) {
                        showSuccess(element);
                        return true;
                    } else {                        
                        showError("error_valid_url", element);
                        return false;
                    }
                }
                //email validation
                if (!!element.attr('email')) {
                    if (value) {
                        validateEmail(value);
                    }
                }
                function validateEmail(value) {
                    var filter = new RegExp('^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$');
                    if (filter.test(value)) {
                        showSuccess(element);
                    }
                    else {
                        if (type === 0) {
                            showError("error_valid_email", element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                }
                //email confirmation
                if (!!element.attr('confirmemail')) {
                    var value1 = value;
                    var value2 = $('.email1').val();
                    if (value1 !== value2) {
                        if (type === 0) {
                            showError("error_email_not_match", element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }
                //start with 0,1,2,3
                if (!!element.attr('startwith')) {
                    if (value) {

                        //  console.log('value.substring(1)', value.substring(0, 1));
                        if (!value.substring(0, 1).match('0') && !value.substring(0, 1).match('1') && !value.substring(0, 1).match('2') && !value.substring(0, 1).match('3')) {

                            if (type === 0) {
                                showError('error_Start_With', element);
                            }
                            else {
                                formcompletecheck = false;
                            }

                        }
                        else {
                            showSuccess(element);
                        }
                    }
                }
                if (!!element.attr('startwithzero')) {
                    if (value) {
                        //  console.log('value.substring(1)', value.substring(0, 1));
                        if (!value.substring(0, 1).match('0')) {
                            if (type === 0) {
                                showError('error_Start_WithZero', element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                        else {
                            showSuccess(element);
                        }
                    }
                }
                if (!!element.attr('password-match')) {
                    var currentPw = $('#txtPassword').val();
                    var pwStatus = $('.password-match');
                    if (element.val()) {
                        if (currentPw == element.val()) {
                            showSuccess(element);
                        } else {
                            if (type === 0) {
                                showError("password_not_match", element, attr);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    }
                }
                //no spaces
                if (!!element.attr('nospaces')) {
                    if (value.match(/\s/g)) {
                        if (type === 0) {
                            showError('error_spaces_not_permitted', element);

                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }
              

                //min Date Validation
                if (!!element.attr('mindate')) {

                    var min = $('#enddate').val(),
                    max = $('#startdate').val();

                    var year = min.slice(-4),
                        month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(min.substr(4, 3)) + 1,
                        day = min.substr(8, 2);
                    min = year + '-' + (month < 10 ? '0' : '') + month + '-' + day;
                    var year1 = max.slice(-4),
                       month1 = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(max.substr(4, 3)) + 1,
                       day1 = max.substr(8, 2);
                    max = year1 + '-' + (month1 < 10 ? '0' : '') + month1 + '-' + day1;


                    var test = moment(min).isAfter(max);



                    if (element.val()) {
                        if (min >= max) {
                            showSuccess(element);
                        } else {
                            if (type === 0) {
                                showError("End Date should be equal or greater than Start date", element, attr);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    }
                }

                if (!!element.attr('mindatetime')) {

                    var mindatetime, maxdatetime;
                    var endate = $('#enddate').val(),
                        startdate = $('#startdate').val(),
                        endtime = $('#mobileendtime').val(),
                        starttime = $('#mobilestartTime').val();
                    var timelength = "";
                    if (endtime.split(":").length > 0) {
                        timelength = endtime.split(":")[0];
                        if (timelength.length === 1) {
                            endtime = "0" + endtime;
                        }
                    }
                    if (starttime.split(":").length > 0) {
                        timelength = starttime.split(":")[0];
                        if (timelength.length === 1) {
                            starttime = "0" + starttime;
                        }
                    }
                    starttime = convertTo24Hour(starttime);
                    endtime = convertTo24Hour(endtime);

                    var minyear = endate.slice(-4),
                      minmonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                               'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(endate.substr(4, 3)) + 1,
                      minday = endate.substr(8, 2);
                    endate = minyear + '/' + (minmonth < 10 ? '0' : '') + minmonth + '/' + minday + ' ' + endtime;


                    var maxyear = startdate.slice(-4),
                     maxmonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(startdate.substr(4, 3)) + 1,
                     maxday = startdate.substr(8, 2);
                    startdate = maxyear + '/' + (maxmonth < 10 ? '0' : '') + maxmonth + '/' + maxday + ' ' + starttime;

                    if (element.val()) {
                        if (endate >= startdate) {
                            showSuccess(element);
                        }
                        else {
                            if (type === 0) {

                                if (element.attr('id') === 'mobileendtime') {
                                    showError("End Time should be equal or greater than Start Time", element, attr);
                                }
                                else {
                                    showError("Start Time should be equal or less than End Time", element, attr);
                                }
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    }
                }



           




                //select validation
                if (!!element.attr('validate-select')) {
                    value = element.find('option:selected').text();

                    if (value.match(/Select a/) || value === "") {
                        if (type === 0) {
                            showError('error_make_selection', element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                    else {
                        showSuccess(element);
                    }
                }
                if (!!element.attr('Katakana')) {
                    var katakanaRegx = new RegExp(/^[\u30a0-\u30ff]+$/);
                    if (value) {
                        if (!katakanaRegx.test(value)) {
                            if (type === 0) {
                                element.val('');
                                showError('error_katakana', element);
                            }
                            else {
                                formcompletecheck = false;
                            }

                        }
                        else {
                            showSuccess(element);
                        }
                    }

                }
                //checkbox validation
                if (!!element.attr('checkbox-validate')) {

                    if (!element.is(":checked")) {
                        if (type === 0) {
                            showError('checkbox_required', element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }
                //alphabatical only validation          
                if (!!element.attr('alfa')) {
                    if (value.match(/[0-9]/g)) {
                        if (type === 0) {
                            showError("error_number_not_permitted", element);
                        }
                        else {
                            formcompletecheck = false;
                        }

                    }
                    else {
                        showSuccess(element);
                    }
                }

                //cc date validation
                if (!!element.attr('ccdate')) {
                    dateValidate(element, attrs, type);

                }
                //regex validation
                if (!!element.attr('regxvalid')) {
                    if (value) {
                        //console.log('element.attr(regxvalidValue)', element.attr('regxvalidValue'));
                        regexValidate(value, element.attr('regxvalidValue'));
                    }

                }
                function regexValidate(value, regxExp) {
                    var filter = new RegExp(regxExp);
                    if (filter.test(value)) {
                        showSuccess(element);
                    }
                    else {
                        if (type === 0) {
                            showError("INVALID_FORMAT", element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                }
                if (!!element.attr('regxvalidPan')) {
                    if (value) {
                        regexValidatePan(value, element.attr('regxvalidValue'));
                    }
                }
                function regexValidatePan(value, regxExp) {
                    var filter = new RegExp(regxExp);
                    if (filter.test(value)) {
                        if (!StopExecution) {
                            scope.VerifySSN(value);
                        }
                    }
                    else {
                        if (type === 0) {
                            showError("INVALID_FORMAT", element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                }
                if (!!element.attr('ccvalid')) {
                    if (value) {
                        var valid = valid_credit_card(value);
                        if (!valid) {
                            if (type === 0) {
                                showError("error_invalid_cc", element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        }
                    }
                }
                function valid_credit_card(value) {
                    // accept only digits, dashes or spaces

                    if (/[^0-9-\s]+/.test(value)) { return false; }

                    // The Luhn Algorithm. It's so pretty.
                    var nCheck = 0, nDigit = 0, bEven = false;
                    //value = value.replace(/\D/g, "");

                    for (var n = value.length - 1; n >= 0; n--) {
                        var cDigit = value.charAt(n);
                        nDigit = parseInt(cDigit, 10);

                        if (bEven) {
                            if ((nDigit *= 2) > 9) { nDigit -= 9; }
                        }

                        nCheck += nDigit;
                        bEven = !bEven;
                    }

                    return (nCheck % 10) === 0;
                }

                //regex validation
                if (!!element.attr('regxvalidzip')) {
                    if (value) {
                        //console.log('element.attr(regxvalidValue)', element.attr('regxvalidValue'));
                        regexValidateZip(value, element.attr('regexcountry'));
                    }
                }
                function regexValidateZip(value, countrycode) {
                    var regxExp = getRegex(countrycode);
                    var filter = new RegExp(regxExp);
                    if (filter.test(value)) {
                        showSuccess(element);
                    }
                    else {
                        if (type === 0) {
                            showError("INVALID ZIP", element);
                        }
                        else {
                            formcompletecheck = false;
                        }
                    }
                }
                function getRegex(countryCode) {
                    var regex;
                    switch (countryCode) {
                        case "AR":
                            //  regex = /^([A-HJ-TP-Z]{1}\d{4}[A-Z]{3}|[a-z]{1}\d{4}[a-hj-tp-z]{3})$/;
                            regex = /^[a-zA-Z0-9 _.-]*$/;
                            break;
                        case "AT":
                        case "AU":
                        case "BE":
                        case "HU":
                        case "NZ":
                        case "PH":
                            regex = /^\d{4}$/;
                            break;
                        case "CR":
                        case "DE":
                        case "ES":
                        case "FR":
                        case "MX":
                        case "PE":
                            regex = /^\d{5}$/;
                            break;
                        case "PA":
                        case "RU":
                        case "EC":
                        case "SG":
                        case "IN":
                            regex = /^\d{6}$/;
                            break;
                        case "JP":
                            regex = /^\d{7}$/;
                            break;
                        case "CA":
                            regex = /^([ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]\d[ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz]\d)$/;
                            break;
                        case "CL":
                            regex = /^(\d{7}|\d{3}[-]\d{4})$/;
                            break;
                        case "KR":
                            regex = /^(\d{6}|\d{3}[-]\d{3})$/;
                            break;
                        case "NL":
                            regex = /^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$/;
                            break;
                        case "PT":
                            regex = /^\d{4}([\-]?\d{3})?$/;
                            break;
                        case "US":
                            regex = /^\d{5}([\-]?\d{4})?$/;
                            break;
                        case "GB":
                            // regex = /^([g][i][r][0][a][a])$|^((([a-pr-uwyz]{1}([0]|[1-9]\d?))|([a-pr-uwyz]{1}[a-hk-y]{1}([0]|[1-9]\d?))|([a-pr-uwyz]{1}[1-9][a-hjkps-uw]{1})|([a-pr-uwyz]{1}[a-hk-y]{1}[1-9][a-z]{1}))(\d[abd-hjlnp-uw-z]{2})?)$/i;
                            //  regex = /^ ?(([BEGLMNSWbeglmnsw][0-9][0-9]?)|(([A-PR-UWYZa-pr-uwyz][A-HK-Ya-hk-y][0-9][0-9]?)|(([ENWenw][0-9][A-HJKSTUWa-hjkstuw])|([ENWenw][A-HK-Ya-hk-y][0-9][ABEHMNPRVWXYabehmnprvwxy])))) ?[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}$/;
                            //   regex = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([AZa-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/;
                            regex = /^([A-Pa-pR-Ur-uWYZwyz](([0-9](([0-9]|[A-Ha-hJKSTUWjkstuw])?)?)|([A-Ha-hK-Yk-y][0-9]([0-9]|[ABEHMNPRVWXYabehmnprvwxy])?)) [0-9][ABabD-Hd-hJLNjlnP-Up-uW-Zw-z]{2})|GIRgir 0AAaa$/;

                            break;
                        default:
                            regex = /^[a-zA-Z0-9 _.-]*$/;
                            break;
                    }
                    return regex;
                }


            };

            var showError = function (msg, element, value) {
                if (!element.hasClass('novalidate') && !windowBlur) {
                    if (element.closest('.control-group').find('span').hasClass('has-error')) {
                    }
                    else {
                        //create and display error message
                        element.removeClass('state-success');
                        element.addClass('state-error');
                        var container = $("<small />");
                        var nextEl = element.next();

                        msg = $translate(msg, { value: value });
                        container.text(msg).addClass('has-error help-block ng-scope').attr('translate', '');

                        if (element.hasClass('errNextElem')) {
                            if (element.parent().hasClass('input-group')) {
                                element.parent().after(container);
                            }
                            else {
                                nextEl.after(container);
                            }
                        }
                        else if (element.hasClass('errNextToText')) {
                            var textElem = nextEl.next(".textElem");
                            textElem.after(container);
                        }
                        else { element.after(container); }

                        element.closest(".control-group").addClass('has-error');
                    }
                }
            };
            var showSuccess = function (element, value) {
                if (!element.hasClass('novalidate') && !windowBlur) {
                    if (element.closest('.control-group').find('span').hasClass('has-error')) {
                    }
                    else {
                        //create and display error message 
                        element.removeClass('state-error');
                        element.addClass('state-success');
                    }
                }
            };

            if (attrs.triggerCheck) {
                element.click(function (ev) {
                    StopExecution = true;
                    ev.preventDefault();
                    //remove any visible errors
                    $('.control-group.has-error').not('.ccvalidation, .sumlength').removeClass('has-error');
                    $('.control-group').not('.ccvalidation, .sumlength').find(".has-error").remove();

                    scope.$apply(attrs.pvCheck);

                    //grab all inputs set for validation minus the button triggering the check
                    var inputs = $("[validate]").not("input[trigger-check]").not('.novalidate');

                    //loop through inputs and send them through checkForm
                    inputs.each(function (i, e) {
                        var element = $(e);
                        checkForm(element, 0);
                    });

                    //special pw check that happens only on btn click and if pw field exists
                    if ($("#password1").length > 0) {
                        var pw1 = $("#password1");
                        var pw2 = $("#password2");
                        var ctrlGrps = pw1.closest('.controls-row').find('.control-group');
                        if (pw1.val() !== pw2.val()) {
                            ctrlGrps.addClass('has-error');
                            $(".password-match").text("No Match").addClass('has-error');

                        }
                    }
                    var errors = $(".control-group.has-error");
                    if (errors.length === 0) {

                        scope.$apply(function () {

                            if (scope.account) {

                                $.each(scope.account, function (i, e) {

                                    scope.order.profile[i] = scope.account[i];
                                    scope.order.payment[i] = scope.order.payment[i];
                                    // scope.order.shipping[i]=scope.shipping[i];
                                });
                            }

                            if (scope.payment) {

                                $.each(scope.payment, function (i, e) {
                                    scope.order.payment[i] = scope.payment[i];
                                });

                                $.each(scope.shipping, function (i, e) {
                                    scope.order.shipping[i] = scope.shipping[i];
                                });
                            }

                            if (scope.signature) {
                                scope.order.signature = scope.signature;
                            }

                        });

                        scope.$apply(attrs.success);
                    } else {
                        var target = $(".control-group.has-error")[0];
                        $('html, body').animate({
                            scrollTop: $(target).offset().top
                        }, 1000); //animate scroll to first error
                    }
                });
            }
            if (attrs.silenttriggerCheckblockwise) {
                element.on('keyup blur change', function (ev) {
                    StopExecution = true;
                    formcompletecheck = true;
                    ev.preventDefault();
                    scope.$apply(attrs.pvCheck);

                    //grab all inputs set for validation minus the button triggering the check
                    var sectiontype = attrs['sectiontype'] || null;
                    if (sectiontype && sectiontype !== undefined && sectiontype !== 'undefined' && sectiontype !== null && sectiontype !== '') {
                        //grab all inputs set for validation minus the button triggering the check
                        var inputs = $('[' + sectiontype + ']').not("input[trigger-checksection]").not('.novalidate');
                        //console.log('sectiontype', inputs);
                        //loop through inputs and send them through checkForm
                        inputs.each(function (i, e) {
                            var element = $(e);
                            checkForm(element, 1);
                        });

                        //special pw check that happens only on btn click and if pw field exists
                        if ($("#password1").length > 0) {
                            var pw1 = $("#password1");
                            var pw2 = $("#password2");
                            var ctrlGrps = pw1.closest('.controls-row').find('.control-group');
                            if (pw1.val() !== pw2.val()) {
                                ctrlGrps.addClass('has-error');
                                $(".password-match").text("No Match").addClass('has-error');

                            }
                        }
                        //console.log('formcompletecheck', formcompletecheck);
                        var errors = $(".control-group.has-error");
                        if (formcompletecheck) {
                            scope.ChangeOpacity(sectiontype, true);
                        } else {
                            scope.ChangeOpacity(sectiontype, false);
                        }
                        //$('.control-group.error').not('.ccvalidation, .sumlength', element.closest('.control-group.error')).removeClass('error');
                        //$('.control-group').not('.ccvalidation, .sumlength', element.closest('.control-group')).find(".error").remove();
                        //checkForm(element);
                    }
                });
            }
            if (attrs.triggerChecksection) {
                element.click(function (ev) {
                    StopExecution = true;
                    ev.preventDefault();
                    //remove any visible errors
                    $('.control-group.has-error').not('.ccvalidation, .sumlength').removeClass('has-error');
                    $('.control-group').not('.ccvalidation, .sumlength').find(".has-error").remove();

                    scope.$apply(attrs.pvCheck);
                    var sectiontype = attrs['sectiontype'];

                    //grab all inputs set for validation minus the button triggering the check
                    var inputs = $('[' + sectiontype + ']').not("input[trigger-checksection]").not('.novalidate');

                    //loop through inputs and send them through checkForm
                    inputs.each(function (i, e) {
                        var element = $(e);
                        checkForm(element, 0);
                    });

                    //special pw check that happens only on btn click and if pw field exists
                    if ($("#password1").length > 0) {
                        var pw1 = $("#password1");
                        var pw2 = $("#password2");
                        var ctrlGrps = pw1.closest('.controls-row').find('.control-group');
                        if (pw1.val() !== pw2.val()) {
                            ctrlGrps.addClass('has-error');
                            $(".password-match").text("No Match").addClass('has-error');

                        }
                    }


                    var errors = $(".control-group.has-error");
                    if (errors.length === 0) {
                        scope.$apply(attrs.success);

                    } else {
                        var target = $(".control-group.has-error")[0];
                        setTimeout(angular.bind(this, function () {
                            //console.log("has-error Position", $(target).offset().top);
                            $('html, body').animate({
                                scrollTop: $(target).offset().top
                            }, 1000);
                        }));//animate scroll to first error
                        scope.$apply(attrs.has-error);
                    }
                });
            }
            var addBusyWaiting = function (element) {
                var bw = '<p class="help-inline busywaiting"><img src="assets/img/spinner.gif"></p>';
                element.after(bw);
            };

            var removeBusyWaiting = function (element) {
                element.parent().find('.busywaiting').remove();
            };


            var dateValidate = function (element, attrs, type) {
                var selMonth, selYear, yySel, mmSel;

                /*---Set values for Month/Year - always used---*/
                var today = new Date();
                var thisYear = today.getYear() + 1900;
                var thisMonth = today.getMonth() + 1;
                if (thisMonth < 10) { thisMonth = 0 + thisMonth; }



                /*-----Set values for conditional variables-----*/


                /*if yy-sel attr we blurred off month...
                 yy-sel attr contains selector string for year select*/
                if (!!element.attr('yy-sel')) {
                    yySel = element.attr('yy-sel');
                    selYear = parseInt($(yySel).val(), 10);
                    selMonth = element.val();
                }

                /*if mm-sel attr we blurred off year...
                 mm-sel attr contains selector string for year select */
                if (!!element.attr('mm-sel')) {
                    mmSel = element.attr('mm-sel');
                    selMonth = parseInt($(mmSel).val(), 10);
                    selYear = element.val();
                }


                /*-----has-error handling Logic----*/
                //if blurring off month and selYear === thisYear
                if (selYear && selMonth) {
                    if ((selYear <= thisYear)) {
                        $(mmSel).closest('.control-group').removeClass('has-error');
                        $(mmSel).next('.has-error').remove();
                        $(yySel).closest('.control-group').removeClass('has-error');
                        $(yySel).next('.has-error').remove();
                        //get selected month value from element.val()
                        //selMonth = element.val();

                        if (selMonth < thisMonth || selYear < thisYear) {

                            if (type === 0) {
                                showError('Expiration dates cannot be in the past', element);
                            }
                            else {
                                formcompletecheck = false;
                            }
                        } else {
                            //when you blur off the year and selYear is > thisYear then remove any errors
                            $(mmSel).closest('.control-group').removeClass('has-error');
                            $(mmSel).next('.has-error').remove();
                        }

                        //if blurring off year and selMonth < thisMonth
                    } else if ((selMonth < thisMonth)) {
                        $(mmSel).closest('.control-group').removeClass('has-error');
                        $(mmSel).next('.has-error').remove();
                        //get selected year from element.val()
                        //selYear = parseInt(element.val(), 10);

                        //if selYear === thisYear then...
                        if (selYear <= thisYear) {
                            /*...check to make sure we don't already have an error displayed
                            if we dont throw error... */
                            if (!$(mmSel).closest('.control-group').hasClass('has-error')) {
                                if (type === 0) {
                                    showError('Expiration dates cannot be in the past', $(mmSel));
                                }
                                else {
                                    formcompletecheck = false;
                                }
                            }
                        } else {
                            //when you blur off the year and selYear is > thisYear then remove any errors
                            $(mmSel).closest('.control-group').removeClass('has-error');
                            $(mmSel).next('.has-error').remove();
                        }

                    }
                }

            };
            function convertTo24Hour(time) {
                var hours = time.substr(0, 2);
               
                if (time.indexOf('AM') != -1 && hours == 12) {
                    time = time.replace('12', '0');
                }
                if (time.indexOf('PM') != -1 && hours < 12) {
                    time = time.replace(hours, (parseInt(hours,10) + 12));
                   
                }
                return time.replace(/(AM|PM)/, '');
            }
        }

    };
})

.directive('ccvalidation', function ($translate) {
    return function (scope, element, attrs) {
        var wrapper = element.parent();

        element.focus(function () {
            element.addClass('active');
            wrapper.removeClass('has-error').addClass('success');
            wrapper.find('.has-error').remove();
        });

        element.blur(function () {
            ccvalidate(element);
        });


        var ccvalidate = function (element) {
            var elementvalue = element.val();
            if (elementvalue !== '9696') {
                var result = valid_credit_card(elementvalue);
                if (!result) {
                    if ((!wrapper.hasClass('has-error') && !element.next().hasClass('has-error')) && element.hasClass('active')) {
                        wrapper.removeClass('success').addClass('has-error');
                        msg = $translate("error_invalid_cc");
                        msgSpan = '<span class="has-error help-block">' + msg + "</span>";
                        element.after(msgSpan);
                    }
                }
                else {
                    wrapper.removeClass('has-error').addClass('success');
                    wrapper.find('.has-error').remove();
                }

                //if (result.card_type) {
                //    var card = "img." + result.card_type.name;
                //    $(card).addClass('selected');
                //} else {
                //    $("img.cc.selected").removeClass('selected');
                //}

            }
            else {
                wrapper.removeClass('has-error').addClass('success');
                wrapper.find('.has-error').remove();
            }
        };

        function valid_credit_card(value) {
            // accept only digits, dashes or spaces

            if (/[^0-9-\s]+/.test(value)) { return false; }

            // The Luhn Algorithm. It's so pretty.
            var nCheck = 0, nDigit = 0, bEven = false;
            //value = value.replace(/\D/g, "");

            for (var n = value.length - 1; n >= 0; n--) {
                var cDigit = value.charAt(n);
                nDigit = parseInt(cDigit, 10);

                if (bEven) {
                    if ((nDigit *= 2) > 9) { nDigit -= 9; }
                }

                nCheck += nDigit;
                bEven = !bEven;
            }

            return (nCheck % 10) === 0;
        }
    };



})

.directive('sumLength', function ($timeout, $translate) {
    return function (scope, element, attrs) {
        var combinedInputs = element.attr('combined-inputs');

        $(function () {

            $timeout(function () {

                if (element.val() !== "") {
                    checkLength(element);
                }

            }, 10);
        });

        element.blur(function () {
            checkLength($(this));
        });

        element.focus(function () {
            $(combinedInputs).closest('.control-group').removeClass('has-error');
            $(combinedInputs).closest('.control-group').find(".has-error").remove();

        });

        var checkLength = function (element) {
            var otherInput, totalLength, maxLegnth, input1, input2;
            maxLength = element.attr("sum-length-max");

            var inputArr = combinedInputs.split(",");
            input1 = $(inputArr[0]);
            input2 = $(inputArr[1]);
            totalLength = input1.val().length + input2.val().length;

            //if we've already displayed an error message on page load 
            //we don't want to do it again

            if (totalLength > maxLength && !$(combinedInputs).next().hasClass('has-error')) {
                var msg = $translate("error_sumLength", { value: maxLength });
                var container = $("<span />");
                container.addClass('help-inline has-error').text(msg);
                $(combinedInputs).after(container);
                $(combinedInputs).closest('control-group').addClass('has-error');
            }
        };
    };
})

.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input. 
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue === undefined) { return ''; }
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput !== inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
})

;
