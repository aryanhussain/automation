/* Filters */

angular.module('OUApp.filters', [])

.filter('moment', [
    function () {
        return function (date, method) {
            var localtime = moment.utc(date).toDate();
            var momented = moment(localtime);
            if (moment().diff(localtime, 'hours') > 23) {
                return localtime = moment(localtime).format('MM-DD-YYYY');
            }

            else {
                return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
            }


        };
    }
])
.filter('momenthours', [
    function () {
        return function (date, method) {
            var localtime = moment.utc(date).toDate();
            var momented = moment(localtime);
            return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
        };
    }
])
.filter('unique', function () {
    return function (collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function (item) {
            var key = item[keyname];
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
})
.filter('mask', function () {
    return function (text, cc) {
        if (!angular.isDefined(text)) {
            return text;
        }
        var len = text.length;
        var str = "•";

        if (cc) { len = len - 4; }

        for (var i = 1; i < len; i++) {
            str += "•";
        }

        if (cc) {
            str += text.slice(-4);
        }
        return str;
    };
})
 .filter('ccmask', function () {
     return function (cc) {
         if (!cc) { return ''; }

         var mask_length = cc.length - 4;
         var mask = "";
         for (var i = 0; i < mask_length; i++) {
             mask += '*';
         }
         lastDigits = cc.slice(cc.length - 4);
         return (mask + lastDigits);
     };
 })
.filter('unsafe', function ($sce) {
    return $sce.trustAsHtml;
})
.filter('translateFilter', function ($translate) {
    return function (value) {
        return $translate(value);
    };
})
.filter('customdate', function () {
    return function (input) {
        if (angular.isDefined(input)) {
            if (input.length >= 10) {
                input = input.slice(0, 10);
                input = input.slice(0, 4) + '/' + input.slice(5, 7) + '/' + input.slice(8, 10);
            }
        }
        return input;
    };
})
.filter('customdateMili', function () {
    return function (input) {

        var date = "";
        var dateString = input + '';
        if (Math.abs(parseInt(dateString.substr(6), 10)) > 0) {
            date = new Date(parseInt(dateString.substr(6), 10));
        }
        else {
            date = '--';
        }
        return date;
    };
})
.filter('stringtodate', function () {
    return function (input, format) {

        if (input) {
            input = new Date(input);
        }
        return input;
    };
})
.filter('translateFilter', function ($translate) {
    return function (value) {
        return $translate(value);
    };
})
.filter('isempty', function () {
        return function (value, replaceBy) {
            if (value) {
                return value;
            }
            else {
                return replaceBy;
            }
        };

    })
.filter('stringfilter', function () {
        return function (input) {
            return input.toString().trim();

        };

    })
 .filter('datestring', function () {
     return function (input) {
         var d = new Date(input);
         return d.toDateString();

     };

 })
 .filter('takechar', function () {
     console.log('takechar');
     return function (input, numChar) {
         if (!input) {
             return "";
         }

         var shortData = input.substring(0, numChar);
         if (input.length > numChar) {
             shortData = shortData + "....";
         }

         return shortData;
     };
 })
.filter('currencysymbol', function () {
    return function (text) {
        var str = "$" + text;
        return str;
    };
})
;
