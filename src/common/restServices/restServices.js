﻿angular.module('RestService', [
]).factory('$RestService', [
    '$http', 'ApiUrl', '$q', 'pendingRequests', '$window', '$state', '$stateParams',
function ($http, ApiUrl, $q, pendingRequests, $window, $state, $stateParams) {

    return {

        CreateUserAndSite: function (request) {
            var Requesturl = ApiUrl + "/CreateUserAndSite";
            return this.Post(Requesturl, request);
        },
        Token: function (request) {
            var Requesturl = ApiUrl + "/token";
            return this.Post(Requesturl, request);
        },
        Authentication: function (request) {
            var Requesturl = ApiUrl + "/Authentication";
            return this.Post(Requesturl, request);
        },
        ChangePassword: function (request) {
            var Requesturl = ApiUrl + "/api/Account/ChangePassword";
            return this.PostWithoutLogin(Requesturl, request);
        },
        GetReportsAll: function (request) {
            var Requesturl = ApiUrl + "/api/Report/GetAll?" + request;
            return this.Get(Requesturl, '');
        },
        GetReportDetails: function (request) {
            var Requesturl = ApiUrl + "/api/Report/GetReport?" + request;
            return this.Get(Requesturl, '');
        },
        UpdateReportDetails: function (request) {
            var Requesturl = ApiUrl + "/api/Report/UpdateDetails";
            return this.Post(Requesturl, request);
        },
        User_AddUser: function (request) {
            var Requesturl = ApiUrl + "/api/User/AddUser";
            return this.Post(Requesturl, request);
        },
        User_GetUser: function (request) {
            var Requesturl = ApiUrl + "/api/User/GetUser?" + request;
            return this.Get(Requesturl, '');
        },
        User_GetUsers: function (request) {
            var Requesturl = ApiUrl + "/api/User/GetUsers?" + request;
            return this.Get(Requesturl, '');
        },
        Notification_GetNotifications: function (request) {
            var Requesturl = ApiUrl + "/api/Notification/Get?" + request;
            return this.Get(Requesturl, '');
        },
        Notification_AddUpdate: function (request) {
            var Requesturl = ApiUrl + "/api/Notification/AddUpdate";
            return this.Post(Requesturl, request);
        },
        Notification_Queue: function (request) {
            var Requesturl = ApiUrl + "/api/Notification/Queue";
            return this.Post(Requesturl, request);
        },
        Notification_ProcessQueue: function () {
            var Requesturl = ApiUrl + "/api/Notification/ProccesQueue";
            return this.Post(Requesturl, {});
        },
        Notification_Vaccinations: function () {
            var Requesturl = ApiUrl + "/api/Notification/Vaccinations";
            return this.Post(Requesturl, {});
        },
        GetVaccinationsAll: function (request) {
            var Requesturl = ApiUrl + "/api/Vaccination/GetAll?" + request;
            return this.Get(Requesturl, '');
        },
        GetVaccinationDetails: function (request) {
            var Requesturl = ApiUrl + "/api/Vaccination/GetVaccination?" + request;
            return this.Get(Requesturl, '');
        },
        UpdateVaccinationDetails: function (request) {
            var Requesturl = ApiUrl + "/api/Vaccination/UpdateDetails";
            return this.Post(Requesturl, request);
        },
        GetBodyParts: function (request) {
            var Requesturl = ApiUrl + "/api/BodyPart";
            return this.Get(Requesturl, '');
        },
        GetCountries: function () {
            var Requesturl = ApiUrl + "/api/Country";
            return this.Get(Requesturl, '');
        },
        GetCities: function (request) {
            var Requesturl = ApiUrl + "/api/City?" + request;
            return this.Get(Requesturl, '');
        },
        GetSpecializations: function (request) {
            var Requesturl = ApiUrl + "/api/Specialization";
            return this.Get(Requesturl, '');
        },
        GetAilments: function (request) {
            var Requesturl = ApiUrl + "/api/Ailment";
            return this.Get(Requesturl, '');
        },
        GetChatsAll: function (request) {
            var Requesturl = ApiUrl + "/api/Chat/GetAll?" + request;
            return this.Get(Requesturl, '');
        },
        Post: function (ResultUrl, RequestParameterData) {
            var canceller = $q.defer();
            pendingRequests.add({
                url: ResultUrl,
                canceller: canceller
            });
            var refreshToken = $window.localStorage.getItem('refreshToken');
            var tokenExpiretime = new Date($window.localStorage.getItem('tokenExpiretime'));
            var ExpireTime = new Date(tokenExpiretime);
            ExpireTime.setMinutes(ExpireTime.getMinutes() + (-1));
            if (new Date() > ExpireTime && tokenExpiretime && $window.localStorage.getItem('tokenExpiretime') !== "null" && $window.localStorage.getItem('tokenExpiretime') !== null) {
                pendingRequests.remove("/token");
                var request = {
                    client_id: 'swasthyWebApp',
                    grant_type: "refresh_token",
                    refresh_token: refreshToken
                };
                return this.PostLogin(ApiUrl + "token", request);
            }
            else {
                var Authorization = null;
                var headers = null;
                
                var token = $window.localStorage.getItem('accessToken');
                if (token !== 'null' && token) {
                    headers = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Authorization': 'Bearer ' + token };
                }
                else {
                    headers = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' };

                }
                var PostResponse = $http({
                    url: ResultUrl,
                    method: "POST",
                    dataType: "",
                    headers: headers,
                    data: typeof (RequestParameterData) === "string" ? RequestParameterData : $.param(RequestParameterData),
                    timeout: canceller.promise
                }).success(function (data) {
                   
                    if (data) {
                        if (data.Status) {
                            if (parseInt(data.Status, 10) === 2) {
                                $rootScope = $scope = {};
                                $window.localStorage.setItem('refreshToken', null);
                                $window.localStorage.setItem('tokenExpiretime', null);
                                $window.localStorage.setItem('KeepSignedIn', false);
                                $window.localStorage.setItem('CustomerInfo', JSON.stringify({}));
                                $window.localStorage.setItem('SessionData', undefined);
                                sessionStorage.clear();
                                window.location.href = 'index.html';
                                return false;
                            }
                        }
                    }
                }).error(function (error, status) {
                   
                    if (parseInt(status, 10) === 401) {
                        $rootScope = $scope = {};
                        $window.localStorage.setItem('refreshToken', null);
                        $window.localStorage.setItem('tokenExpiretime', null);
                        $window.localStorage.setItem('KeepSignedIn', false);
                        sessionStorage.clear();
                        window.location.href = 'index.html';
                        return false;
                    }
                });
                return PostResponse;
            }
        },

        PostLogin: function (ResultUrl, RequestParameterData) {
            try {
                var canceller = $q.defer();
                pendingRequests.add({
                    url: ResultUrl,
                    canceller: canceller
                });

                
                var PostResponse = $http({
                    url: ResultUrl,
                    method: "POST",
                    dataType: "",
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    data: $.param(RequestParameterData),
                    timeout: canceller.promise
                }).success(function (data) {
                  
                    try {
                        //Status    Success = 0, Failed = 1, SessionExpired = 2, AuthKeyRequired = 3    
                        if (data.refresh_token) {
                            if (data.Message) {
                                if (data.Message.search("denied") > 0) {
                                    $rootScope = $scope = {};
                                    $window.localStorage.setItem('accessToken', null);
                                    $window.localStorage.setItem('refreshToken', null);
                                    $window.localStorage.setItem('tokenExpiretime', null);
                                    $window.localStorage.setItem('KeepSignedIn', false);
                                    $window.localStorage.setItem('CustomerInfo', JSON.stringify({}));
                                    sessionStorage.clear();
                                    $window.localStorage.setItem('SessionData', undefined);
                                    //window.location.href = 'index.html';
                                    return false;
                                }
                                else {
                                    $window.localStorage.setItem('accessToken', data.access_token);
                                    $window.localStorage.setItem('refreshToken', data.refresh_token);
                                    $window.localStorage.setItem('tokenExpiretime', data['.expires']);

                                    $state.transitionTo($state.current, $stateParams, {
                                        reload: true,
                                        inherit: false,
                                        notify: true
                                    });
                                }
                            }
                            else {

                                $window.localStorage.setItem('accessToken', data.access_token);
                                $window.localStorage.setItem('refreshToken', data.refresh_token);
                                $window.localStorage.setItem('tokenExpiretime', data['.expires']);

                                $state.transitionTo($state.current, $stateParams, {
                                    reload: true,
                                    inherit: false,
                                    notify: true
                                });
                            }
                        }
                        else if (data.error === "invalid_grant") {
                           
                            $rootScope = $scope = {};
                            $window.localStorage.setItem('accessToken', null);
                            $window.localStorage.setItem('refreshToken', null);
                            $window.localStorage.setItem('tokenExpiretime', null);
                            $window.localStorage.setItem('KeepSignedIn', false);
                            $window.localStorage.setItem('CustomerInfo', JSON.stringify({}));
                            sessionStorage.clear();
                            $window.localStorage.setItem('SessionData', undefined);
                            //window.location.href = 'index.html';
                            return false;
                        }
                        else {

                        }
                    }
                    catch (ex) {
                        console.log("ex", ex);
                    }
                }).error(function (error) {
                    if (error) {
                        //if (status === "invalid_grant") {
                        $rootScope = $scope = {};
                        $window.localStorage.setItem('accessToken', null);
                        $window.localStorage.setItem('refreshToken', null);
                        $window.localStorage.setItem('tokenExpiretime', null);
                        $window.localStorage.setItem('KeepSignedIn', false);
                        $window.localStorage.setItem('CustomerInfo', JSON.stringify({}));
                        sessionStorage.clear();
                        $window.localStorage.setItem('SessionData', undefined);

                        //window.location.href = 'index.html';
                        return false;
                        //}
                    }
                });
                if (PostResponse.Message === "SessionExpired") {
                    $location.path('/Login');
                }
                else {
                    PostResponse['finally'](function () {
                        pendingRequests.remove(url);
                    });
                    return PostResponse;
                }
            }
            catch (ex) {
                console.log("ex", ex);

            }
        },

        Get: function (ResultUrl) {
            var canceller = $q.defer();
            pendingRequests.add({
                url: ResultUrl,
                canceller: canceller
            });
            var refreshToken = $window.localStorage.getItem('refreshToken');
            var tokenExpiretime = new Date($window.localStorage.getItem('tokenExpiretime'));
            var ExpireTime = new Date(tokenExpiretime);
            ExpireTime.setMinutes(ExpireTime.getMinutes() + (-1));
            if (new Date() > ExpireTime && tokenExpiretime && $window.localStorage.getItem('tokenExpiretime') !== "null" && $window.localStorage.getItem('tokenExpiretime') !== null) {
                pendingRequests.remove("/token");
                var request = {
                    client_id: 'swasthyWebApp',
                    grant_type: "refresh_token",
                    refresh_token: refreshToken
                };
                return this.PostLogin(ApiUrl + "token", request);
            }
            else {                
                var token = $window.localStorage.getItem("accessToken");
                var headers = {};
                if (token) {
                    headers = {
                        Authorization: 'Bearer ' + token,
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    };
                }
                else {
                    headers = {
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    };
                }
                var PostResponse = $http({
                    url: ResultUrl,
                    method: "GET",
                    dataType: "",
                    headers: headers,
                    timeout: canceller.promise
                }).success(function (data) {
                    //Status    Success = 0, Failed = 1, SessionExpired = 2, AuthKeyRequired = 3
                    if (data) {
                        if (data.Status) {
                            if (parseInt(data.Status, 10) === 2) {
                                window.localStorage.clear();
                                sessionStorage.clear();
                                window.location.href = 'index.html';
                            }
                        }
                    }
                }).error(function (error, status) {
                    if (parseInt(status, 10) === 401) {
                        $rootScope = $scope = {};
                        $window.localStorage.setItem('SessionData', undefined);
                        $window.localStorage.setItem('refreshToken', null);
                        $window.localStorage.setItem('tokenExpiretime', null);
                        $window.localStorage.setItem('KeepSignedIn', false);
                        $window.localStorage.setItem('CustomerInfo', JSON.stringify({}));
                        sessionStorage.clear();
                        window.location.href = 'index.html';
                        return false;
                    }
                });
                return PostResponse;
            }
        },
        PostWithoutLogin: function (ResultUrl, RequestParameterData) {
            try {
                var canceller = $q.defer();
                pendingRequests.add({
                    url: ResultUrl,
                    canceller: canceller
                });
                var headers = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' };
                var PostResponse = $http({
                    url: ResultUrl,
                    headers: headers,
                    method: "POST",
                    dataType: "",
                    data: $.param(RequestParameterData),
                    timeout: canceller.promise
                });

                //It will show the alert message like progress,success and error
                PostResponse.success(function (result) {
                });
                PostResponse.error(function () {
                });
                return PostResponse;

            }
            catch (ex) {
                console.log("ex", ex);
            }

        },
        CheckExpiration: function () {
            var AuthTokenDetails = JSON.parse($window.localStorage.getItem('AuthTokenDetails'));
            var tokenExpiretime = new Date(AuthTokenDetails['.expires']);
            tokenExpiretime.setMinutes(tokenExpiretime.getMinutes() + (-1));
            if (new Date() > tokenExpiretime && tokenExpiretime && AuthTokenDetails.tokenExpiretime !== "null" && AuthTokenDetails.tokenExpiretime !== null) {
                pendingRequests.remove("/token");
                var request = {
                    client_id: 'swasthyWebApp',
                    grant_type: "refresh_token",
                    refresh_token: AuthTokenDetails.refresh_token
                };
                return { IsExpired: true, Result: request };
            }
            else {
                var headers = null;
                if (AuthTokenDetails.access_token !== 'null' && AuthTokenDetails.access_token) {
                    headers = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Authorization': 'Bearer ' + AuthTokenDetails.access_token };
                }
                else {
                    headers = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' };

                }
                return { IsExpired: false, Result: headers };
            }
        }
    };
}

]);

