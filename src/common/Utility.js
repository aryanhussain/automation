﻿function BindYears(DropdownId) {
    var minOffset = 18, maxOffset = 100; // Change to whatever you want
    var thisYear = (new Date()).getFullYear();
    var select = $(DropdownId);
    var years = [];
    for (var i = minOffset; i <= maxOffset; i++) {
        var year = thisYear - i;
        years.push({ year: year });
    }
    return years;
}

function getobjectKeysize(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
        { size++; }
    }
    return size;
}
function getobjectsize(obj) {
    var size = 0, key;
    for (key in obj) {
      size++; 
    }
    return size;
}
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}
