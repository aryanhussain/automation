﻿angular.module('OUApp.CommonModel', ['ngResource', 'ui.bootstrap'])


.controller('AddEventinfoCntrl', function ($timeout, $state, $location, $scope, $RestService, ApiUrl, $filter) {
    $scope.SessionData.EventTypePopup = angular.copy($scope.SessionData.PopupCalendarEventType);
    $scope.SessionData.EventTypePopup   = _.filter($scope.SessionData.EventTypePopup,function(item){return item.ID !== 0 ;});
    if (parseInt($scope.SessionData.SelectedEvent.CalendarTypeID, 10) === 0) {
        $scope.SessionData.SelectedEvent = {};
        $scope.SessionData.SelectedEvent.CalendarTypeID = $scope.SessionData.EventTypePopup[0].ID;
        $scope.SessionData.SelectedEvent.Location = false;
    }
    $scope.SessionData.SelectedEvent.Location = $scope.SessionData.SelectedEvent.Location === "false" ? false : true;
    if ($('.time-picker')[0]) {
        $('.time-picker').datetimepicker({
            format: 'LT'
        });
    }
    if ($('#startdate')[0]) {
        $('#startdate').datetimepicker({
            format: 'ddd,MMM DD YYYY',
            minDate: moment(),
            showClose: true
        }).on('dp.change', function () {
            $scope.AssignValuetoendDate();
        });
    }
    if ($('#enddate')[0]) {
        $('#enddate').datetimepicker({
            format: 'ddd,MMM DD YYYY',
            minDate: moment(),
            showClose: true
        });
    }
    $scope.ShowAdrs = function () {
        try {
            $scope.LocationValue = false;
            if ($scope.SessionData.SelectedEvent.Location) {
                $scope.LocationValue = true;
            }
            else {
                $scope.LocationValue = false;
            }
        }
        catch (ex) {
            console.log('ex', ex);
        }
    };
    setTimeout(function () {
        $scope.ShowAdrs();
    }, 10);
    $scope.getdate = function () {
        var startdate = $scope.SessionData.SelectedEvent.StartDate;
        var enddate = $scope.SessionData.SelectedEvent.EndDate;
        $timeout(function () {
            $scope.SessionData.SelectedEvent.StartDate = $filter('date')(startdate, "EEE, MMM d y");
            $scope.SessionData.SelectedEvent.startime = $filter('date')(startdate, "hh:mm a");
            $scope.SessionData.SelectedEvent.EndDate = $filter('date')(enddate, "EEE, MMM d y");
            $scope.SessionData.SelectedEvent.endtime = $filter('date')(enddate, "hh:mm a");
        }, 200);
    };
    $scope.getdate();
    $scope.changeToTime = function () {
        try {
            $scope.ShowTimeBlocks = false;
            if ($scope.SessionData.SelectedEvent.AllDay) {
                $("#endtime").val("").parent().removeClass("fg-toggled");
                $("#startTime").val("").parent().removeClass("fg-toggled");
                $("#mobileendtime").val("").parent().removeClass("fg-toggled");
                $("#mobilestartTime").val("").parent().removeClass("fg-toggled");
                $("#enddate").removeAttr('validate').removeAttr('noempty');
                $("#startdate").removeAttr('validate').removeAttr('noempty');
                $scope.ShowTimeBlocks = true;
            }
            else {
                $("#enddate").attr('validate', '').attr('noempty', 'true');
                $("#startdate").attr('validate', '').attr('noempty', 'true');
            }
        }
        catch (ex) {
            console.log('ex', ex);
        }
    };
    $scope.changeToTime();
    $scope.FGetCountrieList = function () {
        try {
            var GetCountrieListRequest = {};
            $RestService.GetCountrieList(GetCountrieListRequest).success(function (result) {
                $scope.country = result;
                $scope.SessionData.SelectedEvent.Country = $scope.SessionData.SelectedEvent.Country || 'us';
                $scope.FGetCountryStateList($scope.SessionData.SelectedEvent.Country, 1);

            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
            });
        }
        catch (ex) {
            console.log("ex", ex);
        }
    };
    $scope.FGetCountrieList();
    $scope.FGetCountryStateList = function (CountryCode, type) {
        try {
            var GetCountryStateListRequest = "CountryCode=" + CountryCode;
            $scope.regions = '';
            $RestService.GetCountryStateList(GetCountryStateListRequest).success(function (result) {
                $scope.regions = result;
                if ($scope.regions.length > 0 && $scope.regions[0].RegionCode !== null) {
                    if (type) {
                        $scope.SessionData.SelectedEvent.State = $scope.regions[0].RegionCode;
                    }
                    else {
                        $scope.SessionData.SelectedEvent.State = $scope.SessionData.SelectedEvent.State ? $scope.SessionData.SelectedEvent.State : $scope.regions[0].RegionCode;
                    }
                }
                else {
                    $scope.Notification('Error', 'States not found for desired country', 'danger');
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
            });
        }
        catch (ex) {
            console.log("ex", ex);
        }
    };
    setTimeout(function () {
        $('.datpick').next().addClass("pull-right");
    }, 10);
    $scope.FGetTimeZoneList = function () {
        try {
            var GetTimeZoneListRequest = "";
            $RestService.GetTimeZoneList(GetTimeZoneListRequest).success(function (result) {
                $scope.TimeZoneList = result;
                $scope.SessionData.SelectedEvent.TimeZone = $scope.SessionData.SelectedEvent.TimeZone || $scope.TimeZoneList[0].Id;
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
            });
        }
        catch (ex) {
            console.log("ex", ex);
        }
    };
    $scope.FGetTimeZoneList();
    $scope.closepopup = function () {
        $scope.close();
        $scope.SessionData.SelectedEvent = {};
    };
    $scope.FAddCalendarItem = function () {
        var CreateCalendarItemRequest;
        try {
            CreateCalendarItemRequest =
                 {
                     AllDay: $scope.SessionData.SelectedEvent.AllDay,
                     ClientID: 1,
                     CalendarItemID: $scope.SessionData.SelectedEvent.EditEventID,
                     CalendarItemRepeatTypeID: $scope.SessionData.SelectedEvent.CalendarItemRepeatTypeID,
                     CalendarItemStatusID: 1,
                     CalendarTypeID: $scope.SessionData.SelectedEvent.CalendarTypeID,
                     CustomerID: $scope.SessionData.CustomerInfo.CustomerID,
                     Description: $scope.SessionData.SelectedEvent.Description,
                     EndDate: $("#enddate").val() + ' ' + $("#mobileendtime").val(),

                     Location: $scope.SessionData.SelectedEvent.Location,
                     PriorityTypeID: 1,
                     StartDate: $("#startdate").val() + ' ' + $("#mobilestartTime").val(),

                     TimeZoneOffset: $scope.SessionData.SelectedEvent.TimeZone,
                     Title: $scope.SessionData.SelectedEvent.Title,
                     Color: "",
                     StartDateMS: "",
                     EndDateMS: "",
                     Address1: $scope.SessionData.SelectedEvent.Address1,
                     Address2: $scope.SessionData.SelectedEvent.Address2,
                     City: $scope.SessionData.SelectedEvent.City,
                     State: $scope.SessionData.SelectedEvent.State,
                     Zip: $scope.SessionData.SelectedEvent.Zip,
                     Country: $scope.SessionData.SelectedEvent.Country
                 };

            if ($scope.SessionData.SelectedEvent.EditEventID > 0) {
                $RestService.UpdateCalendarItem(CreateCalendarItemRequest).success(function (result) {
                    if (result) {
                        $scope.Notification('Success', 'Event Successfully updated', 'success');
                        $scope.close(1);
                        $scope.SessionData.SelectedEvent = {};
                    }
                    else {
                        $scope.Notification('Error', 'Event updation failed', 'danger');
                    }
                }).error(function (error) {
                    $scope.Notification('Error', 'Some error occur please try again', 'danger');
                });

            }
            else {
                $RestService.CreateCalendarItem(CreateCalendarItemRequest).success(function (result) {
                    if (result) {
                        $scope.Notification('Success', 'Event Successfully created', 'success');
                        $scope.close(1);
                        $scope.SessionData.SelectedEvent = {};
                    }
                    else {
                        $scope.Notification('Error', 'Event creation failed', 'danger');
                    }
                }).error(function (error) {
                    $scope.Notification('Error', 'Some error occur please try again', 'danger');
                });
            }

        }
        catch (ex) {
            console.log('ex', ex);
        }
    };
    $scope.FDeleteCalendarItem = function (id) {
        try {
            console.log('id', id);
            var DeleteCalendarRequest;
            DeleteCalendarRequest =
            {
                CalendarItemsID: id

            };

            $RestService.DeleteCalendarItem(DeleteCalendarRequest).success(function (result) {
                try {
                    if (result) {
                        $scope.Notification('Success', 'Event Deleted Successfully', 'success');
                        $scope.close(1);
                        $scope.SessionData.SelectedEvent = '';
                    }

                    else {
                        $scope.Notification('Error', 'Some error occur please try again', 'danger');
                    }
                }
                catch (ex) {
                    console.log('ex', ex);
                }



            });
        }
        catch (ex) {
            console.log("ex", ex.message);
        }
    };
    $scope.AssignValuetoendDate = function () {
        var StartDateVal = $('#startdate').val();
        var EndDateVal = $('#enddate').val();
        $('#enddate').val(StartDateVal);
    };
})
;



