﻿(function () {
    'use strict';
    angular.module('OUApp.Companies', [
      'ui.router',
      'ui.bootstrap',
      'ngMaterial'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('Companies', {
            url: '/Companies',
            views: {
                "main": {
                    controller: 'CompaniesController as Companiescntrl',
                    templateUrl: 'Admin/Companies/Companies.tpl.html'
                }
            },
            data: { pageTitle: 'Companies' }
        });
    })
    .controller("CompaniesController", CompaniesController);
    CompaniesController.$inject = ['$rootScope', '$scope', '$RestService', '$filter', '$timeout', '$compile', '$http'];
    function CompaniesController($rootScope, $scope, $RestService, $filter, $timeout, $compile, $http) {
        var vm = this;
        $scope.SessionData.PageName = 'Companies';
        vm.CompaniesRawData = [];
        $scope.UserId = 0;
        vm.GetAdminCompanies = GetAdminCompanies;
        function GetAdminCompanies() {
            $scope.loading = true;
            try {
                var request = "id=" + $scope.UserId;
                $RestService.GetCompaniesAll(request).success(function (result) {
                    vm.CompaniesRawData = result.Companies;
                    $scope.loading = false;
                }).error(function (error) {
                    $scope.loading = false;
                    $scope.Notification('Error', 'Some error occur in getting data', 'danger');
                });
            }
            catch (e) {
                $scope.loading = false;
                $scope.Notification('Error', 'Some error occur in getting data', 'danger');
            }
        }
        //GetAdminCompanies();
        $scope.EditCompany = function (Company) {
            $scope.Session.EditCompanyId = Company.CompanyId;
            $scope.GoToState('EditCompany');
        };

        $('.mdl-layout__container').remove();
        //Sorting
        $scope.SortBy = function (predicate) {
            $scope.ColumnName = predicate.toString().trim();
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.predicate = predicate;
        };

        $scope.predicate = $scope.ColumnName = "CompanyId";
        $scope.reverse = true;        
    }
})();