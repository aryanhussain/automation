﻿angular.module('OUApp.Users', [
 'ui.router',
  'placeholders',
  'ui.bootstrap',
  'RestService'])
.config(function config($stateProvider) {
    $stateProvider
    .state('Users', {
        url: '/Users',
        views: {
            "main": {
                controller: 'Userscntrl',
                templateUrl: 'Admin/Users/Users.tpl.html'
            }
        },
        data: { pageTitle: 'Users' }

    });
})
.controller('Userscntrl', ['$rootScope', '$scope', '$window', '$log', '$location', '$timeout', '$RestService', function ($rootScope, $scope, $window, $log, $location, $timeout, $RestService) {
    $rootScope.userHeader = true;
    $scope.SessionData.PageName = 'Users';
    $scope.UserTypeId = null;
    $scope.GetUsers = function () {
        try {
            $scope.loading = true;
            var request = "userTypeId=" + $scope.UserTypeId;
            $RestService.User_GetUsers(request).success(function (result) {
                $scope.Users = result.Users;
                $scope.loading = false;
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur in getting data', 'danger');
            });
        }
        catch (e) {
            $scope.Notification('Error', 'Some error occur in getting data', 'danger');
        }
    };
    //$scope.GetUsers();
    $scope.EditUser = function (user) {
        $scope.Session.EditUser = { UserId: user.UserId };
        $scope.GoToState('ManageUser');
    };
    $scope.SortBy = function (predicate) {
        $scope.ColumnName = predicate.toString().trim();
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.predicate = $scope.ColumnName = "UserId";
    $scope.reverse = true;
}]);