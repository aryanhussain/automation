﻿angular.module('OUApp.Start', [
 'ui.router',
  'placeholders',
  'ui.bootstrap',
  'RestService'])
.config(function config($stateProvider) {
    $stateProvider
    .state('Start', {
        url: '/Start',
        views: {
            "main": {
                controller: 'Startcntrl',
                templateUrl: 'Admin/Start/Start.tpl.html'
            }
        },
        data: { pageTitle: 'Start' }

    });
})
.controller('Startcntrl', ['$rootScope', '$scope', '$window', '$log', '$location', '$timeout', '$RestService', '$stateParams', '$compile', '$state', function ($rootScope, $scope, $window, $log, $location, $timeout, $RestService, $stateParams, $compile, $state) {
    $rootScope.userHeader = true;    
    $scope.SessionData.PageName = 'Dashboard';
}]);