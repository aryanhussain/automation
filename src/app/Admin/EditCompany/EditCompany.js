﻿(function () {'use strict';
    angular.module('OUApp.EditCompany', [
      'ui.router',
      'ui.bootstrap',
      'ngMaterial'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('EditCompany', {
            url: '/EditCompany',
            views: {
                "main": {
                    controller: 'EditCompanyController as EditCompanycntrl',
                    templateUrl: 'Admin/EditCompany/EditCompany.tpl.html'
                }
            },
            data: { pageTitle: 'Edit Company' }
        });
    })
    .controller("EditCompanyController", EditCompanyController);
    EditCompanyController.$inject = ['$rootScope', '$scope', '$RestService', '$filter', '$timeout', '$compile', '$http'];
    function EditCompanyController($rootScope, $scope, $RestService, $filter, $timeout, $compile, $http) {
        $scope.SessionData.PageName = 'Edit Company';               
        if ($scope.Session.EditCompanyId === 0) {
            $scope.GoToState('Companyies');
        }
        else {
            $scope.CompanyId = $scope.Session.EditCompanyId;
            $scope.Session.EditCompanyId = 0;
        }
        var vm = this;
    }
})();