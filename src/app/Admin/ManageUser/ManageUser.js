﻿angular.module('OUApp.ManageUser', [
 'ui.router',
  'placeholders',
  'ui.bootstrap',
  'RestService'])
.config(function config($stateProvider) {
    $stateProvider
    .state('ManageUser', {
        url: '/ManageUser',
        views: {
            "main": {
                controller: 'ManageUsercntrl',
                templateUrl: 'Admin/ManageUser/ManageUser.tpl.html'
            }
        },
        data: { pageTitle: 'Manage User' }

    });
})
.controller('ManageUsercntrl', ['$rootScope', '$scope', '$window', '$log', '$location', '$timeout', '$RestService', 'ApiUrl', '$state', function ($rootScope, $scope, $window, $log, $location, $timeout, $RestService, ApiUrl, $state) {
    $rootScope.userHeader = true;
    $scope.User = $scope.Session.EditUser || { UserId: 0 };

    $scope.BtnText = "Add";
    if ($scope.User.UserId > 0) {
        $scope.BtnText = "Update";
    }
    else {
        $scope.BtnText = "Add";
    }

    $scope.User.Specializations = [];
    $scope.SpecializationsData = $scope.SpecializationsData || [];
    $scope.Specializations = $scope.Session.Specializations || [];

    $scope.loadSpecializations = function () {
        try {
            $scope.loading = true;
            $RestService.GetSpecializations('').success(function (result) {
                $scope.Specializations = result.Items;
                $scope.initSpecializationsData();
                $scope.loading = false;
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur in getting data', 'danger');
            });
        }
        catch (e) {
            $scope.Notification('Error', 'Some error occur in getting data', 'danger');
        }
    };
    $scope.loadCountries = function () {
        try {
            $scope.loading = true;
            $RestService.GetCountries().success(function (result) {
                $scope.Countries = result.Items;
                $scope.loading = false;
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur in getting data', 'danger');
            });
        }
        catch (e) {
            $scope.Notification('Error', 'Some error occur in getting data', 'danger');
        }
    };
    $scope.loadCities = function (countryId) {
        try {
            countryId = countryId || 1;
            var request = "id=" + countryId;
            $scope.loading = true;
            $RestService.GetCities(request).success(function (result) {
                $scope.Cities = result.Items;
                $scope.loading = false;
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur in getting data', 'danger');
            });
        }
        catch (e) {
            $scope.Notification('Error', 'Some error occur in getting data', 'danger');
        }
    };

    $scope.loadSpecializations();
    //$scope.loadCountries();
    //$scope.loadCities();
    $scope.loadUserDetails = function () {
        try {
            if ($scope.User.UserId > 0) {
                var request = "id=" + $scope.User.UserId;
                $scope.loading = true;
                $RestService.User_GetUser(request).success(function (result) {
                    $scope.User = result.User;
                    $scope.loading = false;
                }).error(function (error) {
                    $scope.Notification('Error', 'Some error occur in getting data', 'danger');
                });
            }
        }
        catch (e) {
            $scope.Notification('Error', 'Some error occur in getting data', 'danger');
        }
    };
    $scope.loadUserDetails();

    $scope.isSpecializationsSelected = function (specialization) {
        var isSelected = false;
        _.each($scope.SpecializationsData, function (item) {
            if (item.SelectedSpecializationId.toString() === specialization.Key.toString()) {
                isSelected = true;
            }
        });
        return isSelected;
    };
    $scope.getSpecializationById = function (specializationId) {
        var specialization;
        _.each($scope.Specializations, function (item) {
            if (item.Key.toString() === specializationId.toString()) {
                specialization = item;
            }
        });
        return specialization;
    };

    $scope.setSelectedSpecializations = function () {
        if ($scope.SpecializationsData.length === 0) {
            $scope.SpecializationsData = [];
            _.each($scope.User.Specializations, function (item) {
                $scope.SpecializationsData.push({ SelectedSpecializationId: item.SpecializationId, Timing: item.Timing, Fee: item.Fee });
            });
        }
    };


    $scope.initSpecializationsData = function () {
        $scope.setSelectedSpecializations();
        var specializationData = [];
        var remainingSpecializations = _.filter($scope.Specializations, function (item) { return !$scope.isSpecializationsSelected(item); });
        var itemcounter = 0;
        _.each($scope.SpecializationsData, function (item) {
            itemcounter++;
            var availSpecializations = [];
            if (remainingSpecializations !== null && remainingSpecializations !== undefined) {
                availSpecializations = Object.create(remainingSpecializations);
            }
            var currentSpec = $scope.getSpecializationById(item.SelectedSpecializationId);
            if (currentSpec !== undefined) {
                availSpecializations.splice(0, 0, currentSpec);
            }
            item.Value = item.Value || '';
            specializationData.push({ SelectedSpecializationId: item.SelectedSpecializationId, Timing: item.Timing, Fee: item.Fee, AvailableSpecializations: availSpecializations, IsAddMore: itemcounter === $scope.SpecializationsData.length, IsAddMoreEnabled: (itemcounter < $scope.Specializations.length || availFields.length > 1) });

        });

        if (specializationData.length === 0 && remainingSpecializations !== null && remainingSpecializations !== undefined && remainingSpecializations.length > 0) {
            specializationData.push({ SelectedSpecializationId: remainingSpecializations[0].Key, Timing: '', Fee: '', AvailableSpecializations: remainingSpecializations, IsAddMore: true, IsAddMoreEnabled: (remainingSpecializations.length > 1) });
        }
        $scope.SpecializationsData = Object.create(specializationData);
    };
    $scope.newOrRemoveSpecializationItem = function (specializationItem, event) {
        var specializationData = Object.create($scope.SpecializationsData);
        if (event === 'addNew') {
            specializationItem.IsAddMore = false;
            var remainingSpecializations = _.filter($scope.Specializations, function (item) { return !$scope.isSpecializationsSelected(item); });
            if (remainingSpecializations !== null && remainingSpecializations !== undefined && remainingSpecializations.length > 0) {
                specializationData.push({ SelectedSpecializationId: remainingSpecializations[0].Key, Timing: '', Fee: 0, AvailableSpecializations: remainingSpecializations, IsAddMore: true, IsAddMoreEnabled: (remainingSpecializations.length > 1) });
            }
        } else {
            if (specializationData !== null && specializationData !== undefined && specializationData.length > 0) {
                var index = 0;
                for (; index < specializationData.length; index++) {
                    if (specializationData[index].SelectedSpecializationId.toString() === specializationItem.SelectedSpecializationId.toString()) {
                        break;
                    }
                }
                specializationData.splice(index, 1);
                if (specializationData.length > 0) {
                    specializationData[specializationData.length - 1].IsAddMore = true;
                }
            }
        }
        $scope.SpecializationsData = Object.create(specializationData);
        $scope.initSpecializationsData();
    };
    $scope.changeSpecializationType = function () { $scope.initSpecializationsData(); };

    $scope.UpdateUserDetails = function () {      
        $scope.loading = true;
        var userRequest = new FormData();
       
        try {
            var specializationsData = [];
            for (var i = 0; i < $scope.SpecializationsData.length; i++) {
                var specialization = $scope.SpecializationsData[i];
                if (specialization !== undefined) {
                    specializationsData.push({ SpecializationId: specialization.SelectedSpecializationId, Timing: specialization.Timing, Fee: specialization.Fee });
                }
            }

            var splitLength = parseInt($('#userImage').val().split('.').length, 10);
            var extensionCaseInsensitive = $('#userImage').val().split('.')[splitLength - 1];
            userRequest.append('file[0]', document.getElementById('userImage').files[0]);          

            userRequest.append('UserId', $scope.User.UserId);
            userRequest.append('UserTypeId', $scope.User.UserTypeId);
            userRequest.append('Specializations', JSON.stringify(specializationsData));
            userRequest.append('Email', $scope.User.Email);
            userRequest.append('Name', $scope.User.Name);
            userRequest.append('Mobile', $scope.User.Mobile);
            userRequest.append('DateOfBirth', $scope.User.DateOfBirth);
            userRequest.append('Address', $scope.User.Address);
            userRequest.append('City', $scope.User.City || 0);
            userRequest.append('Country', $scope.User.Country || 0);
            userRequest.append('Longitude', $scope.User.Longitude);
            userRequest.append('Latitude', $scope.User.Latitude);

            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState == 4 && request.status == 200) {
                    if (request.status == 200) {
                        var Result = JSON.parse(request.response);
                        if (Result) {
                            $scope.$apply(function () {
                                $scope.loading = true;
                                
                                $scope.UserId = Result.Identity;
                                if (parseInt($scope.UserId, 10) > 0) {
                                    $scope.Notification('Success', 'User inserted successfully.', 'success');
                                    //$scope.gritter('Success', '<div class="" style="background-color:green;"><span class="marginleft">User inserted successfully.</span></div>', 'success');
                                    $scope.GoToState('Users');
                                }
                                else {
                                    $scope.Notification('Error', 'Some error occur in getting data', 'danger');
                                    //$scope.gritter('Error', '<div class="" style="background-color:red;"><span class="marginleft">User registration failed.</span></div>', 'success');
                                }
                            });
                        }
                        else {
                            $scope.loading = false;
                        }
                    }
                }
            };
            var RequestWebServiceUrl = "";

            RequestWebServiceUrl = ApiUrl + "/api/User/AddUser";
            request.open("POST", RequestWebServiceUrl, true);
            request.setRequestHeader("Accept", "application/json");
            request.setRequestHeader("Authorization", 'Bearer ' + JSON.parse($window.localStorage.getItem('AuthTokenDetails')).access_token);
            request.send(userRequest);
        }
        catch (ex) {
            console.log("ex", ex);
        }
    };
}]);