angular.module('OUApp', [
    //Utilities
    'templates-app',
    'templates-common',
    'ngCookies',
    'angular.filter',
    'pascalprecht.translate',
    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ui.router',
    'OUApp.validation-directives',
    'RestService',
    'OUApp.services',
    'OUApp.directives',
    'OUApp.filters',
    'OUApp.Config',
    'ngMaterial',
    'ngAria',
    'OUApp.CommonModel',
    //Login
    'OUApp.Login',
    'OUApp.ForgotPassword',
    //ADMIN
    'OUApp.Start',
    'OUApp.Companies',
    'OUApp.EditCompany',
    'OUApp.Users',
    'OUApp.ManageUser',
    //'Admin.Chats',
    //'Admin.ManageChat',
    //'Admin.ManageUser',
    //'Admin.Users',
    //'Admin.Notifications',
    //'Admin.ManageNotification',

    //'WSI.Notifications',
    //'WSI.Miscellaneous',
    //'WSI.FeatureFlags',
    //'WSI.Calendar',
    //'WSI.Community',
    //'WSI.CreateCompany',
    //'WSI.Companies',
    'cookieService',
    'ngCookies'
])

.constant('ApiUrl', (function () {
    if (location.host.match(/.*swasthy\.com/)) {
        return 'http://localhost:50703';
    } else {
        return 'http://swasthy.com';
    }
})())
.config(function myAppConfig($stateProvider, $urlRouterProvider, $translateProvider) {

            $urlRouterProvider.otherwise('/Login');

            $translateProvider.useLocalStorage();
            $translateProvider.useStaticFilesLoader({
                prefix: 'assets/languages/',
                suffix: '.json'
            });

            $translateProvider.preferredLanguage('en_US');
        })
.run(function run(countryModel) {
        if (!countryModel.selectedCountry) {
            countryModel.init();
        }
    })

.controller('AppCtrl', ['$scope', '$rootScope', '$location', '$filter', 'ApiUrl', '$uibModal', '$translate', '$window', '$RestService', 'cookieService', '$state', '$cookies', 'Config', '$mdMedia', '$mdDialog', '$translatePartialLoader', '$http', 'countryModel',
     function AppCtrl($scope, $rootScope, $location, $filter, ApiUrl, $uibModal, $translate, $window, $RestService, cookieService, $state, $cookies, Config, $mdMedia, $mdDialog, $translatePartialLoader, $http, countryModel) {
         $rootScope.SessionData = {};
         $rootScope.WinWidth = $(window).width(),
         $rootScope.WinHeight = $(window).height();
         $scope.Loginpage = $scope.Loginpage || true;
         $scope.SessionData.poptype = '';
         $rootScope.CompanyConfig = Config.CompanyDetail;
         $scope.SessionData.CompanyDetail = $scope.SessionData.CompanyDetail || {};
         $scope.SessionData.IsSuperAdmin = false;
         $rootScope.CurrentUser = $rootScope.CurrentUser || {};
         $rootScope.Session = $rootScope.Session || {};

         if ($window.localStorage.getItem('CurrentUser')) {
             if ($window.localStorage.getItem('CurrentUser') === 'undefined') {
                 $rootScope.CurrentUser = {};
             } else {
                 $rootScope.CurrentUser = JSON.parse($window.localStorage.getItem('CurrentUser'));
             }
         }
         $rootScope.$watch('CurrentUser', function (new_value, old_value) {
             if (new_value !== old_value) {
                 $window.localStorage.setItem('CurrentUser', JSON.stringify($rootScope.CurrentUser));
             }
         }, true);


         if ($window.localStorage.getItem('Session')) {
             if ($window.localStorage.getItem('Session') === 'undefined') {
                 $rootScope.Session = {};
             } else {
                 $rootScope.Session = JSON.parse($window.localStorage.getItem('Session'));
             }
         }
         $rootScope.$watch('Session', function (new_value, old_value) {
             if (new_value !== old_value) {
                 $window.localStorage.setItem('Session', JSON.stringify($rootScope.Session));
             }
         }, true);
         $scope.startover = function () {
             $scope.clearlocalStorage();
         };
         $scope.logOrder = function () {
             $log.debug($rootScope.CurrentUser);
         };
         $scope.localStorage = function () {
             $log.debug(JSON.parse($window.localStorage.CurrentUser));
         };

         $scope.settings = {};
         /*if locale is in localStorage, retrieve and set $scope.settings.locale*/
         if ($window.localStorage.getItem('locale')) {

             var match = $.parseJSON($window.localStorage.getItem('locale'));
             var thisLocale = $.grep(countryModel.selectedCountry.languages, function (n, i) { return n.locale === match.locale; });
             $scope.settings.locale = thisLocale[0];

             //if locale not in localStorage, set locale = first availble array item...
         } else { $scope.settings.locale = countryModel.selectedCountry.languages[0]; }
         $scope.gotoAnchor = function (divid) {
             var newHash = divid;
             console.log('divid', divid);
             if ($location.hash() !== newHash) {
                 // set the $location.hash to `newHash` and
                 // $anchorScroll will automatically scroll to it
                 $location.hash(divid);
             } else {
                 $anchorScroll();
             }
         };
         $scope.settings.showLanguage = $location.path() == '/enroll/start';
         $scope.setLocale = function (locale) {
             $translate.uses(locale.locale);
             $scope.settings.locale = locale;
             $window.localStorage.setItem('locale', JSON.stringify($scope.settings.locale));
         };

         if (!localStorage.getItem('CurrentUser')) {
             document.location = 'index.html';
         }
         $scope.$on('$locationChangeStart', function (event, toState, toParams, fromState, fromParams) {
             $scope.currentstate = $location.path();
             $scope.SessionData.CurrentState = $scope.currentstate;
             if ($scope.currentstate === "/Login" || $scope.currentstate === "/ForgotPassword") {
                 $scope.Header = false;
                 $scope.Footer = false;
                 $scope.Loginpage = true;
             }
             else if ($scope.currentstate === "/Home") {
                 $scope.Header = true;
                 $scope.Footer = true;
                 $scope.Loginpage = false;
             }
             else if ($scope.currentstate === "/lockscreen") {
                 $scope.Header = false;
                 $scope.Footer = false;
                 $scope.Loginpage = true;
             }
             else {
                 $scope.Header = true;
                 $scope.Footer = true;
                 $scope.Loginpage = false;
             }

         });
         $scope.$on('$stateChangeSuccess', function (event, toState, ToParams, fromState, fromParams) {
             $scope.CurrentPath = $location.path();
             if (angular.isDefined(toState.data.pageTitle)) {
                 $scope.pageTitle = toState.data.pageTitle + ' | Automation';
             }
             if ($location.path().search('/Login') > -1) {
                 $rootScope.CurrentUser = {};
             }

             if ($location.path().search('/Login') > -1) {
                 $rootScope.Session = {};
             }

             var url = $window.location.href;
             if (url.toLowerCase().indexOf("ManagePageContentBlocks") >= 1 || url.toLowerCase().indexOf("AddPage") >= 1) {
                 $('#sidebar ul li a').removeClass('active');
                 $('.pages').addClass('active');
             }

         });

         $scope.changeLocale = function () {
             $scope.updateLocale();
         };
         $scope.updateLocale = function () {
             $scope.setLocale($scope.settings.locale);
             return $scope.settings.locale;
         };
         $scope.countryModel = countryModel;
         $rootScope.CurrentUser.country = $rootScope.CurrentUser.country || countryModel.selectedCountry;
         $scope.clearlocalStorage = function () {
             $window.localStorage.clear();
             $window.localStorage.setItem('accessToken', null);
             $window.localStorage.setItem('refreshToken', null);
             $window.localStorage.setItem('tokenExpiretime', null);
             $scope = $rootScope = $window.localStorage = {};
             document.location = 'index.html';
             return false;
         };

         $scope.setBodyClass = function () {
             return $window.localStorage.getItem('NG_TRANSLATE_LANG_KEY');
         };

         $rootScope.Popup = function (type, poptype, fun, okFun, closefun, title, message) {
             $scope.CurrentUser.poptype = poptype;
             $scope.btntext = $translate('Add');
             switch (type) {
                 case 'page':
                     $scope.template = 'modal/addpagemodal.tpl.html';
                     break;

             }
         };
         $rootScope.PopupExtended = function (popupcode, popuptype, title, message, data, afteropen, onclose, settings) {
             console.log("data", data);
             switch (popupcode) {
                 case 'page':
                     $scope.template = 'modal/addpagemodal.tpl.html';
                     break;
             }
         };
         $scope.SignIn = function () {
             $location.path('/Login');
         };
         $scope.SignUp = function () {
             $location.path('/SignUp');
         };

         $scope.GoTo = function (state) {
             if (state === 'TemplatesAdd') {
                 $scope.Session.TemplateID = 0;
                 $scope.TemplateInfo = {};
                 $scope.Session.EditTemplate = false;
             }
             $location.url('/' + state + '');
         };
         $scope.GoToState = function (state) {

             $location.path('/' + state + '');
         };

         $scope.NextStep = function (SiteId) {
             if (parseInt(SiteId, 10) > 0) {
                 $scope.CurrentUser.SiteID = SiteId;
                 _.each($scope.CurrentUser.GetSitePagesList, function (Site) {
                     if (Site.SiteID === $scope.CurrentUser.SiteID) {
                         $scope.CurrentUser.SiteInfo = Site;
                     }
                     if ($scope.currentstate === '/Home') {
                         $location.path('/Start');
                     }
                     else {
                         $location.path('/Start');
                     }

                 });
             }
             else {
                 $scope.newsite = true;
                 $location.path('/SiteInformation');
             }


         };
         $scope.test = true;
         // Call Every Time When State Change
         if (sessionStorage.getItem('SessionData')) {
             if (sessionStorage.getItem('SessionData') === 'undefined') {
                 $rootScope.SessionData = {};
             } else {
                 $rootScope.SessionData = JSON.parse(sessionStorage.getItem('SessionData'));
             }
         }
         $rootScope.$watch('SessionData', function (new_value, old_value) {
             if (new_value !== old_value) {
                 sessionStorage.setItem('SessionData', JSON.stringify($rootScope.SessionData));
             }
         }, true);
         $scope.logCurrent = function () {
             $log.debug($rootScope.SessionData);
         };

         $scope.GoToState = function (state) {
             $location.path("/" + state);
         };
         // Clear Local Storage
         $scope.logout = function () {
             $scope.clearlocalStorage();
         };

         $scope.SessionData.windowheight = window.innerHeight;


         $scope.SessionData.windowWidth = $(window).width();
         var doUpdate = function () {
             $scope.SessionData.windowWidth = $(window).width();
             $rootScope.WinWidth = $(window).width(),
                 $rootScope.WinHeight = $(window).height();
             var size = $(window).width();
             if (size >= 768) {
                 $scope.SessionData.SubMenu = true;
                 $scope.SessionData.MobileScreen = false;

             }
             else {
                 $scope.SessionData.ShowSubMenu = false;
                 $scope.SessionData.SubMenu = false;
                 $scope.SessionData.MobileScreen = true;
             }
         };
         $(window).resize(function () {
             if ($rootScope.$$phase) {
                 $scope.$eval(doUpdate);
             } else {
                 $scope.$apply(doUpdate);
             }
         });
         $rootScope.popup = function (type, title, message, closeFn, data, size) {
             $scope.SessionData.poptype = type;
             if (type.toLowerCase().trim() === 'addeditadmineventinfo') {
                 $scope.templateUrl = 'Common/modal/AddEditEventinfo.tpl.html';
             }
             var modalInstance = $uibModal.open({
                 animation: true,
                 templateUrl: $scope.templateUrl,
                 backdrop: 'static',
                 keyboard: true,
                 size: size,
                 controller: ['$scope', '$location', '$uibModal', '$modalInstance', function ($scope, $location, $uibModal, $modalInstance) {
                     $('body').addClass('modal-open');
                     $scope.title = title;
                     $scope.message = message;
                     $scope.type = type;
                     $scope.data = data;
                     $scope.close = function (type) {
                         $('body').removeClass('modal-open');
                         if (parseInt(type, 10) === 1) {
                             if (closeFn) {
                                 closeFn.call();
                             }
                         }
                         else {
                             $('body').removeClass('modal-open');
                             $modalInstance.dismiss('cancel');
                         }
                         $('body').removeClass('modal-open');
                         $modalInstance.dismiss('cancel');
                     };
                 }]
             });
         };
         $rootScope.Notification = function (title, message, type, from, align, icon, animIn, animOut) {
             if ($("body").find(".growl-animated").length === 0) {
                 $.growl({
                     icon: icon,
                     title: title,
                     message: message,
                     url: ''
                 }, {
                     element: 'body',
                     type: type,
                     allow_dismiss: true,
                     placement: {
                         from: from,
                         align: align
                     },
                     offset: {
                         x: 20,
                         y: 85
                     },
                     spacing: 10,
                     z_index: 1031,
                     delay: 2500,
                     timer: 1000,
                     url_target: '_blank',
                     mouse_over: false,
                     animate: {
                         enter: animIn,
                         exit: animOut
                     },
                     icon_type: 'class',
                     template: '<div data-growl="container" class="alert" role="alert">' +
                     '<button type="button" class="close" data-growl="dismiss">' +
                     '<span aria-hidden="true">&times;</span>' +
                     '<span class="sr-only">Close</span>' +
                     '</button>' +
                     '<span data-growl="icon"></span>' +
                     '<span  class="text-left pull-left"  data-growl="title"  style="font-size: 15px; font-weight: 800;"></span><br/>' +
                     '<span  class="pull-left" data-growl="message"></span>' +
                     '<a href="#" data-growl="url"></a>' +
                     '</div>'
                 });
             }
         };
         var windowwidth = $(window).width();
         if (windowwidth >= 768) {
             $scope.SessionData.SubMenu = true;
             $scope.SessionData.MobileScreen = false;
         }
         else {
             $scope.SessionData.ShowSubMenu = false;
             $scope.SessionData.SubMenu = false;
             $scope.SessionData.MobileScreen = true;
         }
         $scope.SessionData.CompanyDetail = $scope.SessionData.CompanyDetail || {};
         $rootScope.mdDialogPopup = function (ev, tempurl, type, closeFn, data) {
             $scope.SessionData.PopupType = type;
             var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
             $mdDialog.show({
                 controller: popupctrl,
                 templateUrl: tempurl,
                 parent: angular.element(document.body),
                 targetEvent: ev,
                 clickOutsideToClose: true,
                 locals: {
                     SessionData: $scope.SessionData,
                     modaldata: data
                 }
             }).then(function (callback) {
                 if (callback) {
                     closeFn.call();
                     $('body').removeClass('modal-open');
                 }
             }, function () {
             });
             $('body').addClass('modal-open');
             $scope.$watch(function () {
                 return $mdMedia('xs') || $mdMedia('sm');
             }, function (wantsFullScreen) {
                 $scope.customFullscreen = (wantsFullScreen === true);
             });

         };

         function popupctrl($timeout, $state, $location, $scope, $RestService, ApiUrl, $filter, SessionData, $rootScope, modaldata) {
             if (modaldata.AccessType === 'shoppingcart') {
                 shoppingCtrl($timeout, $state, $location, $scope, $RestService, ApiUrl, $filter, SessionData, $rootScope, modaldata);
             }

         }
         function shoppingCtrl() {

         }
         $scope.$on('$viewContentLoaded', function () {
             setTimeout(function () {
                 $(function () {
                     $('.ds_Dialog').find('[data-action]').on('click', function () {
                         alert(this.getAttribute('data-action') + ' (for pretend).');
                         $.ds.activeDialogs[$.ds.activeDialogs.last.type][$.ds.activeDialogs.last._id].close();
                     });

                     $('#ds_Nav-admin-filter-toolbar').slideUp(280);

                     $('#ds_Nav-admin-filter-toggle').on('change', function () {
                         var self = this;
                         function triggerResize() {
                             $(window).trigger('resize');
                         }
                         if (self.checked) {
                             $('#ds_Nav-admin-filter-toolbar').stop().slideDown(280, triggerResize);
                             $('.mdl-icon-toggle[for="ds_Nav-admin-filter-toggle"]').find('.mdl-icon-toggle__label').removeClass('icon-filter c-white').addClass('icon-filter-remove c-white');
                             setTimeout(function () {
                                 $('#ds_Nav-admin-filter-toolbar').css('height', 'auto');
                             }, 1000);
                         } else {
                             $('#ds_Nav-admin-filter-toolbar').stop().slideUp(280, triggerResize);
                             $('.mdl-icon-toggle[for="ds_Nav-admin-filter-toggle"]').find('.mdl-icon-toggle__label').removeClass('icon-filter-remove c-white').addClass('icon-filter c-white');

                             setTimeout(function () {
                                 $('#ds_Nav-admin-filter-toolbar').css('height', 'auto');
                             }, 1000);
                         }
                     });

                     $('#ds_Drawer-left').dsDialog({
                         type: 'drawer',
                         triggerSelector: '[data-toggle="#ds_Drawer-left"]',
                         callbacks: {
                             beforeOpen: function (dialog) {
                                 document.body.classList.add('ds_layout--left-drawer-active');
                             },
                             afterClose: function (dialog) {
                                 document.body.classList.remove('ds_layout--left-drawer-active');
                             }
                         }
                     });
                 });
             }, 2000);
         });

         $rootScope.Notification = function (title, message, type, from, align, icon, animIn, animOut) {
             if ($("body").find(".growl-animated").length === 0) {
                 $.growl({
                     icon: icon,
                     title: title,
                     message: message,
                     url: ''
                 }, {
                     element: 'body',
                     type: type,
                     allow_dismiss: true,
                     placement: {
                         from: from,
                         align: align
                     },
                     offset: {
                         x: 20,
                         y: 85
                     },
                     spacing: 10,
                     z_index: 1031,
                     delay: 2500,
                     timer: 1000,
                     url_target: '_blank',
                     mouse_over: false,
                     animate: {
                         enter: animIn,
                         exit: animOut
                     },
                     icon_type: 'class',
                     template: '<div data-growl="container" class="alert" role="alert">' +
                     '<button type="button" class="close" data-growl="dismiss">' +
                     '<span aria-hidden="true">&times;</span>' +
                     '<span class="sr-only">Close</span>' +
                     '</button>' +
                     '<span data-growl="icon"></span>' +
                     '<span  class="text-left pull-left"  data-growl="title"  style="font-size: 15px; font-weight: 800;"></span><br/>' +
                     '<span  class="pull-left" data-growl="message"></span>' +
                     '<a href="#" data-growl="url"></a>' +
                     '</div>'
                 });
             }
         };
     }]);

