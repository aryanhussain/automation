﻿(function () {
    'use strict';
    angular.module('WSI.CreateCompany', [
      'ui.router',
       'ui.bootstrap',
    'ngMaterial'
    ])
    .config(function config($stateProvider) {
        $stateProvider.state('CreateCompany', {
            url: '/CreateCompany',
            views: {
                "main": {
                    controller: 'CreateCompanyController as CreateCompanyCtrl',
                    templateUrl: 'CreateCompany/CreateCompany.tpl.html'
                }
            },
            data: { pageTitle: 'Create Company' }
        });
    })

    .directive("disableAnimate", disableAnimate)
    .controller("CreateCompanyController", CreateCompanyController);
    disableAnimate.$inject = ['$animate'];
    CreateCompanyController.$inject = ['$scope', '$RestService', '$filter', '$timeout', '$compile', '$rootScope', '$mdDialog', '$mdMedia', '$sce'];
    function disableAnimate($animate) {
        return function (scope, element) {
            $animate.enabled(false, element);
        };
    }
    function CreateCompanyController($scope, $RestService, $filter, $timeout, $compile, $rootScope, $mdDialog, $mdMedia, $sce) {
        var vm = this;
        $scope.SessionData.PageName = 'Create Company';
        $scope.SessionData.EndDate = '';
        vm.indeterminate = 100;
        vm.userState = '';
        vm.companyDetails = {};
        vm.DateString = new Date().getTime();
        $scope.SessionData.companyInfo = $scope.SessionData.companyInfo || {};
        $scope.SessionData.exigoInfo = $scope.SessionData.exigoInfo || {};
        $scope.SessionData.pillarsInfo = $scope.SessionData.pillarsInfo || {};
        vm.mailingDetails = {};
        vm.langDetails = {};
        vm.teamlinkSetup = {};
        vm.ziplingoSetup = {};
        vm.treeSetup = [];
        vm.securitySetup = {};
        var treeArray = [];
        var treeBlockCount = "";
        var entitytreeBlockCount = "";
        vm.NewEntityArray = {};
        vm.companyCalendraType = [];
        vm.langDetails.LanguagesModal = [];
        $scope.SessionData.tabNumber = $scope.SessionData.tabNumber || 0;
        vm.currentStep = $scope.SessionData.tabNumber || 0;
        $scope.placement = {
            options: [
              'top',
              'top-left',
              'top-right',
              'bottom',
              'bottom-left',
              'bottom-right',
              'left',
              'left-top',
              'left-bottom',
              'right',
              'right-top',
              'right-bottom'
            ],
            selected: 'top'
        };
        $scope.$on('$destroy', function () {
            $scope.SessionData.tabNumber = 0;
            $scope.SessionData.companyInfo = {};
            $scope.SessionData.exigoInfo = {};
            $scope.SessionData.pillarsInfo = {};
        });
        function resetTreeTabs() {
            treeBlockCount = 1;
            vm.dynamicTreeBlock = [];
            vm.dynamicTreeBlock.push({ id: new Date().getTime() });
        }
        resetTreeTabs();
        vm.nextTab = function (value) {
            if (value === 0) {
                CreateReq();
            }
            else if (value === 1) {
                if (parseInt(vm.currentStep, 10) === 1) {
                    treeArray = [];
                    for (var i = 0; i < vm.treeSetup.length; i++) {
                        treeArray.push(vm.treeSetup[i]);
                    }
                    CreateCompanyDetailsReq();
                }
                else if (parseInt(vm.currentStep, 10) === 2) {
                    FSaveCompanySettings();
                }
                else if (parseInt(vm.currentStep, 10) === 3) {
                    GetReportCenterData();
                    resetTreeTabs();
                    vm.currentStep = 4;
                    $scope.SessionData.tabNumber = 4;

                }
                else if (parseInt(vm.currentStep, 10) === 4) {
                    FSaveReportCenterData();

                }
                else {

                }

            }
        };
        vm.Providers = [
                       { "id": "1", "Name": "Exigo", "Active": true },
                       { "id": "2", "Name": "Infotrax", "Active": false },
                       { "id": "3", "Name": "Orbsix", "Active": false },
                       { "id": "4", "Name": "PillarSystems", "Active": true }
        ];
        //vm.TreeType = [
        //               { "id": "1", "Name": "Unilevel" },
        //               { "id": "2", "Name": "Binary" },
        //               { "id": "3", "Name": "Enrollment" },
        //               { "id": "4", "Name": "Matrix" }
        //];
        vm.DefaultContext = [
                       { "id": "1", "Name": "Sandbox 1" },
                       { "id": "2", "Name": "Sandbox 2" },
                       { "id": "3", "Name": "Live" }
        ];
        vm.CheckProvider = function (id) {
            if (id === '1') {
                $scope.SessionData.pillarsInfo = {};
            }
            else {
                $scope.SessionData.exigoInfo = {};
            }
        };
        $scope.SessionData.companyInfo.ProviderTypesModal = "1";
        $scope.SessionData.companyInfo.TreeTypeModal = $scope.SessionData.companyInfo.TreeTypeModal || "1";
        $scope.SessionData.exigoInfo.DefaultContextModal = $scope.SessionData.exigoInfo.DefaultContextModal || "1";
        $timeout(function () {
            vm.CheckProvider($scope.SessionData.companyInfo.ProviderTypesModal);
        }, 100);
        var CompanyZone = [];
        CompanyZone = moment.tz.names();
        vm.AllTimeZone = [];
        if (CompanyZone.length > 0) {
            _.each(CompanyZone, function (item) {
                if (item) {
                    var timez = moment.tz(item).format('z Z');
                    var timezone = timez.split(' ')[1];
                    vm.AllTimeZone.push({ "Name": (timez + " " + "(" + item + ")"), "Timezone": item, "timezonebyvalue": timezone });
                }
            });
            vm.AllTimeZone = _.sortBy(vm.AllTimeZone, 'timezonebyvalue');
            vm.AllTimeZone.reverse();
            $scope.SessionData.companyInfo.TimeZoneModal = $scope.SessionData.companyInfo.TimeZoneModal || vm.AllTimeZone[0].Timezone;

        }
        //Object.size = function (obj) {
        //    var size = 0, key;
        //    for (key in obj) {
        //        if (obj.hasOwnProperty(key))
        //        { size++; }
        //    }
        //    return size;
        //};
        vm.AddRemoveTreeSetup = function (type, data, id) {
            var size = getobjectKeysize(data);
            if (size === 6) {
                if (type === 'add') {
                    treeBlockCount = treeBlockCount + 1;
                    vm.dynamicTreeBlock.push({ id: new Date().getTime() });
                }
                else {
                    vm.treeSetup = _.filter(vm.treeSetup, function (item) { return item !== data; });
                    vm.dynamicTreeBlock = _.filter(vm.dynamicTreeBlock, function (item) { return item.id !== id; });
                }
            }
            else {

            }

        };
        vm.LangTypeClose = function () {
            if ((vm.langDetails.LanguagesModal.length) == (vm.Languages.length)) {
                $('#LangDropdown1').find('md-select-value').empty();
                $('#LangDropdown1').find('md-select-value').append($compile('<span><div class="_md-container"><div class="_md-icon"></div></div><div class="_md-text ng-binding">All</div></span><span class="_md-select-icon" aria-hidden="true"></span>')($scope));
            }
        };
        $scope.getColor = function (color) {
            return $mdColorUtil.rgbaToHex($mdColors.getThemeColor(color));
        };
        function CreateReq() {
            var Request = {
                "CompanyName": $scope.SessionData.companyInfo.CompanyName,
                "ShortName": $scope.SessionData.companyInfo.ShortName,
                "ProviderId": $scope.SessionData.companyInfo.ProviderTypesModal,
                "ExigoCompanyKey": $scope.SessionData.exigoInfo.CompanyKey ? $scope.SessionData.exigoInfo.CompanyKey : '',
                "ExigoLoginName": $scope.SessionData.exigoInfo.ApiLoginName ? $scope.SessionData.exigoInfo.ApiLoginName : '',
                "ExigoPassword": $scope.SessionData.exigoInfo.ApiPassword ? $scope.SessionData.exigoInfo.ApiPassword : '',
                "ReportingConnectionString": $scope.SessionData.exigoInfo.ReportingString ? $scope.SessionData.exigoInfo.ReportingString : '',
                "ExigoPaymentLoginName": $scope.SessionData.exigoInfo.PaymentApiLoginName ? $scope.SessionData.exigoInfo.PaymentApiLoginName : '',
                "ExigoPaymentPassword": $scope.SessionData.exigoInfo.PaymentApiLoginPassword ? $scope.SessionData.exigoInfo.PaymentApiLoginPassword : '',
                "ExigoDefaultContext": $scope.SessionData.exigoInfo.DefaultContextModal ? $scope.SessionData.exigoInfo.DefaultContextModal : '',
                "CommandKey": $scope.SessionData.pillarsInfo.CommandKey ? $scope.SessionData.pillarsInfo.CommandKey : '',
                "MerchantId": $scope.SessionData.pillarsInfo.MerchantID ? $scope.SessionData.pillarsInfo.MerchantID : '',
                "TreeType": $scope.SessionData.companyInfo.TreeTypeModal,
                "TimeZone": $scope.SessionData.companyInfo.TimeZoneModal,
                "Domain": $scope.SessionData.companyInfo.Domain
            };
            $RestService.CreateCompany(Request).success(function (result) {
                try {
                    if (result) {
                        $scope.SessionData.CompanyID = result;
                        $scope.Notification('Success', 'Successfully your Company ID is "' + $scope.SessionData.CompanyID + '"', 'success');
                        $scope.loading = false;
                        resetTreeTabs();
                        vm.currentStep = 1;
                        $scope.SessionData.tabNumber = 1;
                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });

        }
        function CreateCompanyDetailsReq() {
            var Request = {
                "CompanyId": $scope.SessionData.CompanyID,
                "Details": {
                    "Address1": vm.companyDetails.Address1,
                    "Address2": vm.companyDetails.Address2,
                    "City": vm.companyDetails.City,
                    "State": vm.companyDetails.State,
                    "Country": vm.companyDetails.Country,
                    "Zip": vm.companyDetails.Zip,
                    "Phone": vm.companyDetails.Phone,
                    "Email": vm.companyDetails.Email,
                    "CompanyName": vm.companyDetails.CompanyName,
                    "FacebookUrl": vm.companyDetails.FacebookUrl,
                    "TwitterUrl": vm.companyDetails.TwitterUrl,
                    "LinkedInUrl": vm.companyDetails.LinkedInUrl,
                    "PinterestUrl": vm.companyDetails.PinterestUrl,
                    "InstagramUrl": vm.companyDetails.InstagramUrl,
                    "GooglePlusUrl": vm.companyDetails.GooglePlusUrl,
                    "YoutubeUrl": vm.companyDetails.YoutubeUrl,
                    "VimeoUrl": vm.companyDetails.VimeoUrl,
                    "SupportPhone": vm.companyDetails.SupportPhone,
                    "SupportEmail": vm.companyDetails.SupportEmail,
                    "ProductEmail": vm.companyDetails.ProductEmail,
                    "ReplicatedSiteUrl": vm.companyDetails.ReplicatedSiteUrl
                },
                "Encryption": {
                    "EncryptionKey": vm.securitySetup.EncryptionKey,
                    "IVKey": vm.securitySetup.IVKey,
                    "TokenLifeTime": vm.securitySetup.TokenLifeTime
                },
                "Languages": (vm.langDetails.LanguagesModal ? vm.langDetails.LanguagesModal : null),
                "Mail": {
                    "LoginName": vm.mailingDetails.LoginName,
                    "Password": vm.mailingDetails.Password,
                    "HostName": vm.mailingDetails.HostName,
                    "ServerPort": vm.mailingDetails.ServerPort,
                    "FromEmail": vm.mailingDetails.FromEmail,
                    "NoReplyEmail": vm.mailingDetails.NoReplyEmail,
                    "ForgetUrlLink": vm.mailingDetails.ForgetUrlLink,
                    "ForgotPasswordSupportEmail": vm.mailingDetails.ForgotPasswordSupportEmail
                },
                "Trees": treeArray,
                "Teamlink": {
                    "UserName": vm.teamlinkSetup.UserName,
                    "Password": vm.teamlinkSetup.Password,
                    "CompanyKey": vm.teamlinkSetup.CompanyKey,
                    "Url": vm.teamlinkSetup.Url
                },
                "Ziplingo": {
                    "GrantType": vm.ziplingoSetup.GrantType,
                    "ClientId": vm.ziplingoSetup.ClientId,
                    "Password": vm.ziplingoSetup.Password,
                    "UserName": vm.ziplingoSetup.UserName
                },
                "CalendarTypes": (vm.companyCalendraType ? vm.companyCalendraType : null)

            };
            $RestService.SetCompanyDetails(Request).success(function (result) {
                try {
                    if (parseInt(result, 10) > -1) {
                        $scope.loading = false;
                        resetTreeTabs();
                        vm.currentStep = 2;
                        $scope.SessionData.tabNumber = 2;
                        GetProfileSettingControl();
                        $scope.Notification('Success', 'Successfull your Company Details', 'success');
                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });
        }

        /*************** COMPANY SETTING START ****************************************************/

        $scope.SettingControl = {};
        $scope.WidgetSettingControl = {};
        $scope.Getdate = {};
        //$scope.SaveSettingArray = {};

        function GetWidgetsSettingData() {
            var Request = { CompanyId: 10 };
            $scope.loading = true;
            $RestService.GetWidgetsSettingData(Request).success(function (result) {
                try {
                    if (result) {
                        $scope.GetWidgetSettingData = result;
                        $scope.GlobalAccessTypes = result.AccessTypes;
                        $scope.GlobalWidgets = result.Widgets;
                        _.each($scope.GlobalAccessTypes, function (item) {
                            item.SettingControl = {};
                            var SettingControl = _.filter($scope.WidgetSettingsJson, function (jsondata) {
                                return jsondata.elementaccessId == item.Key;
                            })[0];

                            var ControlTypeCategoryWise = _.filter($scope.GlobalWidgets, function (data) {
                                return data.WidgetId == SettingControl.categoryId;
                            })[0];
                            item.canEdit = SettingControl.canEdit;
                            item.categoryId = SettingControl.categoryId;
                            item.controlType = SettingControl.controlType;
                            item.customDataSelect = SettingControl.customDataSelect;
                            item.tooltip = SettingControl.tooltip;
                            item.valueType = SettingControl.valueType;
                            item.elementaccessId = SettingControl.elementaccessId;
                            item.elementaccessName = SettingControl.elementaccessName;
                            item.WidgetId = ControlTypeCategoryWise.WidgetId;
                            item.Description = ControlTypeCategoryWise.Description;
                            item.Name = ControlTypeCategoryWise.Name;
                        });
                        _.each($scope.GetWidgetSettingData.Widgets, function (widgetdata) {
                            widgetdata.Description = $sce.trustAsHtml('<b style="color: red">You can set</b><div class="label label-success">' + widgetdata.Name + '</div> content');
                            widgetdata.WidgetSetting = $sce.trustAsHtml('<div class="label label-success">Set ' + widgetdata.Name + ' settings</div>');
                            widgetdata.WidgetAccess = $sce.trustAsHtml('<div class="label label-success">Set ' + widgetdata.Name + ' Access</div>');
                            widgetdata.WidgetElementAccess = $sce.trustAsHtml('<div class="label label-success">Set ' + widgetdata.Name + ' Elements Access</div>');
                            _.each(widgetdata.AccessTypes, function (accesstypes) {
                                var acctype = _.filter($scope.GlobalAccessTypes, function (globalaccesstype) { return parseInt(accesstypes, 10) === parseInt(globalaccesstype.Key, 10); })[0];
                                widgetdata.CurrentAccessTypes = widgetdata.CurrentAccessTypes || [];
                                widgetdata.CurrentAccessTypes.push({ 'key': accesstypes, 'value': acctype.Value, 'popover': $sce.trustAsHtml('<div class="label label-info">Set ' + acctype.Value + ' Configuration</div>') });
                            });
                        });

                        $scope.CustomerTypes = $scope.AutomationCommonData['CustomerTypes'];
                        $scope.CustomerTypes.push({ 'Key': "0", 'Value': 'Non Team' });
                        _.each($scope.CustomerTypes, function (cust) {
                            cust.popover = $sce.trustAsHtml('<div class="label label-info">Set ' + cust.Value + ' Configuration</div>');
                        });
                        setTimeout(function () {
                            console.log('$scope.GetWidgetSettingData', $scope.GetWidgetSettingData);
                        }, 5000);
                        $scope.loading = false;
                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });
        }

        function GetAutomationCommonSettings() {
            var Request = { CompanyId: 10 };
            $RestService.GetAutomationCommonSettings(Request).success(function (result) {
                try {
                    if (result) {
                        $scope.AutomationCommonData = {};
                        $scope.AutomationCommonData = result;
                        vm.companyCalendraType.push($scope.AutomationCommonData['CalenderType'][0].Key);
                        vm.langDetails.LanguagesModal.push($scope.AutomationCommonData['Languages'][0].ISOCode);
                        if (vm.currentStep === 3) {
                            GetWidgetsSettingData();
                        }

                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });
        }
        GetAutomationCommonSettings();

        vm.GetSettingJsondata = function (set) {
            setTimeout(function () {
                $scope.SettingControl[set] = _.filter($scope.SettingsJson, function (item) {
                    return item.settingId == set;
                })[0];
                if ($scope.SettingControl[set].controlType === 5) {
                    if ($scope.AutomationCommonData[$scope.SettingControl[set].customDataSelect]) {
                        $scope.SettingControl[set].InputText = $scope.AutomationCommonData[$scope.SettingControl[set].customDataSelect][0].Value;

                    }
                }
                else if ($scope.SettingControl[set].controlType === 6) {
                    if ($scope.AutomationCommonData[$scope.SettingControl[set].customDataSelect]) {
                        $scope.SettingControl[set].InputText.push($scope.AutomationCommonData[$scope.SettingControl[set].customDataSelect][0].Value);
                    }
                }
            }, 10);
        };

        function GetProfileSettingControl() {
            var Request = { companyId: 10 };
            $scope.loading = true;
            $RestService.GetCompanySettings(Request).success(function (result) {
                try {
                    if (result) {
                        $scope.ProfileSettingControl = result.Table;
                        _.each($scope.ProfileSettingControl, function (item) {
                            item.SettingControl = {};
                            var SettingControl = _.filter($scope.SettingsJson, function (jsondata) {
                                return jsondata.settingId == item.Id;
                            })[0];
                            var ControlTypeCategoryWise = _.filter(result.Table1, function (data) {
                                return data.CategoryId == SettingControl.categoryId;
                            })[0];
                            item.canEdit = SettingControl.canEdit;
                            item.controlType = SettingControl.controlType;
                            item.customDataSelect = SettingControl.customDataSelect;
                            item.settingId = SettingControl.settingId;
                            item.settingName = SettingControl.settingName;
                            item.valueType = SettingControl.valueType;
                            item.CategoryId = ControlTypeCategoryWise.CategoryId;
                            item.Description = ControlTypeCategoryWise.Description;
                            item.Name = ControlTypeCategoryWise.Name;
                        });

                        // console.log('$scope.ProfileSettingControl', $scope.ProfileSettingControl);
                        $scope.loading = false;
                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });
        }
        if (vm.currentStep === 2) {
            GetProfileSettingControl();
        }
        vm.MakeDatePicker = function (set) {
            _.each($scope.SettingsJson, function (item1) {
                if (item1.settingId == set.Id) {
                    $scope.Getdate[set.Id] = item1;
                }
            });
        };
        function FSaveCompanySettings() {
            var SaveSettingArray = [];
            try {
                var array = Object.keys($scope.SettingControl).map(function (key) {
                    if ($scope.SettingControl[key]) {
                        return $scope.SettingControl[key];
                    }
                });
            }

            catch (ex) {
                console.log('ex', ex);
            }

            setTimeout(function () {
                _.each(array, function (item) {
                    try {
                        if (item) {
                            if ($scope.SettingControl[item.settingId] && $scope.SettingControl[item.settingId].canEdit) {
                                SaveSettingArray.push({ Key: item.settingId, Value: $scope.SettingControl[item.settingId].InputText || '' });
                            }
                        }
                    }
                    catch (ex) { console.log('ex', item); }
                });
                try {
                    var Request = {
                        CompanyId: 82,
                        Settings: SaveSettingArray
                    };
                    // console.log(Request);
                    $RestService.SetCompanySettings(Request).success(function (result) {
                        try {
                            if (result) {
                                $scope.SetCompanySettingsResponse = result;
                                resetTreeTabs();
                                vm.currentStep = 3;
                                $scope.SessionData.tabNumber = 3;
                                GetWidgetsSettingData();
                            }
                        }
                        catch (ex) {
                            console.log("ex", ex.message);
                            $scope.loading = false;
                        }
                    }).error(function (error) {
                        $scope.Notification('Error', 'Some error occur please try again', 'danger');
                        $scope.loading = false;
                    });
                }
                catch (ex) {
                    console.log('ex', ex);
                }
            }, 1000);

        }
        /*************** COMPANY SETTING END ****************************************************/


        /*************** COMPANY WIDGET START ****************************************************/
        $scope.GetWidgetJsondata = function (set) {
            setTimeout(function () {
                $scope.WidgetSettingControl[set] = _.filter($scope.WidgetSettingsJson, function (item) {
                    return item.elementaccessId == set;
                })[0];
                if ($scope.WidgetSettingControl[set].controlType === 5) {
                    $scope.WidgetSettingControl[set].InputText = $scope.AutomationCommonData[$scope.WidgetSettingControl[set].customDataSelect][0].Value;
                }
                else if ($scope.WidgetSettingControl[set].controlType === 6) {
                    $scope.WidgetSettingControl[set].InputText.push($scope.AutomationCommonData[$scope.WidgetSettingControl[set].customDataSelect][0].Value);
                }
            }, 1000);

        };
        $scope.SessionData.SelectedWidgets = [];
        $scope.SessionData.WidgetAccessTypes = {};
        $scope.guid = {};
        $scope.IsLockButtonShow = {};
        $scope.IsSubmitButtonShow = {};
        $scope.AccessElementCustomerTypes = [];
        $scope.AccessElements = {};
        $scope.WidgetAllData = {};
        vm.IsWidgetDone = {};

        vm.GetWidgetAccessTypes = function (widgetid, accesstype, custtype, value) {
            $scope.guid = widgetid + "_" + accesstype + "_" + custtype;
            if (value) {
                $scope.SessionData.SelectedWidgets.push({
                    'id': $scope.guid,
                    'WidgetId': widgetid,
                    'AccessTypeId': accesstype,
                    'CustomerTypeId': parseInt(custtype, 10),
                    'AccessElements': []
                });
                $scope.IsLockButtonShow[widgetid] = true;
            }
            else {
                $scope.SessionData.SelectedWidgets = _.without($scope.SessionData.SelectedWidgets, _.findWhere($scope.SessionData.SelectedWidgets, { id: $scope.guid }));
                var find = _.findWhere($scope.SessionData.SelectedWidgets, { WidgetId: widgetid });
                if (!find) {
                    $scope.IsLockButtonShow[widgetid] = false;
                }
            }

        };

        vm.GetCurrentWidgetData = function (widgetid, accesstype, custtype) {
            $scope.CurrentData = {}; var AccessTypes = []; var CustomerTypes = [];
            $scope.CurrentWidgetguid = widgetid + "_" + accesstype + "_" + custtype;
            $scope.AccessElements = {};
            var CurrentWidgetData = _.filter($scope.SessionData.SelectedWidgets, function (item) {
                return item.WidgetId === widgetid;
            });
            _.each(CurrentWidgetData, function (current) {
                var acctype1 = _.filter($scope.GlobalAccessTypes, function (globalaccesstype) { return parseInt(current.AccessTypeId, 10) === parseInt(globalaccesstype.Key, 10); })[0];
                var acctype2 = _.filter($scope.CustomerTypes, function (custtypes) { return parseInt(current.CustomerTypeId, 10) === parseInt(custtypes.Key, 10); })[0];
                AccessTypes.push({ 'key': current.AccessTypeId, 'value': acctype1.Value });
                CustomerTypes.push({ 'key': current.CustomerTypeId, 'value': acctype2.Value, 'accesstype': current.AccessTypeId, 'popover': $sce.trustAsHtml('<div class="label label-info">Set ' + acctype2.Value + ' Configuration</div>') });
                $scope.CurrentData[widgetid] = {
                    'AccessTypes': _.uniq(AccessTypes, function (item) {
                        return item.key;
                    }),
                    'CustomerTypes': CustomerTypes
                };
            });
        };

        vm.ExpandSwitch = function (value, widgetid) {
            if (!value) {
                var doUpdate = function myfunction() {
                    $scope.AccessElements = {};
                    vm.IsShowElementAccess = false;
                };
                if ($rootScope.$$phase) {
                    $scope.$eval(doUpdate);
                } else {
                    $scope.$apply(doUpdate);
                }
            }
            else {

            }
        };

        vm.GetElementAccessType = function (widgetid, elaccesstype, custtype, accesstype, value) {
            $scope.Elguid = widgetid + "_" + "_" + elaccesstype + "_" + accesstype + "_" + custtype;
            $scope.AccessTypeUid = accesstype + "_" + custtype;
            var CustomerTypeId = custtype;
            var AccessTypeId = accesstype;
            $scope.AccessElements[$scope.AccessTypeUid] = $scope.AccessElements[$scope.AccessTypeUid] || {};
            $scope.AccessElements[$scope.AccessTypeUid].array = $scope.AccessElements[$scope.AccessTypeUid].array || [];
            if (value) {
                $scope.AccessElements[$scope.AccessTypeUid].array.push(elaccesstype);
                //$scope.IsSubmitButtonShow[widgetid] = true;
            }
            else {
                Array.prototype.remove = function () {
                    var what, a = arguments, L = a.length, ax;
                    while (L && this.length) {
                        what = a[--L];
                        while ((ax = this.indexOf(what)) !== -1) {
                            this.splice(ax, 1);
                        }
                    }
                    return this;
                };
                $scope.AccessElements[$scope.AccessTypeUid].array.remove(elaccesstype);
                //if ($scope.AccessElements[$scope.AccessTypeUid].array.length === 0)
                //{
                //    delete $scope.AccessElements[$scope.AccessTypeUid];
                //}
                //if (Object.keys($scope.AccessElements).length > 0)
                //{
                //    console.log('dsads');
                //    $scope.IsSubmitButtonShow[widgetid] = false;
                //}

            }
            console.log('$scope.AccessElements[$scope.AccessTypeUid].array', $scope.AccessElements);
        };

        vm.SaveWidgetData = function (widgetid, index) {
            var filteredArray = _.filter($scope.GetWidgetSettingData.Widgets, function (item) {
                return item.WidgetId === widgetid;
            })[0];
            var WidgetSettingData = [];
            if (filteredArray) {
                _.each(filteredArray.WidgetSettings, function (item) {
                    WidgetSettingData.push({ Id: item.Key, Value: $scope.WidgetSettingControl[item.Key].InputText ? $scope.WidgetSettingControl[item.Key].InputText : null });
                });
            }
            else {
                WidgetSettingData = [];
            }
            $scope.ElementAccessRequest = [];
            var request = {};
            for (var i in $scope.AccessElements) {
                var AccessType; var CustomerType; var ElementIds;
                AccessType = i.toString().split('_')[0];
                CustomerType = i.toString().split('_')[1];
                ElementIds = $scope.AccessElements[i].array;
                $scope.ElementAccessRequest.push(
                    {
                        CustomerTypeId: parseInt(CustomerType, 10),
                        AccessTypeId: parseInt(AccessType, 10),
                        AccessElementList: ElementIds
                    });
            }
            setTimeout(function () {
                request = {
                    CompanyId: $scope.SessionData.CompanyID,
                    WidgetId: widgetid,
                    Settings: WidgetSettingData ? WidgetSettingData : null,
                    AccessElementCustomerTypes: $scope.ElementAccessRequest
                };
                $RestService.SetWidgetSettingData(request).success(function (result) {
                    try {
                        if (result) {
                            console.log('result', result);

                        }
                    }
                    catch (ex) {
                        console.log("ex", ex.message);
                        $scope.loading = false;
                    }
                }).error(function (error) {
                    $scope.Notification('Error', 'Some error occur please try again', 'danger');
                    $scope.loading = false;
                });

                $('#md-' + widgetid).trigger("click");
                vm.InputText[index] = false;
                $scope.AccessElements = {};
                vm.IsShowElementAccess = true;
                vm.IsWidgetDone[widgetid] = true;
                $scope.IsSubmitButtonShow[widgetid] = true;
                console.log('request', request);
            }, 500);

        };
        /*************** COMPANY WIDGET END ********************************************************/

        /*************** REPORT CENTER START ********************************************************/
        $scope.IsDragable = false;
        vm.SelectedTreeEntities = {};
        $scope.selectedItem = {};
        $scope.selectedItemText = {};
        function GetReportCenterData() {
            var Request = { CompanyId: 10 };
            $scope.loading = true;
            $RestService.GetDynamicReportsSettingData(Request).success(function (result) {
                try {
                    if (result) {
                        $scope.SelectedFieldGroups = {};
                        $scope.ReportCenterSettingData = result;
                        vm.TreeEntities = result.TreeEntities;
                        vm.FieldList = result.CompanyFields;
                        vm.FieldGroups = result.FieldGroups;
                        vm.FieldColumn = {};
                        _.each(result.CompanyFields, function (item) {
                            vm.FieldColumn[item.Key] = false;
                            $scope.SelectedFieldGroups[item.Key] = vm.FieldGroups[0].Key;
                        });
                        resetEntityTreeblock();
                        $scope.loading = false;
                        setTimeout(function () {
                            $('#table1').tableDnD({
                                onDrop: function (table, row) {
                                    var rows = table.tBodies[0].rows;
                                    var neworder = [];
                                    $scope.newarray = [];
                                    for (var i = 0; i < rows.length; i++) {
                                        neworder.push(rows[i].id);
                                    }
                                    _.each(neworder, function (item) {
                                        $scope.IsDragable = true;
                                        var record = vm.FieldList[parseInt(item, 10)];
                                        $scope.newarray.push({ key: record.Key, order: parseInt(item, 10) });
                                    });
                                    console.log("newarray", $scope.newarray);
                                }
                            });
                        }, 500);
                    }
                }
                catch (ex) {
                    console.log("ex", ex.message);
                    $scope.loading = false;
                }
            }).error(function (error) {
                $scope.Notification('Error', 'Some error occur please try again', 'danger');
                $scope.loading = false;
            });
        }
        if (vm.currentStep === 4) {
            GetReportCenterData();
        }
        var guiTime = "";
        vm.IsDisable = {};
        vm.AddRemoveEntity = function (type, selectedtreetypeid, displayvalue, id) {
            if (type === 'add') {
                if (selectedtreetypeid !== undefined) {
                    vm.IsDisable[id] = true;
                    guiTime = new Date().getTime();
                    vm.EntityTreeBlock.push({ id: guiTime });
                    if (vm.remainingItems == null || vm.remainingItems === undefined) {
                        vm.remainingItems = _.filter(vm.TreeEntities, function (item) { return item.Key !== selectedtreetypeid; });
                    }
                    else {
                        vm.remainingItems = _.filter(vm.remainingItems, function (item) { return item.Key !== selectedtreetypeid; });
                    }
                    vm.NewEntityArray[guiTime] = angular.copy(vm.remainingItems);
                }
            }
            else {
                vm.EntityTreeBlock = _.filter(vm.EntityTreeBlock, function (item) { return item.id !== id; });
                if (vm.EntityTreeBlock.length === 1) {
                    vm.remainingItems = null;
                    resetEntityTreeblock();
                }
                else {
                    _.each(vm.TreeEntities, function (item) {
                        if (item.Key === selectedtreetypeid) {
                            vm.remainingItems.push(item);
                        }
                    });
                    vm.NewEntityArray[guiTime] = angular.copy(vm.remainingItems);
                }
            }
        };

        function resetEntityTreeblock() {
            vm.EntityTreeBlock = [];
            vm.EntityTreeBlock.push({ id: new Date().getTime() });
            vm.NewEntityArray[vm.EntityTreeBlock[0].id] = angular.copy(vm.TreeEntities);
        }

        //vm.DisablePrimaryCheckboxes = function (value) {
        //    setTimeout(function () {
        //        if (!value) {
        //            vm.DisbleAll = true;
        //        }
        //        else {
        //            vm.DisbleAll = false;
        //        }
        //    }, 50);
        //};

        vm.getorder = getorder;
        function getorder(key) {
            _.each($scope.newarray, function (item) {
                if (parseInt(item.key, 10) === parseInt(key, 10)) {
                    return item.order;
                }
                console.log("item.order", item.order);
            });
        }

        function FSaveReportCenterData() {
            var SaveReportCenterArray = [];
            var TreeEntitiesArray = [];
            var CompanyFieldsArray = [];
            var fieldArray = [];
            var key = "";
            var sortOrder = [];
            var SelectedTree = [];
            var SortedFieldarr = [], count = 1, getidname = [];
            $('#table1 tr').each(function () {
                var mdswitch = $(this).find('md-switch')[0];
                var getelementid = $(mdswitch).attr('id');
                if (getelementid !== undefined) {
                    getidname = getelementid.split('-');
                }
                if ($(this).find('md-switch').hasClass('md-checked')) {
                    SortedFieldarr.push({ 'key': parseInt(getidname[1], 10), 'sortorder': count });
                    count++;
                }
            });
            for (key in vm.NewEntityArray) {
                SelectedTree.push({
                    key: $scope.selectedItem[key],
                    Value: $scope.selectedItemText[key]
                });
            }
            if ($scope.IsDragable === true) {

                _.each(vm.FieldList, function (item) {
                    if (vm.FieldColumn[item.Key]) {
                        sortOrder = _.filter(SortedFieldarr, function (arr) {
                            //console.log(arr.key, item.Key);
                            return parseInt(arr.key, 10) === parseInt(item.Key, 10);
                        });
                        fieldArray.push({ FieldId: item.Key, FieldApiName: null, DisplayName: vm.fieldDispname[item.Key], SortOrder: sortOrder[0].sortorder, IsDefault: vm.DefaultSelected[item.Key], FieldGroupId: vm.SelectedFieldGroups[item.Key] });
                    }
                });
            }
            else {
                _.each(vm.FieldList, function (item) {
                    sortOrder = _.filter(SortedFieldarr, function (arr) {
                        return parseInt(arr.key, 10) === parseInt(item.Key, 10);
                    });
                    if (vm.FieldColumn[item.Key]) {
                        fieldArray.push({ FieldId: item.Key, FieldApiName: null, DisplayName: vm.fieldDispname[item.Key], SortOrder: sortOrder[0].sortorder, IsDefault: vm.DefaultSelected[item.Key], FieldGroupId: vm.SelectedFieldGroups[item.Key] });
                    }
                });
            }
            console.log("fieldArray", fieldArray);

            try {
                var Request = {
                    CompanyId: 10,
                    TreeEntities: SelectedTree,
                    CompanyFields: fieldArray,
                    KpiFields: null
                };
                $RestService.SetReportCenterSettings(Request).success(function (result) {
                    try {
                        if (result) {
                            $scope.SetReportCenterSettingsResponse = result;
                            resetTreeTabs();
                            vm.currentStep = 5;
                            $scope.SessionData.tabNumber = 5;
                        }
                    }
                    catch (ex) {
                        console.log("ex", ex.message);
                        $scope.loading = false;
                    }
                }).error(function (error) {
                    $scope.Notification('Error', 'Some error occur please try again', 'danger');
                    $scope.loading = false;
                });
            }
            catch (ex) {
                console.log('ex', ex);
            }
        }

        /*************** SHOPPING CARTS START ********************************************************/
        vm.shoppingCarts = {};
        function getSavedShoppingCarts() {

        }
        vm.AddShoppingCartSetup = function (event, type, existid) {
            var Cartid = "";
            if (existid) {
                Cartid = existid;
            }
            if (parseInt(type, 10) === 1) {
                setTimeout(function () {
                    var data = {};
                    data = {
                        id: Cartid ? Cartid : '',
                        AccessType: 'shoppingcart',
                        AutomationData: $scope.AutomationCommonData
                    };
                    var callbackfun = getSavedShoppingCarts;
                    $scope.mdDialogPopup(event, 'CreateCompany/MdDialogs/addShoppingCartConfig.tpl.html', type, callbackfun, data);
                }, 10);
            }
            else {
                setTimeout(function () {
                    var data = {};
                    data = {
                        id: Cartid ? Cartid : '',
                        AccessType: 'autoOrderShoppingcart',
                        AutomationData: $scope.AutomationCommonData
                    };
                    console.log(data);
                    var callbackfun = getSavedShoppingCarts;
                    $scope.mdDialogPopup(event, 'CreateCompany/MdDialogs/addAutoOrderCartConfig.tpl.html', type, callbackfun, data);
                }, 10);

            }

        };
        vm.Fdeletecart = function (id, type) {
            if (parseInt(type, 10) === 1) {
                delete $scope.SessionData.ShoppingCarts[id];
            }
            else {
                delete $scope.SessionData.AutoOrderShoppingCarts[id];
            }
        };
        vm.FEditCart = function (event, id, type) {
            vm.AddShoppingCartSetup(event, type, id);
        };
        /*************** SHOPPING CARTS END ********************************************************/
    }

})()
;