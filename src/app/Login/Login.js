
angular.module('OUApp.Login', [
        'ui.router',
        'ui.bootstrap',
        'RestService'
        
])

.config(function config($stateProvider) {
    $stateProvider.state('Login', {
        url: '/Login',
        views: {
            "main": {
                controller: 'LoginCtrl',
                templateUrl: 'Login/Login.tpl.html'
            }
        },
        data: { pageTitle: 'Login' }
    });
}).
run(function ($rootScope) {
    angular.element(document).on("click", function (e) {
        $rootScope.$broadcast("documentClicked", angular.element(e.target));
    });
})
/**
 * And of course we define a controller for our route.
 */
.controller('LoginCtrl', ['$scope', '$location', '$RestService', '$window', '$compile','$rootScope', function HomeController($scope, $location, $RestService, $window, $compile,$rootScope) {
    $scope.Loginpage = true;
    $rootScope.companyHeader = false;
    $rootScope.userHeader = false;
    $scope.loading = false;
    $scope.CurrentUser.LoginDetails = $scope.CurrentUser.LoginDetails || {};
    $scope.showSystemMessage = true;
    $window.localStorage.setItem('accessToken', null);
    $window.localStorage.setItem('refreshToken', null);
    $window.localStorage.setItem('tokenExpiretime', null);
    $scope.AuthenticationFunc = function () {        
        $window.localStorage.setItem('AuthTokenDetails', JSON.stringify({}));
        try {
            $scope.loading = true;
            if ($scope.Username === "admin" && $scope.password === "admin") {
                $location.path('/Start');
            }
            //var TokenRequest = {
            //    client_id: "swasthyWebApp",
            //    grant_type: "password",
            //    UserName: $scope.Username,
            //    Password: $scope.password
            //};
            //$RestService.Token(TokenRequest).success(function (result) {
            //    $window.localStorage.setItem('AuthTokenDetails', JSON.stringify(result));
            //    $window.localStorage.setItem('accessToken', result.access_token);
            //    $window.localStorage.setItem('refreshToken', result.refresh_token);
            //    $window.localStorage.setItem('tokenExpiretime', result['.expires']);
            //    $scope.CurrentUser.LoginDetails = result;
            //    $scope.CurrentUser.RoleID = result.role;                
            //    $location.path('/Start');
            //}).error(function () { $scope.loading = false; });
        }
        catch (ex) {
            $scope.loading = false;
        }
    };
}]);

