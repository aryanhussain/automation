﻿angular.module('OUApp.ForgotPassword', [
 'ui.router',
  'placeholders',
  'ui.bootstrap',
  'RestService'])
.config(function config($stateProvider) {
    $stateProvider
    .state('ForgotPassword', {
        url: '/ForgotPassword',
        views: {
            "main": {
                controller: 'ForgotPasswordcntrl',
                templateUrl: 'ForgotPassword/forgotpassword.tpl.html'
            }
        },
        data: { pageTitle: 'Login' }

    });
})
.controller('ForgotPasswordcntrl', ['$rootScope', '$scope', '$window', '$log', '$location', '$timeout', '$RestService', '$stateParams', function ($rootScope, $scope, $window, $log, $location, $timeout, $RestService, $stateParams) {
    $rootScope.Loginpage = true;
    $rootScope.companyHeader = false;
    $rootScope.userHeader = false;
    $scope.loading = false;
    $scope.CurrentUser.LoginDetails = $scope.CurrentUser.LoginDetails || {};
    $scope.showSystemMessage = true;
    $scope.token = "";
    $scope.PageMessage = '';
    var queryStrings = $location.search();
    if (queryStrings !== null && queryStrings !== undefined) {
        if (queryStrings.token !== null && queryStrings.token !== undefined) {
            $scope.token = queryStrings.token;
        }
        else {
            $location.path('/');
        }

        if (queryStrings.uid !== null && queryStrings.uid !== undefined) {
            $scope.ReferenceId = queryStrings.uid;
        }
        else {
            $location.path('/');
        }

        $scope.token = queryStrings.token;
    }
    else {
        $location.path('/');
    }

    $scope.changePassword = function () {
        $scope.PageMessage = '';
        if ($scope.Password !== $scope.ConfirmPassword) {
            $scope.PageMessage = "confirm password does not match";
        }
        else {
            message.showMessage('progress');
            try {
                var request = {
                    ReferenceId: $scope.ReferenceId,
                    Token: $scope.token,
                    Password: $scope.Password
                };
                $RestService.ChangePassword(request).success(function (resp) {
                    if (resp.Result.Status === 1) {
                        message.showMessage('success');
                        $location.path('/');
                    }
                    else {
                        if (resp.Result.Errors !== undefined && resp.Result.Errors.length > 0) {
                            $scope.PageMessage = resp.Result.Errors[0];
                        }
                        else {
                            $scope.PageMessage = "Unable to change the password, please try again later";
                            message.showMessage('error');
                        }
                    }
                }).error(function () { message.showMessage('error'); });
            }
            catch (ex) {
                message.showMessage('error');
            }
        }
    };
}]);